var SiteUrl2 = "/ukwriter/backend/web/index.php/";
function CalculateTotalAssignningValue(order_num_of_pages)
{
    var txtVal = $("input[name='rate_price']").val();
    var pay_cond = $("input[name='payment_cond']:checked").val();
    var amount_to_print;
    if(pay_cond=="0"){ amount_to_print = (parseFloat(txtVal)*parseFloat(order_num_of_pages)); }
    else{ amount_to_print = txtVal; }

    if(isNaN(amount_to_print)){ var amount_to_print = ""; }
    else{amount_to_print = parseFloat(amount_to_print).toFixed(2); $(".total_amount").text(amount_to_print);}
}

function GetWriterDetails(writer_id,total_pages)
{
    $.ajax({
        type: "POST",
        url: SiteUrl2+"ajax/writer-details/",
        data: "wid="+writer_id,
        dataType: "json",
        cache: false,
        success: function(data)
        {
            $(".pcur").html(data.cur);
            $("input.rp").val(data.rate);
            $("input[name='payment_cond'][value='"+data.pc+"']").prop('checked', true);
            CalculateTotalAssignningValue(total_pages);
        },
        error:function(jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);
        }
    });
    return false;
}


$(document).ready(function(){

    $("[name='FixedLinks[paper_type_id]']").change(function(){
        
        paper_type_id = $(this).val();
        GetPrice($(this));
        
    });

    $("[name='FixedLinks[service_type_id]']").change(function(){
        
        service_type_id = $(this).val();
        GetPrice($(this));
        
    });

    $("[name='FixedLinks[urgency_id]']").change(function(){
        
        urgency_time_id = $(this).val();
        GetPrice($(this));
        
    });

    $("[name='FixedLinks[quality_level_id]']").change(function(){
        
        quality_level_id = $(this).val();
        GetPrice($(this));
        
    });


    $("[name='FixedLinks[no_pages_id]']").change(function(){
        
        number_of_pages_is = $(this).val();
        GetPrice($(this));
        
    });

    $("[name='FixedLinks[currency]']").change(function(){
        
        currency_id = $(this).val();
        $("#exchange_rate").val(parseFloat(currency_list[currency_id]['r']).toFixed(2));
        GetPrice($(this));
        
    });

    jQuery("#discount_percentage_field").keyup(function(){
        SetPercentageDiscount();
    });
    jQuery("#discount_amount_field").keyup(function(){
        SetFixedDiscount();
    });

    jQuery("#fixedlinks-amount").blur(function(){
        SetFixedDiscount();
        SetPercentageDiscount();
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if(target == "#profile7")
            jQuery("#f_or_p_selected").val('1');
        else
            jQuery("#f_or_p_selected").val('2    ');
    });
});

var actual_amount_changeable = false;
var first_time_execution = true;
var service_type_id = urgency_time_id = quality_level_id = number_of_pages_is = 0;
var currency_id = jQuery("[name='FixedLinks[currency]']").val();
function ThousandFormat(num)
{
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function GetPrice(field)
{
    price_okay = true;
    if(paper_type_id==0 || paper_type_id=="")
    {
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat('0.00')+'</div><div class="message">(select type of paper)</div>');
        price_okay = false;
    }
    else if(service_type_id==0)
    {
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat('0.00')+'</div><div class="message">(select type of service)</div>');
        price_okay = false;
    }
    else if(urgency_time_id==0)
    {
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat('0.00')+'</div><div class="message">(select urgency)</div>');
        price_okay = false;
    }
    else if(quality_level_id==0)
    {
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat('0.00')+'</div><div class="message">(select quality level)</div>');
        price_okay = false;
    }
    else if(number_of_pages_is==0)
    {
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat('0.00')+'</div><div class="message">(select number of pages)</div>');
        price_okay = false;
    }


    if(price_okay==false)
    {
        jQuery("#fixedlinks-amount").val("0.00");
        
    }
    else
    {
        gross_plag_amount = 0;

        price_per_page = ((parseFloat(paper_service[paper_type_id][service_type_id])+parseFloat(paper_urgency[paper_type_id][urgency_time_id])+parseFloat(quality_list[quality_level_id]))*parseFloat(currency_list[currency_id]['r'])).toFixed(2);
        total_price = ((price_per_page*number_of_pages_is)+parseFloat(gross_plag_amount)).toFixed(2);
        jQuery("#actual_amount_label").html('<div class="total">'+currency_list[currency_id]['s']+ThousandFormat(total_price)+'</div>');
        if(first_time_execution==true && total_price!=jQuery("#fixedlinks-amount").val())
        {
            SetAmountChangeableSection();
        }
        if(actual_amount_changeable==false)
        {
            jQuery("#fixedlinks-amount").val(total_price);
        }
        //jQuery("#fixedlinks-amount_changer").show(0);
    }
    SetPercentageDiscount();
    SetFixedDiscount();
    first_time_execution = false;
    field.focus();
}

function SetPercentageDiscount()
{
    actual_amount_is = parseFloat(jQuery("#fixedlinks-amount").val());
    discount_percentage_field_is = jQuery("#discount_percentage_field");
    percn_discount_percentage = parseFloat(discount_percentage_field_is.val());
    if(isNaN(percn_discount_percentage)==true)
    {
        discount_percentage_field_is.val("");
        percn_discount_percentage = 0;
    }
    else
    {
        if(percn_discount_percentage>100)
        {
            discount_percentage_field_is.val("100");
            percn_discount_percentage = 100;
        }
        else if(percn_discount_percentage<0)
        {
            discount_percentage_field_is.val("0");
            percn_discount_percentage = 0;
        }
    }
    discount_amount_is = ((actual_amount_is/100)*percn_discount_percentage).toFixed(2);
    charge_amount_is = (actual_amount_is-discount_amount_is).toFixed(2);
    jQuery("#percentage_discount_amount").html(currency_list[currency_id]['s']+ThousandFormat(discount_amount_is));
    jQuery("#percentage_discount_charge").html(currency_list[currency_id]['s']+ThousandFormat(charge_amount_is));
}

function SetFixedDiscount()
{
    actual_amount_is = parseFloat(jQuery("#fixedlinks-amount").val());
    discount_amount_field_is = jQuery("#discount_amount_field");
    fixed_discount_amount = parseFloat(discount_amount_field_is.val());
    if(isNaN(fixed_discount_amount)==true)
    {
        discount_amount_field_is.val("");
        fixed_discount_amount = 0;
    }
    else
    {
        if(fixed_discount_amount>actual_amount_is)
        {
            discount_amount_field_is.val(actual_amount_is);
            fixed_discount_amount = actual_amount_is;
        }
        else if(fixed_discount_amount<0)
        {
            discount_amount_field_is.val("0");
            fixed_discount_amount = 0;
        }
    }
    discount_percentage_is = (actual_amount_is=='0') ? '0' : ((fixed_discount_amount/actual_amount_is)*100).toFixed(2);
    charge_amount_is = (actual_amount_is-fixed_discount_amount).toFixed(2);
    jQuery("#fixed_discount_percentage").html(discount_percentage_is+'%');
    jQuery("#fixed_discount_charge").html(currency_list[currency_id]['s']+ThousandFormat(charge_amount_is));
    discount_amount_field_is.parent().children("div").html(currency_list[currency_id]['s']);
}