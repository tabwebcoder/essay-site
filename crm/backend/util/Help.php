<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 9/21/2018
 * Time: 3:15 PM
 */

namespace backend\util;

use common\models\Urgency;

class Help
{
    public static function getCustomerDealLine($urgencyId)
    {
        $urgency = Urgency::find()->where(['id' => $urgencyId])->one();
        $deadline = date('Y-m-d h:i:s a', strtotime('+ ' . $urgency->name));

        return $deadline;
    }

    public static function getRemainingDays($writerDeadline)
    {
        $date = strtotime($writerDeadline);//Converted to a PHP date (a second count)

        $diff = $date - time();//time returns current time in seconds
        $days = floor($diff / (60 * 60 * 24));//seconds/minute*minutes/hour*hours/day)
        $hours = round(($diff - $days * 60 * 60 * 24) / (60 * 60));
        return "$days days $hours hours";
    }

    public static function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::_crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }

    private static function _crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
}