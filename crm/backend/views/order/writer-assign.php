<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 9/22/2018
 * Time: 1:47 PM
 */
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Writer Assigning and Assign Detail for Order#' . $id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;


$writersList = ArrayHelper::map(User::find()->where(['role_id' => User::ROLE_WRITER])->asArray()->all(), 'id', 'full_name');
$perPage = $o->noPages->name;
?>
<?php $form = ActiveForm::begin(['id' => 'w_form']); ?>
    <div class="card">
        <div class="card-body">
            <h4>Assign Task to Writer - Order #<?= $id ?></h4>
            <br>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group required">
                        <label class="control-label" for="orders-topic">Assign Order To:</label>
                        <?= Html::dropDownList('writer_id', [], $writersList, ['prompt' => 'Select Writers', 'class' => 'form-control swriters', 'required' => 'required', 'id' => 'writer_id']) ?>
                        <div class="help-block we"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group required">
                        <label class="control-label" for="orders-topic">Submission Date:</label>
                        <?= Html::textInput('submission_date', $o->writer_deadline, ['class' => 'form-control dt', 'required' => 'required']) ?>
                        <div class="help-block we"></div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Payment Condition:</label>
                    <div class="radio radio-info">
                        <input name="payment_cond" id="payment_cond_0" value="0" type="radio" required>
                        <label for="payment_cond_0">Per Page</label>
                    </div>
                    <div class="radio radio-primary">
                        <input name="payment_cond" id="payment_cond_1" value="1" type="radio" required>
                        <label for="payment_cond_1">Fixed</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group required">
                        <label class="control-label" for="orders-topic">Agreed Rate/Price:</label>
                        <div class="input-group bootstrap-touchspin">
                            <div class="input-group-prepend bootstrap-touchspin-prefix"><span
                                        class="input-group-text pcur">USD</span></div>
                            <input id="tch5" class="form-control rp" name="rate_price" required
                                   onkeyup="CalculateTotalAssignningValue('<?= $perPage[0] ?>')" value=""
                                   data-bts-button-down-class="btn btn-secondary btn-outline"
                                   data-bts-button-up-class="btn btn-secondary btn-outline" style="display: block;"
                                   type="text">
                            <div class="input-group-append bootstrap-touchspin-postfix"><span
                                        class="input-group-text total_amount"></span></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group float-none">
                        <br>
                        <?= Html::submitButton('Assign Task', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
$this->registerJs(
    "
    $(\"#w_form\").validate();
    var total_pages = '" . $perPage[0] . "';
    $(function () {
         $('.dt').datetimepicker();
         $('.swriters').on('change',function() {
            GetWriterDetails($(this).val(),total_pages);
         
         });
     });"
)

?>