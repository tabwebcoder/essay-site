<?php
use backend\util\Help;

$this->title = 'Pending Orders';
#$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = 'Dashboard';
$pendingOrders = \common\models\Orders::find()->where(['status'=>'pending'])->all();

?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Pending Works</h4>
        <div class="table-responsive">
            <table class="table color-bordered-table warning-bordered-table">
                <thead>
                <tr>
                    <th>Order #</th>
                    <th>Customer Name</th>
                    <th>Topic</th>
                    <th>Subject Area</th>
                    <th>Service</th>
                    <th>Status</th>
                    <th>Pages</th>
                    <th>Agent</th>
                    <th>Customer Deadline</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pendingOrders as $o): ?>
                <tr>
                    <td><?= $o->id ?></td>
                    <td><?= $o->customer->name ?></td>
                    <td><?= $o->topic ?></td>
                    <td><?= $o->subjectArea->name ?></td>
                    <td><?= $o->serviceType->name ?></td>
                    <td><span class="badge badge-warning"><?= $o->status ?></span></td>
                    <td><?= $o->noPages->name ?></td>
                    <td><?= $o->agent->full_name ?></td>
                    <td><?= Help::getCustomerDealLine($o->urgency_id) ?></td>
                    <td><a href="<?= \yii\helpers\Url::to(['order/writer-assign', 'id' => $o->id]) ?>" title="Edit or Assign Task" aria-label="Edit or Assign Task" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="<?= \yii\helpers\Url::to(['order/view', 'id' => $o->id]) ?>" title="View Order Details" aria-label="View Order Details" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
