<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Order Edit #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;

$subjectAreaList = ArrayHelper::map(\common\models\SubjectsAreas::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$academicList = ArrayHelper::map(\common\models\AcademicLevels::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');
$writingStyleList = ArrayHelper::map(\common\models\WritingStyles::find()->andWhere(['is_active'=>'1'])->asArray()->all(), 'id', 'name');


?>
<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'paper_type_id')->textInput(['maxlength' => true,'value'=>$model->paperType->name,'disabled'=>true]) ?>

        <?= $form->field($model, 'service_type_id')->textInput(['maxlength' => true,'value'=>$model->serviceType->name,'disabled'=>true]) ?>

        <?= $form->field($model, 'order_date')->textInput() ?>

        <?= $form->field($model, 'urgency_id')->textInput(['maxlength' => true,'value'=>$model->urgency->name,'disabled'=>true]) ?>

        <?= $form->field($model, 'customer_deadline')->textInput(['maxlength' => true,'value'=>\backend\util\Help::getCustomerDealLine($model->urgency_id),'disabled'=>true]) ?>

        <?= $form->field($model, 'quality_id')->textInput(['maxlength' => true,'value'=>$model->quality->name,'disabled'=>true]) ?>

        <?= $form->field($model, 'no_pages_id')->textInput(['maxlength' => true,'value'=>$model->noPages->name,'disabled'=>true]) ?>

        <?= $form->field($model, 'subject_area_id')->dropDownList($subjectAreaList,['prompt' => "Select Subject Area"]) ?>

        <?= $form->field($model, 'topic')->textInput() ?>

        <?= $form->field($model, 'acadamic_level_id')->dropDownList($academicList,['prompt' => "Select Subject Area"]) ?>

        <?= $form->field($model, 'writing_style_id')->dropDownList($writingStyleList,['prompt' => "Select Subject Area"]) ?>

        <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'instructions')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Change Order Details', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
