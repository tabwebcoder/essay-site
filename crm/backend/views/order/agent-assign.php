<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 9/22/2018
 * Time: 1:47 PM
 */
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Agent Assigning for Order#' . $id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;


$agentsList = ArrayHelper::map(User::find()->where(['role_id' => User::ROLE_CSR])->asArray()->all(), 'id', 'full_name');
?>
<?php $form = ActiveForm::begin(['id' => 'w_form']); ?>
    <div class="card">
        <div class="card-body">
            <h4>Assign Task to Agent - Order #<?= $id ?></h4>
            <br>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group required">
                        <label class="control-label" for="orders-topic">Assign Order To:</label>
                        <?= Html::dropDownList('agent_id', [], $agentsList, ['prompt' => 'Select Agents', 'class' => 'form-control', 'required' => 'required', 'id' => 'agent_id']) ?>
                        <div class="help-block we"></div>
                    </div>
                </div>


            </div>
            <div class="row">

                <div class="col-md-4">
                    <div class="form-group float-none">
                        <br>
                        <?= Html::submitButton('Assign Task', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
