<?php
/* @var $this yii\web\View */
use yii\helpers\Html;


$this->title = 'Order View #'.$id;
$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Basic Order Information</h4>
        <table class="table color-bordered-table success-bordered-table">
            <tr>
                <th>Order Id</th>
                <td><?=$o->id?></td>
            </tr>
            <tr>
                <th>Paper Type</th>
                <td><?=$o->paperType->name?></td>
            </tr>
                <th>Service Type</th>
                <td><?=$o->serviceType->name?></td>
            </tr>
            <tr>
                <th>Order Date</th>
                <td><?=$o->order_date?></td>
            </tr>
            <tr>
                <th>Urgency</th>
                <td><?=$o->urgency->name?></td>
            </tr>
            <tr>
                <th>Customer Deadline</th>
                <td><?=\backend\util\Help::getCustomerDealLine($o->urgency_id)?></td>
            </tr>
            <tr>
                <th>Quality</th>
                <td><?=$o->quality->name?></td>
            </tr>
            <tr>
                <th>Num of Pages</th>
                <td><?=$o->noPages->name?></td>
            </tr>
            <tr>
                <th>Subject</th>
                <td><?=$o->subjectArea->name?></td>
            </tr>
            <tr>
                <th>Topic</th>
                <td><?=$o->topic?></td>
            </tr>
            <tr>
                <th>Level</th>
                <td><?=$o->acadamicLevel->name?></td>
            </tr>
            <tr>
                <th>Writing Style</th>
                <td><?=$o->writingStyle->name?></td>
            </tr>
            <tr>
                <th>Language</th>
                <td><?=$o->language?></td>
            </tr>
            <tr>
                <th>Description</th>
                <td><?=$o->instructions?></td>
            </tr>


        </table>

    </div>
</div>

<div class="card">
    <div class="card-body">
    <h4 class="card-title">Client Information - Order # <?=$id?></h4>
        <table class="table color-bordered-table success-bordered-table">
            <tr>
                <th>Client Id</th>
                <td><?=$c->id?></td>
            </tr>
            <tr>
                <th>Customer Name</th>
                <td><?=$c->name?></td>
            </tr>
                <th>Customer Email</th>
                <td><?=$c->email?></td>
            </tr>
            <tr>
                <th>Customer Phone</th>
                <td><?=$c->phone?></td>
            </tr>
            <tr>
                <th>Customer IP</th>
                <td><?=$c->ip?></td>
            </tr>
            <tr>
                <th>Customer IP-City</th>
                <td><?=$c->city?></td>
            </tr>
            <tr>
                <th>Customer IP-Region</th>
                <td><?=$c->region?></td>
            </tr>
            <tr>
                <th>Customer IP-Country</th>
                <td><?=$c->country?></td>
            </tr>
            <tr>
                <th>Customer Registration </th>
                <td><?=$c->created_at?></td>
            </tr>


        </table>

    </div>
</div>
