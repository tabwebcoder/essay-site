<?php
use backend\util\Help;

$this->title = 'Process Orders';
#$this->params['breadcrumbs'][] = ['label' => 'Dashboard', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = 'Dashboard';

$progressOrders = \common\models\Orders::find()->where(['status'=>'in_progress'])->all();
?>


<div class="card">
    <div class="card-body">
        <h4 class="card-title">In-Process Orders List</h4>
        <div class="table-responsive">
            <table class="table color-bordered-table warning-bordered-table">
                <thead>
                <tr>
                    <th>Order #</th>
                    <th>Customer Name</th>
                    <th>Writer</th>
                    <th>Subject Area</th>
                    <th>Remaining</th>
                    <th>Writer Deadline</th>
                    <th>Pages</th>
                    <th>Customer Deadline</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($progressOrders as $o): ?>
                <tr>
                    <td><?= $o->id ?></td>
                    <td><?= $o->customer->name ?></td>
                    <td><?= $o->writerTasks[0]->writer->full_name ?></td>
                    <td><?= $o->subjectArea->name ?></td>
                    <td><?= Help::getRemainingDays($o->writer_deadline) ?></td>
                    <td><?= $o->writer_deadline ?></td>
                    <td><?= $o->noPages->name ?></td>
                    <td><?= Help::getCustomerDealLine($o->urgency_id) ?></td>
                    <td><span class="badge badge-success"><?= $o->status ?></span></td>
                    <td><a href="<?= \yii\helpers\Url::to(['order/assign-update', 'id' => $o->id]) ?>" title="Change Assigning Details" aria-label="Change Assigning Details" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a href="<?= \yii\helpers\Url::to(['order/view', 'id' => $o->id]) ?>" title="View Order Details" aria-label="View Order Details" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>