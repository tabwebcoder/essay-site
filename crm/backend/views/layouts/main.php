<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
$divClass = "";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>mhrwriter | <?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/images/favicon-ecommerce.png">
    <!-- Bootstrap Core CSS -->

    <!--<link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/morrisjs/morris.css" rel="stylesheet">-->

    <link href="//themepixels.me/amanda/lib/morris.js/morris.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap-datepicker/jquery.datetimepicker.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/css/multiple-select.css" rel="stylesheet">
    <link href="/theme1/dtp/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/css-chart/css-chart.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/jsgrid/jsgrid.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/jsgrid/jsgrid-theme.min.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/css/aoa.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/dropify/dist/css/dropify.min.css">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/css/colors/blue.css" id="theme" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/footable/css/footable.core.css" id="theme" rel="stylesheet">
    <link href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/plugins/ion-rangeslider/css/ion.rangeSlider.skinModern.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link href="/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <![endif]-->
    <style>
        .error {
            color:red;
            border-bottom: 1px solid red;
        }
        .loader {

        }
    </style>
</head>
<body class="fix-header fix-sidebar card-no-border">
<?php $this->beginBody() ?>
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!--<div id="loading" class="ui-front loader ui-widget-overlay bg-white opacity-100">
    <img src="/monster-admin/images/loader-dark.gif" alt="" />
</div>-->
<?php
if( !Yii::$app->user->isGuest ){
?>
    <div id="main-wrapper" class="demo-example">

        <?= $this->render('_header'); ?>
        <?php if (!Yii::$app->user->isGuest):
            $divClass = "page-content-wrapper";
            ?>
            <?= $this->render('_sidebar'); ?>
        <?php endif; ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-12 col-8 align-self-center">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <?= \yii\widgets\Breadcrumbs::widget([
                                            'links' => isset(\Yii::$app->params['breadcrumbs']) ? \Yii::$app->params['breadcrumbs'] : [],
                                        ]) ?>
                                        <h3><?=$this->title?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
                <?= $content ?>
            </div>
        </div>
    </div><!-- #page-wrapper -->

    <?php
}else{
    ?>
    <?=$content?>
<?php
}
?>

<?php $this->endBody() ?>
<?php
if( !Yii::$app->user->isGuest ){
    ?>
    <footer class="footer">
        © <?=date('Y');?> mhwriter
    </footer>
<?php
}
?>

<script>
    $('.cs-logout').on('click', function () {
        $("#lg").submit();
    });
    $('body').find("span").removeClass("glyphicon").addClass("fa");
    $('body').find("span.glyphicon-trash").removeClass("glyphicon-trash").addClass("fa-trash");
    $('body').find("span.glyphicon-pencil").removeClass("glyphicon-pencil").addClass("fa-pencil");
    $('body').find("span.glyphicon-eye-open").removeClass("glyphicon-eye-open").addClass("fa-eye");
</script>
<form id="lg" action="/<?= \Yii::$app->params['preLink'] ?>site/logout" method="post">
    <input id="form-token" type="hidden" name="_csrf-backend" value="<?=Yii::$app->request->csrfToken?>"/>
</form>

</body>
</html>

<?php $this->endPage() ?>

