<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
/*$this->params['breadcrumbs'][] = $this->title;*/
?>
<section id="wrapper">
    <div class="login-register" style="background-image:url(/<?= \Yii::$app->params['subfolder'] ?>/monster-admin/assets/images/background/login-register.jpg); background-position: center;">
        <div class="login-box card">
            <div class="card-body">
                <!--<form class="form-horizontal form-material" id="loginform" action="index.html">-->
                    <?php $form = ActiveForm::begin(['id' => 'login-form','options'=>['class' => 'form-horizontal form-material']]); ?>
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <!--<input class="form-control" type="text" required="" placeholder="Username">-->
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control'
                            ]) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <!--<input class="form-control" type="password" required="" placeholder="Password">-->
                            <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <!--<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>-->
                            <?= Html::submitButton('Login', ['class' => 'btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light', 'name' => 'login-button']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</section>