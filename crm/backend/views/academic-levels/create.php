<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AcademicLevels */

$this->title = 'Create Academic Levels';
$this->params['breadcrumbs'][] = ['label' => 'Academic Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
