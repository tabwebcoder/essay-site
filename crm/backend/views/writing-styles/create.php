<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WritingStyles */

$this->title = 'Create Writing Styles';
$this->params['breadcrumbs'][] = ['label' => 'Writing Styles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
