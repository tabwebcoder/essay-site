<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WritingStyles */

$this->title = 'Update Writing Styles: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Writing Styles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card">
    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
