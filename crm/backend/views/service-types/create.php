<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ServiceTypes */

$this->title = 'Create Service Types';
$this->params['breadcrumbs'][] = ['label' => 'Service Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
