<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NoPages */

$this->title = 'Create No Pages';
$this->params['breadcrumbs'][] = ['label' => 'No Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?><div class="card">
    <div class="card-body">


        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
