<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'role_id',
                    'value' => 'role.name'
                ],
                'username',
                'full_name',
                // 'password_hash',
                // 'password_reset_token',
                // 'email:email',
                // 'status',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}',],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
<?php
$this->registerJs("
jQuery(document).ready(function () {
        jQuery('.grid-view').find('table').removeClass('table-striped table-bordered').addClass('color-bordered-table muted-bordered-table');
    })
");
?>
