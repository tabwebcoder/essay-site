<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Change Details';
$this->params['breadcrumbs'][] = 'Change Details';
?>
<div class="card-body">

    <div class="user-update">
        <?= $this->render('_update', [
            'model' => $model,
        ]) ?>
    </div>
</div>