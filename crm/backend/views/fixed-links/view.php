<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FixedLinks */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Fixed Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fixed-links-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'order_id',
            'unique_code',
            'amount',
            'discount_percentage',
            'discount_amount',
            'show_discount_msg',
            'status',
            'expiry_date',
            'paper_type_id',
            'service_type_id',
            'urgency_id',
            'quality_level_id',
            'no_pages_id',
            'currency',
            'created_at',
        ],
    ]) ?>

</div>
