<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FixedLinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fixed Links';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                'title',
                'order_id',
                'amount',
                'discount',
                'to_charge',
                'expiry_date',
                'status',
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}']
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>