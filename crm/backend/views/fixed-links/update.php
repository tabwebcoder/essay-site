<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FixedLinks */

$this->title = 'View Fixed Links';
$this->params['breadcrumbs'][] = ['label' => 'Fixed Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="card">
    <div class="card-body">


        <?= $this->render('_form_view', [
            'model' => $model,
        ]) ?>

    </div>
</div>
