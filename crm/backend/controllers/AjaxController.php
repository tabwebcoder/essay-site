<?php

namespace backend\controllers;

use common\models\Customers;
use common\models\Transactions;
use common\models\User;
use Yii;
use common\models\Orders;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class AjaxController extends \yii\web\Controller
{

    public function actionWriterDetails()
    {
        $wid = Yii::$app->request->post('wid');
        if($wid)
        {
            $u = User::find()->where(['id'=>$wid])->one();
            if($u->per_page_rate != ''){
                $rate = $u->per_page_rate;
                $pc = '0';
            }
            else {
                $rate = $u->fixed_rate;
                $pc = '1';
            }

            $result = ['error'=>0,'pc'=>$pc,'rate'=>$rate,'cur'=>$u->payout_currency];
            return json_encode($result);

        } else {
            $result = ['error'=>1];
            return json_encode($result);
        }
    }

}
