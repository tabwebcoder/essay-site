<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'subfolder'=>'ukwriter/backend/web/',
    'preLink'=>'ukwriter/backend/web/index.php/'
];
