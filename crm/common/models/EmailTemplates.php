<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property string $id
 * @property string $type
 * @property string $subject
 * @property string $body
 * @property int $is_active
 * @property string $created_at
 */
class EmailTemplates extends \yii\db\ActiveRecord
{

    public $customer;
    public $order;
    public $emails = "";
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'subject', 'body', 'created_at'], 'required'],
            [['type', 'body'], 'string'],
            [['created_at'], 'safe'],
            [['subject'], 'string', 'max' => 250],
            [['is_active'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'subject' => 'Subject',
            'body' => 'Body',
            'is_active' => 'Active',
            'created_at' => 'Created At',
        ];
    }

    private function _parseEmails()
    {
        $e = self::find()->where(['type'=>$this->type])->one();
        $content = $e->body;
        $subject = $e->subject;
        if($this->type == 'client_intro')
        {
            // body
            $content = str_replace('#client_name',$this->customer->name,$content);
            $content = str_replace('#agent_name',$this->order->agent->full_name,$content);
            $content = str_replace('#order_no',$this->order->id,$content);
            $e->body  = $content;
            $this->emails = $this->customer->email;

        }

        if($this->type == 'order_not_paid')
        {
            // body
            $content = str_replace('#client_name',$this->customer->name,$content);
            $content = str_replace('#agent_name',$this->order->agent->full_name,$content);
            $content = str_replace('#order_no',$this->order->id,$content);
            $content = str_replace('#task_topic',$this->order->topic,$content);
            $content = str_replace('#no_pages',$this->order->noPages->name,$content);
            $content = str_replace('#subject_area',$this->order->subjectArea->name,$content);
            $e->body  = $content;
            $this->emails = $this->customer->email;

        }

        if($this->type == 'task_assign_to_writer')
        {
            // body
            $content = str_replace('#writer_name',$this->order->writer->full_name,$content);
            $content = str_replace('#agent_name',$this->order->agent->full_name,$content);
            $content = str_replace('#order_no',$this->order->id,$content);
            $content = str_replace('#task_topic',$this->order->topic,$content);
            $content = str_replace('#writer_deadline',$this->order->writer_deadline,$content);
            $e->body  = $content;

            // subject
            $subject = str_replace('#task_topic',$this->order->topic,$subject);
            $subject = str_replace('#order_no',$this->order->id,$subject);
            $e->subject = $subject;
            $this->emails = $this->order->writer->email;

        }

        return $e;
    }

    public function sendEmails()
    {
        $e = $this->_parseEmails();
        try {
            $respond = \Yii::$app->mailer->compose('email', ['result' => $e])
                ->setFrom(['email@MHRWriter.com' => 'MHR Writer'])
                ->setTo($this->emails)
                ->setSubject($e->subject)
                ->send();


        } catch (\Swift_TransportException $st) {
        }

    }
}
