<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property string $id
 * @property string $order_id
 * @property string $customer_id
 * @property string $payment_method
 * @property string $transaction_id
 * @property string $payee_name
 * @property double $payee_amount
 * @property string $fraud_status
 * @property string $payment_date
 * @property string $details
 * @property string $status
 *
 * @property Orders $order
 * @property Customers $customer
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'customer_id', 'payee_amount', 'fraud_status', 'payment_date'], 'required'],
            [['order_id', 'customer_id'], 'integer'],
            [['payment_method', 'fraud_status', 'details', 'status'], 'string'],
            [['payee_amount'], 'number'],
            [['payment_date'], 'safe'],
            [['transaction_id', 'payee_name'], 'string', 'max' => 50],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'customer_id' => 'Customer ID',
            'payment_method' => 'Payment Method',
            'transaction_id' => 'Transaction ID',
            'payee_name' => 'Payee Name',
            'payee_amount' => 'Payee Amount',
            'fraud_status' => 'Fraud Status',
            'payment_date' => 'Payment Date',
            'details' => 'Details',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
}
