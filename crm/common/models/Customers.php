<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $ip
 * @property string $city
 * @property string $region
 * @property string $country
 * @property string $created_at
 *
 * @property Orders[] $orders
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'ip', 'created_at'], 'required'],
            [['created_at'], 'safe'],
            [['name', 'city'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 15],
            [['ip'], 'string', 'max' => 20],
            [['region', 'country'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'ip' => 'Ip',
            'city' => 'City',
            'region' => 'Region',
            'country' => 'Country',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['customer_id' => 'id']);
    }
}
