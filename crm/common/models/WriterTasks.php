<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "writer_tasks".
 *
 * @property string $id
 * @property string $writer_id
 * @property string $order_id
 * @property double $task_rate
 * @property string $status
 * @property string $submission_date
 * @property string $payment_condition
 *
 * @property User $writer
 * @property Orders $order
 */
class WriterTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'writer_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['writer_id', 'order_id', 'task_rate', 'submission_date', 'payment_condition'], 'required'],
            [['writer_id', 'order_id'], 'integer'],
            [['task_rate'], 'number'],
            [['status', 'payment_condition'], 'string'],
            [['submission_date'], 'safe'],
            [['writer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['writer_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'writer_id' => 'Writer ID',
            'order_id' => 'Order ID',
            'task_rate' => 'Task Rate',
            'status' => 'Status',
            'submission_date' => 'Submission Date',
            'payment_condition' => 'Payment Condition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWriter()
    {
        return $this->hasOne(User::className(), ['id' => 'writer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
}
