/*
SQLyog Community
MySQL - 5.6.39-log : Database - ukwriter
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `academic_levels` */

DROP TABLE IF EXISTS `academic_levels`;

CREATE TABLE `academic_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `academic_levels` */

insert  into `academic_levels`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'High School',1,'2018-09-19 08:03:01',NULL),
(2,'Under Graduate',1,'2018-09-19 08:03:12',NULL);

/*Table structure for table `currencies` */

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `currencies` */

insert  into `currencies`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'GBP (£)',1,'2018-09-23 00:01:35','2018-09-23 00:01:35'),
(2,'USD ($)',1,'2018-09-23 00:01:35','2018-09-23 00:01:35'),
(3,'EUR (€)',1,'2018-09-23 00:01:35','2018-09-23 00:01:35'),
(4,'AUD (A$)',1,'2018-09-23 00:01:35','2018-09-23 00:01:35'),
(5,'CAD (C$)',1,'2018-09-23 00:01:35','2018-09-23 00:01:35');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `city` varchar(25) DEFAULT NULL,
  `region` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`email`,`phone`,`ip`,`city`,`region`,`country`,`created_at`) values 
(1,'test','test@test.com','123456789','84.92.60.58','Carnforth','Lancashire','United Kingdom','2018-09-21 14:41:42'),
(2,'john doe','jd@test.com','1234567789','84.92.60.58','Carnforth','Lancashire','United Kingdom','2018-09-22 14:43:01'),
(3,'nania khan','nk@test.com','123456789','84.92.60.58','Carnforth','Lancashire','United Kingdom','2018-09-21 14:41:42'),
(4,'test','test@yopmail.com','123456879','12.12.12.12',NULL,NULL,NULL,'2018-10-06 03:09:14'),
(5,'test','test@yopmail.com','123456879','12.12.12.12',NULL,NULL,NULL,'2018-10-06 03:16:11'),
(6,'test','test@yopmail.com','123456879','12.12.12.12',NULL,NULL,NULL,'2018-10-06 03:17:05');

/*Table structure for table `email_templates` */

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('task_complete','client_intro','order_not_paid','task_assign_to_writer','task_notification_csr') NOT NULL,
  `subject` varchar(250) NOT NULL,
  `body` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `email_templates` */

insert  into `email_templates`(`id`,`type`,`subject`,`body`,`is_active`,`created_at`) values 
(1,'task_complete','Order##order_no - #task_topic','Hi #client_name, \r\n\r\nYour task of #task_topic has been done. Your task and plagiarism report are in attachment.\r\n\r\nPlease check and let me know if you found any issue in it. If there is any mistake, please reply back at-least 8 hours before your actual deadline so that I can process editing request in these 8 hours time. We will edit your work free of cost until you are satisfied.\r\n\r\nMoreover please review the task before submission as we are not providing any passing guarantee. We are just helping our clients to complete the work according to their given initial requirements.\r\n\r\nFeel free to contact back for any further query.\r\n',1,'2018-09-26 08:41:41'),
(2,'client_intro','Welcome to MHR Writer','Hi #client_name,\r\n\r\nThis is #agent_name from MHR Writer. I am assigned to fulfil your request of order ##order_no. I am responsible to deliver back your reports to you.\r\n\r\nMoreover if you have more attachment(s) or details which you think will be helpful for writer to understand your task requirement, please send me over email here.\r\n\r\nIf you have any issue, feel free to contact me back.\r\n\r\nBest Regards\r\n#agent_name\r\nSupport Member\r\nMHR Writer\r\n',1,'2018-09-26 08:26:26'),
(3,'order_not_paid','Order##order_no not paid','Hi #client_name,\r\n\r\nThis is #agent_name from MHR Writer. I am assigned for customer support on your in-complete (not paid) order # #order_no. Please tell me that what problem you were facing on our payment page because I can see from our system that you completes the order form but left the payment page.\r\n\r\nSome of details which you choose on our order form is as below:\r\nTopic: #task_topic\r\nNo# of Pages: #no_pages\r\nSubject Area: #subject_area\r\n\r\nIs there anything problematic in payment processing or any other issue? Please let me know so I can sort out that issue for your convenience.\r\n\r\nFeel free to contact back for any further query.\r\n\r\nBest Regards\r\n#agent_name\r\nSupport Member\r\nMHR Writer\r\n',1,'2018-09-26 08:47:47'),
(4,'task_assign_to_writer','#order_no - #task_topic','Dear #writer_name,\r\n\r\n#agent_name assign #task_topic to you with #writer_deadline',1,'2018-09-26 08:21:21');

/*Table structure for table `fixed_links` */

DROP TABLE IF EXISTS `fixed_links`;

CREATE TABLE `fixed_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `unique_code` varchar(16) NOT NULL,
  `amount` double(11,2) NOT NULL,
  `discount_percentage` double(11,2) DEFAULT NULL,
  `discount_amount` double(11,2) DEFAULT NULL,
  `show_discount_msg` tinyint(1) DEFAULT NULL,
  `status` enum('Not Used','Already Used') NOT NULL DEFAULT 'Not Used',
  `expiry_date` datetime NOT NULL,
  `paper_type_id` int(11) unsigned NOT NULL,
  `service_type_id` int(11) unsigned NOT NULL,
  `urgency_id` int(11) unsigned NOT NULL,
  `quality_level_id` int(11) unsigned NOT NULL,
  `no_pages_id` int(11) unsigned NOT NULL,
  `currency` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`unique_code`),
  KEY `order_id` (`order_id`),
  KEY `paper_type_id` (`paper_type_id`),
  KEY `service_type_id` (`service_type_id`),
  KEY `urgency_id` (`urgency_id`),
  KEY `quality_level_id` (`quality_level_id`),
  KEY `no_pages_id` (`no_pages_id`),
  CONSTRAINT `fixed_links_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `fixed_links_ibfk_2` FOREIGN KEY (`paper_type_id`) REFERENCES `paper_types` (`id`),
  CONSTRAINT `fixed_links_ibfk_3` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`id`),
  CONSTRAINT `fixed_links_ibfk_4` FOREIGN KEY (`urgency_id`) REFERENCES `urgency` (`id`),
  CONSTRAINT `fixed_links_ibfk_5` FOREIGN KEY (`quality_level_id`) REFERENCES `quality_levels` (`id`),
  CONSTRAINT `fixed_links_ibfk_6` FOREIGN KEY (`no_pages_id`) REFERENCES `no_pages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `fixed_links` */

insert  into `fixed_links`(`id`,`title`,`order_id`,`unique_code`,`amount`,`discount_percentage`,`discount_amount`,`show_discount_msg`,`status`,`expiry_date`,`paper_type_id`,`service_type_id`,`urgency_id`,`quality_level_id`,`no_pages_id`,`currency`,`created_at`) values 
(1,'dasdasd',NULL,'Cn3367212iD0S4Dt',29.15,12.00,NULL,1,'Not Used','2018-09-27 06:00:00',1,1,1,1,1,'1','2018-09-23 02:00:19');

/*Table structure for table `no_pages` */

DROP TABLE IF EXISTS `no_pages`;

CREATE TABLE `no_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `no_pages` */

insert  into `no_pages`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'1 page (250 words)',1,'2018-09-19 08:15:36',NULL),
(2,'2 pages (500 words)',1,'2018-09-19 08:15:47',NULL);

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `agent_id` int(10) unsigned DEFAULT NULL,
  `writer_id` int(10) unsigned DEFAULT NULL,
  `status` enum('pending','in_progress','completed','review','delivered','new') NOT NULL DEFAULT 'pending',
  `total_amount` decimal(11,2) NOT NULL,
  `paper_type_id` int(11) unsigned NOT NULL,
  `service_type_id` int(11) unsigned NOT NULL,
  `order_date` datetime NOT NULL,
  `urgency_id` int(11) unsigned NOT NULL,
  `customer_deadline` datetime NOT NULL,
  `quality_id` int(11) unsigned NOT NULL,
  `no_pages_id` int(11) unsigned NOT NULL,
  `subject_area_id` int(11) unsigned NOT NULL,
  `topic` text NOT NULL,
  `acadamic_level_id` int(11) unsigned NOT NULL,
  `writing_style_id` int(11) unsigned NOT NULL,
  `language` varchar(150) DEFAULT NULL,
  `referances` int(11) DEFAULT NULL,
  `instructions` text NOT NULL,
  `payment_method` enum('paypal') NOT NULL,
  `writer_deadline` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `agent_id` (`agent_id`),
  KEY `writer_id` (`writer_id`),
  KEY `paper_type_id` (`paper_type_id`),
  KEY `service_type_id` (`service_type_id`),
  KEY `no_pages_id` (`no_pages_id`),
  KEY `quality_id` (`quality_id`),
  KEY `subject_area_id` (`subject_area_id`),
  KEY `acadamic_level_id` (`acadamic_level_id`),
  KEY `writing_style_id` (`writing_style_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `orders_ibfk_10` FOREIGN KEY (`writing_style_id`) REFERENCES `writing_styles` (`id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`agent_id`) REFERENCES `user` (`id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`writer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`paper_type_id`) REFERENCES `paper_types` (`id`),
  CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`id`),
  CONSTRAINT `orders_ibfk_6` FOREIGN KEY (`no_pages_id`) REFERENCES `no_pages` (`id`),
  CONSTRAINT `orders_ibfk_7` FOREIGN KEY (`quality_id`) REFERENCES `quality_levels` (`id`),
  CONSTRAINT `orders_ibfk_8` FOREIGN KEY (`subject_area_id`) REFERENCES `subjects_areas` (`id`),
  CONSTRAINT `orders_ibfk_9` FOREIGN KEY (`acadamic_level_id`) REFERENCES `academic_levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=321568 DEFAULT CHARSET=latin1;

/*Data for the table `orders` */

insert  into `orders`(`id`,`customer_id`,`agent_id`,`writer_id`,`status`,`total_amount`,`paper_type_id`,`service_type_id`,`order_date`,`urgency_id`,`customer_deadline`,`quality_id`,`no_pages_id`,`subject_area_id`,`topic`,`acadamic_level_id`,`writing_style_id`,`language`,`referances`,`instructions`,`payment_method`,`writer_deadline`) values 
(1,1,2,4,'in_progress',99.25,1,1,'2018-09-21 14:44:30',3,'2018-09-26 14:44:53',1,1,1,'topic dumy...',1,1,'English (UK)',5,'instruction dumy','paypal','2018-10-25 03:57:00'),
(2,2,2,NULL,'in_progress',99.25,2,2,'2018-09-21 14:44:30',2,'2018-09-26 14:44:53',1,2,2,'topic dumy...',2,2,'English (UK)',2,'instruction dumy','paypal','2018-09-29 23:00:00'),
(3,3,2,6,'in_progress',585.00,1,1,'2018-09-21 14:44:30',3,'2018-09-26 14:44:53',1,1,1,'topic dumy',1,1,'English (UK)',5,'instruction dumy','paypal','2018-09-27 21:00:00'),
(321566,3,2,NULL,'pending',585.00,1,1,'2018-09-21 14:44:30',3,'2018-09-26 14:44:53',1,1,1,'topic dumy',1,1,'English (UK)',5,'instruction dumy','paypal','2018-09-27 21:00:00'),
(321567,6,NULL,NULL,'new',12.55,1,1,'2018-10-06 03:17:05',1,'2018-10-07 03:17:06',1,1,1,'from api',1,1,NULL,NULL,'asdasdasd','paypal',NULL);

/*Table structure for table `paper_types` */

DROP TABLE IF EXISTS `paper_types`;

CREATE TABLE `paper_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `paper_types` */

insert  into `paper_types`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'Admission Essay',1,'2018-09-19 07:44:03',NULL),
(2,'Annoted Bibliography',1,'2018-09-19 07:45:00',NULL);

/*Table structure for table `quality_levels` */

DROP TABLE IF EXISTS `quality_levels`;

CREATE TABLE `quality_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `quality_levels` */

insert  into `quality_levels`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'Gold (Equivalent to 1st Class)',1,'2018-09-19 07:51:06',NULL),
(2,'Silver (Equivalent to 2:1)',1,'2018-09-19 07:51:17',NULL);

/*Table structure for table `role_access` */

DROP TABLE IF EXISTS `role_access`;

CREATE TABLE `role_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `controller_id` varchar(150) NOT NULL,
  PRIMARY KEY (`id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

/*Data for the table `role_access` */

insert  into `role_access`(`id`,`role_id`,`controller_id`) values 
(85,1,'pricing'),
(86,1,'sales'),
(87,1,'settings'),
(88,1,'subsidy'),
(89,1,'competitive-pricing'),
(90,1,'cost-price'),
(91,1,'stocks'),
(92,1,'user'),
(93,1,'deal-maker'),
(94,1,'sellers'),
(95,1,'channel-details'),
(96,1,'site'),
(97,7,'pricing'),
(98,7,'subsidy'),
(99,7,'competitive-pricing'),
(100,7,'cost-price'),
(101,7,'stocks'),
(102,7,'deal-maker'),
(103,7,'site'),
(104,4,'pricing'),
(105,4,'sales'),
(106,4,'subsidy'),
(107,4,'competitive-pricing'),
(108,4,'cost-price'),
(109,4,'stocks'),
(110,4,'deal-maker'),
(111,4,'sellers'),
(112,4,'channel-details'),
(113,4,'site'),
(114,6,'pricing'),
(115,6,'sales'),
(116,6,'subsidy'),
(117,6,'competitive-pricing'),
(118,6,'cost-price'),
(119,6,'stocks'),
(120,6,'deal-maker'),
(121,6,'sellers'),
(122,6,'channel-details'),
(123,6,'site'),
(124,5,'pricing'),
(125,5,'sales'),
(126,5,'competitive-pricing'),
(127,5,'cost-price'),
(128,5,'deal-maker'),
(129,5,'site'),
(130,2,'pricing'),
(131,2,'sales'),
(132,2,'competitive-pricing'),
(133,2,'stocks'),
(134,2,'deal-maker'),
(135,2,'site');

/*Table structure for table `service_types` */

DROP TABLE IF EXISTS `service_types`;

CREATE TABLE `service_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `service_types` */

insert  into `service_types`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'Research & Writing',1,'2018-09-19 07:36:29',NULL),
(2,'Editing',1,'2018-09-19 07:36:44',NULL);

/*Table structure for table `subjects_areas` */

DROP TABLE IF EXISTS `subjects_areas`;

CREATE TABLE `subjects_areas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `subjects_areas` */

insert  into `subjects_areas`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'Accounting',1,'2018-09-19 08:03:47',NULL),
(2,'Accounting Law',1,'2018-09-19 08:03:58',NULL);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `payment_method` enum('PayPal','CyberSource','2CO','Others') DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `payee_name` varchar(50) DEFAULT NULL,
  `payee_amount` double(11,2) NOT NULL,
  `fraud_status` enum('Not Received','Fraud Pass','Fraud Fail') NOT NULL,
  `payment_date` datetime NOT NULL,
  `details` text,
  `status` enum('Paid','Not Paid') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`order_id`,`customer_id`,`payment_method`,`transaction_id`,`payee_name`,`payee_amount`,`fraud_status`,`payment_date`,`details`,`status`) values 
(1,3,3,'2CO','206655123425','anna',95.98,'Fraud Pass','2018-09-22 01:38:06',NULL,'Not Paid');

/*Table structure for table `urgency` */

DROP TABLE IF EXISTS `urgency`;

CREATE TABLE `urgency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `urgency` */

insert  into `urgency`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'24 hours',1,'2018-09-14 14:55:20','2018-09-26 14:55:23'),
(2,'48 hours',1,'2018-09-21 14:55:34','2018-09-21 14:55:36'),
(3,'3 days',1,'2018-09-21 14:56:37','2018-09-21 14:56:50'),
(4,'4 days',1,'2018-09-21 14:56:39','2018-09-21 14:56:52'),
(5,'5 days',1,'2018-09-21 14:56:41','2018-09-21 14:56:54'),
(6,'7 days',1,'2018-09-21 14:56:44','2018-09-21 14:56:56'),
(7,'10 days',1,'2018-09-21 14:56:46','2018-09-21 14:56:58');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `bank_acc_details` text COLLATE utf8_unicode_ci,
  `salary` double(11,2) DEFAULT NULL,
  `per_page_rate` double(11,2) DEFAULT NULL,
  `payout_currency` enum('USD','PKR','INR') COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` tinyint(1) DEFAULT '1',
  `timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `force_change_password` tinyint(1) DEFAULT '1',
  `fixed_rate` double(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`role_id`,`username`,`full_name`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`bank_acc_details`,`salary`,`per_page_rate`,`payout_currency`,`availability`,`timezone`,`force_change_password`,`fixed_rate`) values 
(1,1,'admin','tabish','x4jWWxdDtcM2jtAv5PZocLT06dxhd7_u','$2y$13$RWcadUswOiHEw7G1CboNVetZYlW03nfi5U9Je0nn6eQHVoF9fq9P6',NULL,'developer@axleolio.com',10,1482995857,1537360695,'',NULL,55.22,'USD',1,'0',1,NULL),
(2,2,'aina','Aina Hafizalshah','x4jWWxdDtcM2jtAv5PZocLT06dxhd7_u','$2y$13$RWcadUswOiHEw7G1CboNVetZYlW03nfi5U9Je0nn6eQHVoF9fq9P6',NULL,'ucsr@yopmail.com',10,1537365416,1537365416,'asdasdasd',221.00,11.00,'PKR',1,'America/Mexico_City',1,NULL),
(4,3,'writer','writer name','','$2y$13$A/rX2y.ro5bO0P.ASvsDTOl80YLfTIKI.KNE717fQCHpfUaoZs0Iy',NULL,'writer_1@yopmail.com',10,1537514820,1537514820,'test',25.00,12.00,'USD',1,'Europe/London',1,NULL),
(6,3,'writerx','aus writer','','$2y$13$A/rX2y.ro5bO0P.ASvsDTOl80YLfTIKI.KNE717fQCHpfUaoZs0Iy',NULL,'writer_2@yopmail.com',10,1537514820,1537607018,'test',25.00,12.00,'PKR',1,'Europe/London',1,NULL);

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`name`,`created_at`,`updated_at`,`status`) values 
(1,'Super',1482995857,1522919986,1),
(2,'CSR',1506501569,1528162683,1),
(3,'Writer',1506501569,1528162683,1);

/*Table structure for table `writer_tasks` */

DROP TABLE IF EXISTS `writer_tasks`;

CREATE TABLE `writer_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `writer_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `task_rate` double(11,2) NOT NULL,
  `status` enum('Task In Writing','Delivered','Task In Editing','Refund','Cancel From Writer') NOT NULL DEFAULT 'Task In Writing',
  `submission_date` datetime NOT NULL,
  `payment_condition` enum('Per Page','Fixed') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `writer_id` (`writer_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `writer_tasks_ibfk_1` FOREIGN KEY (`writer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `writer_tasks_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `writer_tasks` */

insert  into `writer_tasks`(`id`,`writer_id`,`order_id`,`task_rate`,`status`,`submission_date`,`payment_condition`) values 
(2,6,3,12.00,'Task In Writing','2018-09-27 21:00:00','Per Page'),
(3,4,2,12.00,'Task In Writing','2018-09-29 23:00:00','Per Page'),
(4,4,1,12.00,'Task In Writing','2018-10-25 03:57:00','Per Page');

/*Table structure for table `writing_styles` */

DROP TABLE IF EXISTS `writing_styles`;

CREATE TABLE `writing_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `writing_styles` */

insert  into `writing_styles`(`id`,`name`,`is_active`,`created_at`,`updated_at`) values 
(1,'APA',1,'2018-09-19 04:16:21','2018-09-19 04:29:15'),
(2,'Cambridge',1,'2018-09-19 04:29:46',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
