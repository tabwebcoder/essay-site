function ValidateOrder() {
    var errorMsg = "";
    var emailRegEx = /^[a-zA-Z0-9-\_\.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+$/i;
    var phoneRegEx = /^[0-9]{7,}$/;
    alert(jQuery("[name='full_name']").val().trim());
    if (jQuery("[name='full_name']").val().trim() == "") {
        errorMsg = errorMsg + "Full name must be entered.\n"
    }
    if (jQuery("[name='email_address']").val().trim() == "") {
        errorMsg = errorMsg + "Email address must be entered.\n"
    } else if (jQuery("[name='email_address']").val().search(emailRegEx) == -1) {
        errorMsg = errorMsg + "Invalid email format, best format abc@example.com.\n"
    }
    if (jQuery("[name='contact_phone']").val().trim() == "") {
        errorMsg = errorMsg + "Contact phone must be entered.\n"
    } else if (jQuery("[name='contact_phone']").val().search(phoneRegEx) == -1) {
        errorMsg = errorMsg + "Contact phone can contain numbers only. Must be 7 digits at-least.\n"
    }
    if (jQuery("[name='paper_type']").val().trim() == 0) {
        errorMsg = errorMsg + "Type of paper must be selected.\n"
    }
    if (jQuery("[name='service_type']").val().trim() == 0) {
        errorMsg = errorMsg + "Type of service must be selected.\n"
    }
    if (jQuery("[name='urgency_time']").val().trim() == 0) {
        errorMsg = errorMsg + "Urgency must be selected.\n"
    }
    if (jQuery("[name='quality_level']").val().trim() == 0) {
        errorMsg = errorMsg + "Quality level must be selected.\n"
    }
    if (jQuery("[name='number_of_pages']").val().trim() == 0) {
        errorMsg = errorMsg + "Number of pages must be selected.\n"
    }
    if (jQuery("[name='plag_report']").is(":checked") == !1) {
        errorMsg = errorMsg + "Plagiarism report option must be selected.\n"
    }
    if (jQuery("[name='subject_area']").val().trim() == 0) {
        errorMsg = errorMsg + "Subject area must be selected.\n"
    }
    if (jQuery("[name='paper_topic']").val().trim() == "") {
        errorMsg = errorMsg + "Paper topic must be entered.\n"
    }
    if (jQuery("[name='academic_level']").val().trim() == 0) {
        errorMsg = errorMsg + "Academic level must be selected.\n"
    }
    if (jQuery("[name='writing_style']").val().trim() == 0) {
        errorMsg = errorMsg + "Writing style must be selected.\n"
    }
    if (jQuery("[name='preferred_language']").val().trim() == 0) {
        errorMsg = errorMsg + "Preferred language must be selected.\n"
    }
    if (jQuery("[name='no_of_sources']").val().trim() == 0) {
        errorMsg = errorMsg + "Required references must be selected.\n"
    }
    if (jQuery("[name='any_specification']").val().trim() == "") {
        errorMsg = errorMsg + "Detailed instructions must be entered.\n"
    }
    if (jQuery("[name='reach_us_sources']").val().trim() == 0) {
        errorMsg = errorMsg + "How You Reach Us field must be selected.\n"
    } else if (jQuery("[name='reach_us_sources']").val().trim() == "1" && jQuery("[name='ru_othr_dtl']").val().trim() == "") {
        errorMsg = errorMsg + "Must supply short detail how you reach to us.\n"
    }
    if (jQuery("[name='pay_method']").is(":checked") == !1) {
        errorMsg = errorMsg + "Please select one of the Payment Method options.\n"
    }
    if (jQuery("[name='agree_terms']").prop("checked") == !1) {
        errorMsg = errorMsg + "Please read and accept our Terms & Policies.\n"
    }
    if (jQuery("[name='agree_2co']").prop("checked") == !1) {
        errorMsg = errorMsg + "Please read and accept our payment process method.\n"
    }
    if (file_uploaded === !1) {
        errorMsg = errorMsg + "Your file uploading is not completed.\n"
    }
    if (errorMsg != "") {
        alert(errorMsg);
        return true;
    } else {
        return false;
    }
}