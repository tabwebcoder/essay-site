jQuery(document).ready(function () {

    if (NoCalculate == "undefined" || NoCalculate == "" || NoCalculate != !0) {
        RunOnStart();
        jQuery("[name='paper_type']").change(function () {
            StartLoader();
            paper_type_id = jQuery(this).val();
            SetServiceList(paper_type_id);
            SetUrgencyList(paper_type_id);
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='service_type']").change(function () {
            StartLoader();
            service_type_id = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='urgency_time']").change(function () {
            StartLoader();
            urgency_time_id = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='quality_level']").change(function () {
            StartLoader();
            quality_level_id = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='number_of_pages']").change(function () {
            StartLoader();
            number_of_pages_is = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='currency']").change(function () {
            StartLoader();
            currency_id = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        });
        jQuery("[name='discount_code']").keyup(function () {
            if (jQuery(this).val().toUpperCase() != discount_code) {
                StartLoader();
                discount_code = jQuery(this).val().toUpperCase();
                GetPrice(jQuery(this));
                EndLoader()
            }
        });
        jQuery("[name='plag_report']").change(function () {
            StartLoader();
            plag_report = jQuery(this).val();
            GetPrice(jQuery(this));
            EndLoader()
        })
    }
    jQuery("[name='reach_us_sources']").change(function () {
        var reach_us_val = jQuery(this).val();
        if (reach_us_val == '1') {
            jQuery(".ru_othr_dtl").show()
        } else {
            jQuery(".ru_othr_dtl").hide()
        }
    })
});
function get_paper_types() {
    $.post("http://localhost/outside/essay-site/wp/wp-content/plugins/payment-gateway/api/order_data.php?table_name=paper_types", function (response) {
        var array_data = JSON.parse(response)
        var page_list = '';
        array_response_data = array_data.data;
        for (let i = 0; i < array_response_data.length; i++) {
            page_list += '<option value="' + array_response_data[i].id + '">' + array_response_data[i].name + '</option>';
        }
        jQuery("[name='paper_type']").append(page_list);
    });
}
function get_table_base_data(table, filed_name) {
    $.post("http://localhost/outside/essay-site/wp/wp-content/plugins/payment-gateway/api/order_data.php?table_name=" + table, function (response) {
        var array_data = JSON.parse(response)
        var res_list = '';
        array_response_data = array_data.data;
        for (let i = 0; i < array_response_data.length; i++) {
            res_list += '<option value="' + array_response_data[i].id + '">' + array_response_data[i].name + '</option>';
        }
        jQuery("[name='" + filed_name + "']").append(res_list);
    });
}
function RunOnStart() {
    get_paper_types();
    paper_type_id = jQuery("[name='paper_type']").val();
    SetServiceList(paper_type_id);
    SetUrgencyList(paper_type_id);
    get_table_base_data('quality_levels', 'quality_level');
    get_table_base_data('no_pages', 'number_of_pages');
    get_table_base_data('currencies', 'currency');
    get_table_base_data('subjects_areas', 'subject_area');
    get_table_base_data('academic_levels', 'academic_level');
    get_table_base_data('writing_styles', 'writing_style');
    quality_level_id = jQuery("[name='quality_level']").val();
    number_of_pages_is = jQuery("[name='number_of_pages']").val();
    currency_id = jQuery("[name='currency']").val();
    discount_code = jQuery("[name='discount_code']").val();
    plag_report = jQuery("[name='plag_report']:checked").val();
    GetPrice(jQuery("[name='full_name']"))
}

function SetServiceList(paper_id) {
    return_service_list = '';
    if (paper_id == 0 || paper_id == "") {
        return_service_list = '<option value="0">[Not Selected]</option>'
    } else {
//        for (var service_id in paper_service[paper_id]) {
//            if (service_id != 1) {
//                service_selected = (service_id == service_type_id) ? ' selected="selected"' : '';
//                return_service_list = return_service_list + '<option value="' + service_id + '"' + service_selected + '>' + service_list[service_id] + '</option>'
//            }
//        }
//        service_selected = (service_type_id == 1) ? ' selected="selected"' : '';
//        return_service_list = return_service_list + '<option value="1"' + service_selected + '>' + service_list[1] + '</option>'
        $.post("http://localhost/outside/essay-site/wp/wp-content/plugins/payment-gateway/api/order_data.php?table_name=service_types", function (response) {
            var array_data = JSON.parse(response)
            var return_service_list = '';
            array_response_data = array_data.data;
            for (let i = 0; i < array_response_data.length; i++) {
                return_service_list += '<option value="' + array_response_data[i].id + '">' + array_response_data[i].name + '</option>';
            }
            jQuery("[name='service_type']").append(return_service_list);
        });
    }

    service_type_id = jQuery("[name='service_type']").val()
}
function SetUrgencyList(paper_id) {
    return_urgency_list = '';
    if (paper_id == 0 || paper_id == "") {
        return_urgency_list = '<option value="0">[Not Selected]</option>';
        jQuery("[name='urgency_time']").html(return_urgency_list);
    } else {
//        for (var urgency_id in paper_urgency[paper_id]) {
//            urgency_selected = (urgency_id == urgency_time_id) ? ' selected="selected"' : '';
//            return_urgency_list = return_urgency_list + '<option value="' + urgency_id + '"' + urgency_selected + '>' + urgency_list[urgency_id] + '</option>'
//        }
        $.post("http://localhost/outside/essay-site/wp/wp-content/plugins/payment-gateway/api/order_data.php?table_name=urgency", function (response) {
            var array_data = JSON.parse(response)
            var return_urgency_list = '';
            array_response_data = array_data.data;
            for (let i = 0; i < array_response_data.length; i++) {
                return_urgency_list += '<option value="' + array_response_data[i].id + '">' + array_response_data[i].name + '</option>';
            }
            jQuery("[name='urgency_time']").append(return_urgency_list);
        });
    }

    urgency_time_id = jQuery("[name='urgency_time']").val()
}
function GetPrice(field) {
    SetPrice(!1, !1);
    if (paper_type_id == 0 || paper_type_id == "") {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select type of paper)</div>')
    } else if (service_type_id == 0) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select type of service)</div>')
    } else if (urgency_time_id == 0) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select urgency)</div>')
    } else if (quality_level_id == 0) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select quality level)</div>')
    } else if (number_of_pages_is == 0) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select number of pages)</div>')
    } else if (jQuery("[name='plag_report']").is(":checked") == !1) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat('0.00') + '</div><div class="perpage">(select plagiarism report)</div>')
    } else {
         console.log(paper_service[paper_type_id][service_type_id]);
        price_in_pound = (parseFloat(paper_service[paper_type_id][service_type_id]) + parseFloat(paper_urgency[paper_type_id][urgency_time_id]) + parseFloat(quality_list[quality_level_id]));
        price_per_page = (price_in_pound * parseFloat(currency_list[currency_id].r)).toFixed(2);
        actual_price = (price_per_page * number_of_pages_is).toFixed(2);
        SetPrice(price_per_page, actual_price)
    }
    field.focus()
}
function SetPrice(charge_per_page, actual_charge) {
    jQuery(".discount_view").html('');
    if (actual_charge === !1) {
        actual_charge = 0;
        charge_per_page = 0
    }
    if (plag_report == '1') {
        currency_rate = currency_list[currency_id].r;
        actual_price = actual_charge;
        plag_words_to_pages = (plag_words / 250);
        plag_amount = (number_of_pages_is / plag_words_to_pages);
        net_plag_amount = (Math.ceil(plag_amount) * plag_price);
        actual_charge = (parseFloat(actual_price) + parseFloat(net_plag_amount * currency_rate)).toFixed(2);
        charge_per_page = (actual_charge / number_of_pages_is).toFixed(2)
    }
    if (discount_code != "") {
        if (discount_code == "MHR20") {
            actual_price = actual_charge;
            price_per_page = charge_per_page;
            actual_charge = ((actual_price / 100) * 80).toFixed(2);
            charge_per_page = ((price_per_page / 100) * 80).toFixed(2);
            saving_price = (actual_price - actual_charge).toFixed(2);
            jQuery(".discount_view").html('<div class="form-row"><div class="field-label cost-label">Actual <strong>Cost</strong></div><div class="field-input field-actual-cost"><div class="total">' + currency_list[currency_id].s + ThousandFormat(actual_price) + '</div><div class="perpage">(per page ' + currency_list[currency_id].s + ThousandFormat(price_per_page) + ')</div></div></div><div class="form-row"><div class="field-label cost-label">Discount <strong>Avail</strong></div><div class="field-input field-discount"><div class="total">20% OFF - Saving ' + currency_list[currency_id].s + ThousandFormat(saving_price) + '</div></div></div>')
        } else {
            jQuery(".discount_view").html('<div class="form-row"><div class="field-label"></div><div class="field-input field-invalid">Invalid discount code.</div></div>')
        }

    }
    if (actual_charge !== !1) {
        jQuery(".field-written").html('<div class="total">' + currency_list[currency_id].s + ThousandFormat(actual_charge) + '</div>' + (number_of_pages_is > 1 ? '<div class="perpage">(per page ' + currency_list[currency_id].s + ThousandFormat(charge_per_page) + ')</div>' : ''))
    }
    jQuery(".total_amount").val(actual_charge);
}
function StartLoader() {
    window_height = jQuery(window).height();
    loader_img = jQuery("#Order .loader img");
    loader_img.css('margin-top', '-500px');
    jQuery("#Order .loader").css('height', window_height + 'px');
    jQuery("#Order .loader").show(0);
    loader_img_height = loader_img.height();
    loader_img.css('margin-top', ((window_height - loader_img_height) / 2) + 'px')
}
function EndLoader() {
    setTimeout(function () {
        jQuery("#Order .loader").hide(0)
    }, 200)
}
function ValidateOrder() {
    var errorMsg = "";
    var emailRegEx = /^[a-zA-Z0-9-\_\.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+$/i;
    var phoneRegEx = /^[0-9]{7,}$/;
    if (jQuery("[name='full_name']").val().trim() == "") {
        errorMsg = errorMsg + "Full name must be entered.\n"
    }
    if (jQuery("[name='email_address']").val().trim() == "") {
        errorMsg = errorMsg + "Email address must be entered.\n"
    } else if (jQuery("[name='email_address']").val().search(emailRegEx) == -1) {
        errorMsg = errorMsg + "Invalid email format, best format abc@example.com.\n"
    }
    if (jQuery("[name='contact_phone']").val().trim() == "") {
        errorMsg = errorMsg + "Contact phone must be entered.\n"
    } else if (jQuery("[name='contact_phone']").val().search(phoneRegEx) == -1) {
        errorMsg = errorMsg + "Contact phone can contain numbers only. Must be 7 digits at-least.\n"
    }
    if (jQuery("[name='paper_type']").val().trim() == 0) {
        errorMsg = errorMsg + "Type of paper must be selected.\n"
    }
    if (jQuery("[name='service_type']").val().trim() == 0) {
        errorMsg = errorMsg + "Type of service must be selected.\n"
    }
    if (jQuery("[name='urgency_time']").val().trim() == 0) {
        errorMsg = errorMsg + "Urgency must be selected.\n"
    }
    if (jQuery("[name='quality_level']").val().trim() == 0) {
        errorMsg = errorMsg + "Quality level must be selected.\n"
    }
    if (jQuery("[name='number_of_pages']").val().trim() == 0) {
        errorMsg = errorMsg + "Number of pages must be selected.\n"
    }
    if (jQuery("[name='plag_report']").is(":checked") == !1) {
        errorMsg = errorMsg + "Plagiarism report option must be selected.\n"
    }
    if (jQuery("[name='subject_area']").val().trim() == 0) {
        errorMsg = errorMsg + "Subject area must be selected.\n"
    }
    if (jQuery("[name='paper_topic']").val().trim() == "") {
        errorMsg = errorMsg + "Paper topic must be entered.\n"
    }
    if (jQuery("[name='academic_level']").val().trim() == 0) {
        errorMsg = errorMsg + "Academic level must be selected.\n"
    }
    if (jQuery("[name='writing_style']").val().trim() == 0) {
        errorMsg = errorMsg + "Writing style must be selected.\n"
    }
    if (jQuery("[name='preferred_language']").val().trim() == 0) {
        errorMsg = errorMsg + "Preferred language must be selected.\n"
    }
    if (jQuery("[name='no_of_sources']").val().trim() == 0) {
        errorMsg = errorMsg + "Required references must be selected.\n"
    }
    if (jQuery("[name='any_specification']").val().trim() == "") {
        errorMsg = errorMsg + "Detailed instructions must be entered.\n"
    }
    if (jQuery("[name='reach_us_sources']").val().trim() == 0) {
        errorMsg = errorMsg + "How You Reach Us field must be selected.\n"
    } else if (jQuery("[name='reach_us_sources']").val().trim() == "1" && jQuery("[name='ru_othr_dtl']").val().trim() == "") {
        errorMsg = errorMsg + "Must supply short detail how you reach to us.\n"
    }
    if (jQuery("[name='pay_method']").is(":checked") == !1) {
        errorMsg = errorMsg + "Please select one of the Payment Method options.\n"
    }
    if (jQuery("[name='agree_terms']").prop("checked") == !1) {
        errorMsg = errorMsg + "Please read and accept our Terms & Policies.\n"
    }
    if (jQuery("[name='agree_2co']").prop("checked") == !1) {
        errorMsg = errorMsg + "Please read and accept our payment process method.\n"
    }
    if (file_uploaded === !1) {
        errorMsg = errorMsg + "Your file uploading is not completed.\n"
    }
    if (errorMsg != "") {
        alert(errorMsg);
        return !1
    } else {
        return !0
    }
}