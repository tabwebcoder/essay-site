//var WriteReview=(WriteReview=="yes")?"yes":"no";var SeparateUpload=(SeparateUpload=="yes")?"yes":"no";jQuery(document).ready(function(){file_uploaded=!0;jQuery("[name='displayfiles']").click(function(){jQuery("#fileuploadmanually").click()});jQuery("#fileuploadmanually").change(function(){handleFileUpload(jQuery(this)[0].files,jQuery(this))})});function handleFileUpload(files,obj){var allowed=(WriteReview=="yes")?new Array("jpg","jpeg"):new Array("doc","docx","log","msg","odt","rtf","txt","wpd","wps","csv","pps","ppt","pptx","tar","mid","mp3","ra","wav","wma","3gp","avi","flv","m4v","mov","mp4","mpg","rm","vob","wmv","bmp","gif","jpg","png","psd","tga","tif","tiff","jpeg","ai","eps","svg","pdf","xlr","xls","xlsx","7z","gz","rar","zip","zipx");var fileSizeLimit=(SeparateUpload=="yes")?"125":"25";for(var i=0;i<files.length;i++){var filename=files[i].name;var ext=filename.substring(filename.lastIndexOf(".")+1).toLowerCase();if(allowed.indexOf(ext)=="-1"){if(WriteReview=="yes"){alert("Only JPG or JPEG images are allowed.")}
//else{alert("Unacceptable file extension. Contact us to get solution for this issue.")}}
//else if(parseInt(files[i].size/1024)>(fileSizeLimit*1024)){alert("File larger then "+fileSizeLimit+"MB is not allowed. Contact us to get solution for this issue.")}
//else{var fd=new FormData();fd.append('file',files[i]);fd.append('separate_upload',SeparateUpload);fd.append('write_review',WriteReview);file_uploaded=!1;var status=new createStatusbar(obj,WriteReview);status.setFileNameSize(files[i].name,files[i].size);sendFileToServer(fd,status)}}}
//function createStatusbar(obj,WriteReview){this.statusbar=jQuery('<div class="form-row"><div class="field-label"></div><div class="field-input"><div class="progress-bar"></div></div></div>');this.progressbar=this.statusbar.find('.progress-bar');this.progressis=jQuery('<div class="progress-is"><div></div></div>').appendTo(this.progressbar);this.filenamesize=jQuery('<div class="name-size"><div class="pc"></div><div class="mobile"></div></div>').appendTo(this.progressbar);this.abort=jQuery('<a href="#" class="abort"></a>').appendTo(this.progressbar);this.datafield=jQuery('<input type="hidden" name="" value="" />').appendTo(this.progressbar);obj.parent().parent().after(this.statusbar);this.setFileNameSize=function(name,size){var sizeStr="";var sizeStr=ThousandFormat((size/1024).toFixed(0))+"K";var pc_name=(name.length>32)?name.substr(0,30).trim()+'...':name;var mobile_name=(name.length>20)?name.substr(0,18).trim()+'...':name;this.filenamesize.children('.pc').html(pc_name+' <span>('+sizeStr+')</span>');this.filenamesize.children('.mobile').html(mobile_name+' <span>('+sizeStr+')</span>')}
//this.setProgress=function(progress){if(WriteReview=="yes"){obj.parent().parent().hide()}
//var progressBarWidth=progress*(this.progressis.width()/100);this.progressis.children('div').animate({width:progressBarWidth},10);if(parseInt(progress)>=100){this.progressbar.addClass('completed');file_uploaded=!0}
//else{file_uploaded=!1}}
//this.setAbort=function(jqxhr){var sb=this.statusbar;this.abort.click(function(){jqxhr.abort();file_uploaded=!0;sb.remove();if(WriteReview=="yes"){obj.parent().parent().show()}
//return !1})}
//this.setAbortCompleted=function(data){var sb=this.statusbar;var df=this.datafield;df.attr('name',data.name);df.val(data.value);this.abort.unbind("click").click(function(){cnfrm=confirm('Are you sure to remove this attachment?');if(cnfrm==!0){file_uploaded=!1;removeFileFromServer(data);sb.remove();if(WriteReview=="yes"){obj.parent().parent().show()}}
//return !1})}
//this.doAbort=function(){var sb=this.statusbar.remove();file_uploaded=!0;if(WriteReview=="yes"){obj.parent().parent().show()}}}
//function sendFileToServer(formData,status){var uploadURL=TemplateUrl+"ajax/upload.php";var extraData={};var jqXHR=jQuery.ajax({xhr:function(){var xhrobj=jQuery.ajaxSettings.xhr();if(xhrobj.upload){xhrobj.upload.addEventListener('progress',function(event){var percent=0;var position=event.loaded||event.position;var total=event.total;if(event.lengthComputable){percent=Math.ceil(position/total*100)}
//status.setProgress(percent)},!1)}
//return xhrobj},url:uploadURL,type:"POST",contentType:!1,processData:!1,cache:!1,data:formData,dataType:'json',success:function(data){if(data.success==!0){status.setProgress(100);status.setAbortCompleted(data);if(SeparateUpload=="yes"){alert(data.message)}}
//else{alert(data.message);status.doAbort()}},error:function(xhr,ajaxOptions,thrownError){console.log(thrownError+'\n'+xhr.status+'\n'+ajaxOptions)}});status.setAbort(jqXHR)}
//function removeFileFromServer(rmdata){var uploadURL=TemplateUrl+"ajax/upload-remove.php";jQuery.ajax({url:uploadURL,type:"POST",dataType:'json',data:rmdata,success:function(data){file_uploaded=!0}})}\
var WriteReview = (WriteReview == "yes") ? "yes" : "no";
var SeparateUpload = (SeparateUpload == "yes") ? "yes" : "no";
jQuery(document).ready(function () {
	file_uploaded = !0;
	jQuery("[name='displayfiles']").click(function () {
		jQuery("#fileuploadmanually").click()
	});
	jQuery("#fileuploadmanually").change(function () {
		handleFileUpload(jQuery(this)[0].files, jQuery(this))
	})
});

function handleFileUpload(files, obj) {
	var allowed = (WriteReview == "yes") ? new Array("jpg", "jpeg") : new Array("doc", "docx", "log", "msg", "odt", "rtf", "txt", "wpd", "wps", "csv", "pps", "ppt", "pptx", "tar", "mid", "mp3", "ra", "wav", "wma", "3gp", "avi", "flv", "m4v", "mov", "mp4", "mpg", "rm", "vob", "wmv", "bmp", "gif", "jpg", "png", "psd", "tga", "tif", "tiff", "jpeg", "ai", "eps", "svg", "pdf", "xlr", "xls", "xlsx", "7z", "gz", "rar", "zip", "zipx");
	var fileSizeLimit = (SeparateUpload == "yes") ? "125" : "25";
	for (var i = 0; i < files.length; i++) {
		var filename = files[i].name;
		var ext = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();
		if (allowed.indexOf(ext) == "-1") {
			if (WriteReview == "yes") {
				alert("Only JPG or JPEG images are allowed.")
			} else {
				alert("Unacceptable file extension. Contact us to get solution for this issue.")
			}
		} else if (parseInt(files[i].size / 1024) > (fileSizeLimit * 1024)) {
			alert("File larger then " + fileSizeLimit + "MB is not allowed. Contact us to get solution for this issue.")
		} else {
			var fd = new FormData();
			fd.append('file', files[i]);
			fd.append('separate_upload', SeparateUpload);
			fd.append('write_review', WriteReview);
			file_uploaded = !1;
			var status = new createStatusbar(obj, WriteReview);
			status.setFileNameSize(files[i].name, files[i].size);
			sendFileToServer(fd, status)
		}
	}
}

function createStatusbar(obj, WriteReview) {
	this.statusbar = jQuery('<div class="form-row"><div class="field-label"></div><div class="field-input"><div class="progress-bar"></div></div></div>');
	this.progressbar = this.statusbar.find('.progress-bar');
	this.progressis = jQuery('<div class="progress-is"><div></div></div>').appendTo(this.progressbar);
	this.filenamesize = jQuery('<div class="name-size"><div class="pc"></div><div class="mobile"></div></div>').appendTo(this.progressbar);
	this.abort = jQuery('<a href="#" class="abort"></a>').appendTo(this.progressbar);
	this.datafield = jQuery('<input type="hidden" name="" value="" />').appendTo(this.progressbar);
	obj.parent().parent().after(this.statusbar);
	this.setFileNameSize = function (name, size) {
		var sizeStr = "";
		var sizeStr = ThousandFormat((size / 1024).toFixed(0)) + "K";
		var pc_name = (name.length > 32) ? name.substr(0, 30).trim() + '...' : name;
		var mobile_name = (name.length > 20) ? name.substr(0, 18).trim() + '...' : name;
		this.filenamesize.children('.pc').html(pc_name + ' <span>(' + sizeStr + ')</span>');
		this.filenamesize.children('.mobile').html(mobile_name + ' <span>(' + sizeStr + ')</span>')
	}
	this.setProgress = function (progress) {
		if (WriteReview == "yes") {
			obj.parent().parent().hide()
		}
		var progressBarWidth = progress * (this.progressis.width() / 100);
		this.progressis.children('div').animate({
			width: progressBarWidth
		}, 10);
		if (parseInt(progress) >= 100) {
			this.progressbar.addClass('completed');
			file_uploaded = !0
		} else {
			file_uploaded = !1
		}
	}
	this.setAbort = function (jqxhr) {
		var sb = this.statusbar;
		this.abort.click(function () {
			jqxhr.abort();
			file_uploaded = !0;
			sb.remove();
			if (WriteReview == "yes") {
				obj.parent().parent().show()
			}
			return !1
		})
	}
	this.setAbortCompleted = function (data) {
		var sb = this.statusbar;
		var df = this.datafield;
		df.attr('name', data.name);
		df.val(data.value);
		this.abort.unbind("click").click(function () {
			cnfrm = confirm('Are you sure to remove this attachment?');
			if (cnfrm == !0) {
				file_uploaded = !1;
				removeFileFromServer(data);
				sb.remove();
				if (WriteReview == "yes") {
					obj.parent().parent().show()
				}
			}
			return !1
		})
	}
	this.doAbort = function () {
		var sb = this.statusbar.remove();
		file_uploaded = !0;
		if (WriteReview == "yes") {
			obj.parent().parent().show()
		}
	}
}

function sendFileToServer(formData, status) {
	var uploadURL = TemplateUrl + "ajax/upload.php";
	var extraData = {};
	var jqXHR = jQuery.ajax({
		xhr: function () {
			var xhrobj = jQuery.ajaxSettings.xhr();
			if (xhrobj.upload) {
				xhrobj.upload.addEventListener('progress', function (event) {
					var percent = 0;
					var position = event.loaded || event.position;
					var total = event.total;
					if (event.lengthComputable) {
						percent = Math.ceil(position / total * 100)
					}
					status.setProgress(percent)
				}, !1)
			}
			return xhrobj
		},
		url: uploadURL,
		type: "POST",
		contentType: !1,
		processData: !1,
		cache: !1,
		data: formData,
		dataType: 'json',
		success: function (data) {
			if (data.success == !0) {
				status.setProgress(100);
				status.setAbortCompleted(data);
				if (SeparateUpload == "yes") {
					alert(data.message)
				}
			} else {
				alert(data.message);
				status.doAbort()
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError + '\n' + xhr.status + '\n' + ajaxOptions)
		}
	});
	status.setAbort(jqXHR)
}

function removeFileFromServer(rmdata) {
	var uploadURL = TemplateUrl + "ajax/upload-remove.php";
	jQuery.ajax({
		url: uploadURL,
		type: "POST",
		dataType: 'json',
		data: rmdata,
		success: function (data) {
			file_uploaded = !0
		}
	})
}