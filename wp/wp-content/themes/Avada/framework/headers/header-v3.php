<?php global $smof_data; ?>
<div class="header-v3">
	<?php if($smof_data['header_left_content'] != 'Leave Empty' || $smof_data['header_right_content'] != 'Leave Empty'): ?>
	<div class="header-social">
		<div class="avada-row">
		    
			<div class="col-md-4">
			    <ul  class="top_links"><li class="first"><a title="About the MHR" href="/mhrwriter/about/">About Us</a></li><li><a title="Guarantee on our Services" href="/mhrwriter/guarantee/">Guarantee</a></li><li class="last"><a title="Sitemap of Pages" href="/mhrwriter/sitemap/">Sitemap</a></li></ul>
				<?php
				if($smof_data['header_left_content'] == 'Contact Info') {
					get_template_part('framework/headers/header-info');
				} elseif($smof_data['header_left_content'] == 'Social Links') {
					get_template_part('framework/headers/header-social');
				} elseif($smof_data['header_left_content'] == 'Navigation') {
					get_template_part('framework/headers/header-menu');
				}
				?>
			</div>
			<div class="col-md-4">
			    <div class="call"><span>24/7 UK Toll Free<em>:</em></span><strong>+44 800 048 8966</strong></div>
			    
			</div>
			<div class="col-md-4">
			    <ul class="chat"><li><a title="Chat Using MHR Writer's LiveChat" href="#"  class="livechat"><span class="live">Live</span> <span class="support">Chat</span> <span class="status">ONLINE</span></a></li> </ul>
				<?php
				if($smof_data['header_right_content'] == 'Contact Info') {
					get_template_part('framework/headers/header-info');
				} elseif($smof_data['header_right_content'] == 'Social Links') {
					get_template_part('framework/headers/header-social');
				} elseif($smof_data['header_right_content'] == 'Navigation') {
					get_template_part('framework/headers/header-menu');
				}
				?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<header id="header">
		<div class="avada-row" style="padding-top:<?php echo $smof_data['margin_header_top']; ?>;padding-bottom:<?php echo $smof_data['margin_header_bottom']; ?>;">
			<div class="logo" data-margin-right="<?php echo $smof_data['margin_logo_right']; ?>" data-margin-left="<?php echo $smof_data['margin_logo_left']; ?>" data-margin-top="<?php echo $smof_data['margin_logo_top']; ?>" data-margin-bottom="<?php echo $smof_data['margin_logo_bottom']; ?>" style="margin-right:<?php echo $smof_data['margin_logo_right']; ?>;margin-top:<?php echo $smof_data['margin_logo_top']; ?>;margin-left:<?php echo $smof_data['margin_logo_left']; ?>;margin-bottom:<?php echo $smof_data['margin_logo_bottom']; ?>;">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo $smof_data['logo']; ?>" alt="<?php bloginfo('name'); ?>" class="normal_logo" />
					<?php if($smof_data['logo_retina'] && $smof_data['retina_logo_width'] && $smof_data['retina_logo_height']): ?>
					<?php
					$pixels ="";
					if(is_numeric($smof_data['retina_logo_width']) && is_numeric($smof_data['retina_logo_height'])):
					$pixels ="px";
					endif; ?>
					<img src="<?php echo $smof_data["logo_retina"]; ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo $smof_data["retina_logo_width"].$pixels; ?>;max-height:<?php echo $smof_data["retina_logo_height"].$pixels; ?>; height: auto !important" class="retina_logo" />
					<?php endif; ?>
				</a>
			</div>
			<?php if($smof_data['ubermenu']): ?>
			<nav id="nav-uber">
			<?php else: ?>
			<nav id="nav" class="nav-holder">
			<?php endif; ?>
				<?php get_template_part('framework/headers/header-main-menu'); ?>
			</nav>
			<?php if($smof_data['mobile_menu_design'] == 'modern' && ! $smof_data['ubermenu']): ?>
			<div class="mobile-menu-icons">
				<a href="#" class="fusionicon fusionicon-bars"></a>
				<?php if( class_exists('Woocommerce') && $smof_data['woocommerce_cart_link_main_nav'] ): ?>
				<a href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>" class="fusionicon fusionicon-shopping-cart"></a>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<?php if(tf_checkIfMenuIsSetByLocation('main_navigation') && $smof_data['mobile_menu_design'] == 'classic' && ! $smof_data['ubermenu']): ?>
			<div class="mobile-nav-holder main-menu"></div>
			<?php endif; ?>
		</div>
	</header>			
	<?php if(tf_checkIfMenuIsSetByLocation('main_navigation') && $smof_data['mobile_menu_design'] == 'modern' && ! $smof_data['ubermenu']): ?>
	<div class="mobile-nav-holder main-menu"></div>
	

	
	
	<?php endif; ?>
	<div class="MyMobileMneu" style="display:none;">
		<div class="avada-row">
			<div class="MobileBG">
			<div class="col-xs-8">
			
			<a class="MobileLogo" href="<?php echo home_url(); ?>">
					<img src="<?php echo $smof_data['logo']; ?>" alt="<?php bloginfo('name'); ?>" class="normal_logo" />
					<?php if($smof_data['logo_retina'] && $smof_data['retina_logo_width'] && $smof_data['retina_logo_height']): ?>
					<?php
					$pixels ="";
					if(is_numeric($smof_data['retina_logo_width']) && is_numeric($smof_data['retina_logo_height'])):
					$pixels ="px";
					endif; ?>
					<img src="<?php echo $smof_data["logo_retina"]; ?>" alt="<?php bloginfo('name'); ?>" style="width:<?php echo $smof_data["retina_logo_width"].$pixels; ?>;max-height:<?php echo $smof_data["retina_logo_height"].$pixels; ?>; height: auto !important" class="retina_logo" />
					<?php endif; ?>
				</a>
				
			</div>
			<div class="col-xs-4 ChatBg">
				<div class="">
					<h6 class="LiveText">Live</h6>
					<h6 class="ChatText" style=" color: #FFF;">Chat</h6>
					<h6 class="OnlineText" >ONLINE</h6>
				</div>	 
				
			
			</div>
			</div>

		</div>	
		
		<div class="avada-row"> 
			<div class="BgBottom">
				<div class="col-xs-2 HomeIcon"><i class="fa fa-home" aria-hidden="true"></i></div>
				<div class="col-xs-8">
					<a title="Click Here to Order Now" href="/mhrwriter/order" class="MobileBtn">Order Now </i></a>
				</div>
				<div class="col-xs-2">
					
				</div>
			</div>
		</div>
		
			<div class="avada-row"> 
		<div class="call"><span>24/7 UK Toll Free<em>:</em></span><strong>+44 800 048 8966</strong></div>
		</div>
		
	</div>
</div>