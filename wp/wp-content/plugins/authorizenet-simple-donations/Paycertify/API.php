<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 8:50 PM
 */

namespace Donations\Paycertify;


use Donations\Common\Classes\PaymentResponseObject;

class API {
    private $url;
    private $apiKey;
    private $apiSecret;
    private $errors;

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return mixed
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }



    function __construct($url, $apiKey, $apiSecret) {
        $this->url = $url;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    function sendRequest($params, $headers = array()) {
        $post_string = "";
        foreach( $params as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );


        $request = curl_init($this->url);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request); // close curl object

        $response_array = explode($params["x_delim_char"],$post_response);
        return $this->createResponseObject($response_array);
    }

    function createResponseObject($result) {
        $responses = [
            '1' => PaymentResponseObject::STATUS_SUCCESS,
            '2' => PaymentResponseObject::STATUS_FAILURE,
        ];

        return new PaymentResponseObject($responses[$result[0]], $result[3]);
    }
}