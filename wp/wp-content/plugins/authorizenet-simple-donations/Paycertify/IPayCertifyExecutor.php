<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 28/08/17
 * Time: 11:40 PM
 */

namespace Donations\Paycertify;


interface IPayCertifyExecutor {

    public function execute();
}