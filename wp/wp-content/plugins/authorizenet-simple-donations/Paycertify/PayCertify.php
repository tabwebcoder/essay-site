<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 25/08/17
 * Time: 10:09 PM
 */

namespace Donations\Paycertify;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Classes\PaymentResponseObject;
use Donations\Common\Interfaces\IPayment;
use PayCertify\Gateway;
use PayCertify\ThreeDS;

class PayCertify implements IPayment {
    private $form;
    private $api;
    private $credentials;
    private $environment;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->environment = $environment;
        $this->credentials = $environment == IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        ThreeDS::$api_key = trim(get_option('wds_donation_paycertify_api_key'));
        ThreeDS::$api_secret = trim(get_option('wds_donation_paycertify_api_secret'));
        ThreeDS::$mode = $this->environment;


		$gateWayApiKey = $this->environment == IPayment::ENVIRONMENT_LIVE ? trim(get_option('wds_donation_paycertify_gateway_token'))
		: '7E35FC46-C951-2D2F-FB42-7795F3D24C60';
        Gateway::$api_key = $gateWayApiKey;
        Gateway::$mode = $this->environment;//'test';
        Gateway::configure();

        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_paycertify_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_paycertify_amount');
    }
    public function makePayment() {
        $this->form->setDonationAmount($this->amount);
        $_SESSION['form'] = serialize($this->form);

        $threeDS = new ThreeDS();
        $threeDS->setType('frictionless');

        $threeDS->setCardNumber($this->form->getPaymentForm()->getCreditCardNumber());
        $threeDS->setExpirationMonth(substr($this->form->getPaymentForm()->getExpiry(), 0, 2));
        $threeDS->setExpirationYear("20" . substr($this->form->getPaymentForm()->getExpiry(), 2, 2));
        $threeDS->setAmount($this->amount);
        $threeDS->setTransactionId(time());
        $threeDS->setMessageId($this->form->getDonorEmail());

        $returnUrl = get_site_url()  . "/apply-step-4?3dsecure=true";
        $threeDS->setReturnUrl($returnUrl);

        $enrollment = new Enrollment($threeDS);
        if(false === $enrollment->execute()) {
            $params = [
                'card_exp_month' => $threeDS->getExpirationMonth(),
                'card_exp_year' => $threeDS->getExpirationYear(),
                'transaction_id' => $threeDS->getTransactionId(),
                'pan' => $threeDS->getCardNumber()
            ];
            return $this->makeSecurePayment($params, $this->form, false);
        }
        exit;
    }

    public function makeSubscribe(){

    }

    function getSandboxCredentials() {
        return [
            'username' => trim(get_option('wds_donation_paycertify_api_key')),
            //'api_key' => 'jotr8KHYTNun5JbWfzcTJMzhJAyDIuIS',
            //'api_secret' => 'J2aIDIxnmRBiqD4x37hCKGKj68NFtln4eoCHY0Wb'
        ];
    }

    function getLiveCredentials() {
        return [
            'username' => trim(get_option('wds_donation_paycertify_api_key')),
            //'api_key' => 'SUqD1EUnn0Zb7t9ewylEjsEiqUccsMsc',
            //'api_secret' => 'yKFofpDsgrtdLsh8RtD1M7a4Sc6bPodd0Js50Wn6'
        ];
    }

    function createResponseObject($result) {
        $responses = [
            '1' => PaymentResponseObject::STATUS_SUCCESS,
            '0' => PaymentResponseObject::STATUS_FAILURE,
        ];
        $messages = array();
        foreach($result['message'] as $type => $message) {
            $type = ucfirst(str_replace('_', ' ', $type));
            if(is_array($message)) {
                foreach($message as $m) {
                    $messages[] = $type . ": " . $m;
                }
            } else {
              $messages[] = $type . ": " . $message;
            }
        }

        return new PaymentResponseObject($responses[$result['status']], $messages);
    }

    function verifyEnrollment() {
        $frictionLessCallBack = new \Donations\Paycertify\FrictionLess();
        $response = $frictionLessCallBack->execute();
        return $response;
    }

    function makeSecurePayment($params, $form, $threeDs = false) {
        $transactionData = [
            'type' => 'sale',
            'amount' => $form->getDonationAmount(),
            'currency' => 'USD',
            'card_number' => $params['pan'], //'5454545454545454'
            'expiration_month' => $params['card_exp_month'],
            'expiration_year' => $params['card_exp_year'],
            'name_on_card' => $form->getDonorFirstName() . " " . $form->getDonorLastName(),
            'cvv' => $form->getPaymentForm()->getCvv(),
            'transaction_id' => $params['transaction_id'],
            'billing_city' => $form->getDonorCity(),
            'billing_state' => $form->getDonorState(),
            'billing_country' => 'US',
            'billing_zip' => $form->getDonorZipCode(),
            'shipping_address' => $form->getDonorAddress(),
            'shipping_city' => $form->getDonorCity(),
            'shipping_state' => $form->getDonorState(),
            'shipping_country' => 'US',
            'shipping_zip' => $form->getDonorZipCode(),
            'email' => $form->getDonorEmail(),
            'phone' => $form->getDonorPhone(),
            'order_description' => '',
            'customer_id' => $form->getDonorEmail(),
            'metadata' => '',
        ];

        if($threeDs === true) {
            $transactionData['xid'] = $params['xid'];
            $transactionData['eci'] = $params['eci'];
            $transactionData['cavv'] = $params['cavv'];
        }

        $transaction = new \PayCertify\Gateway\Transaction($transactionData);
        // Submit transaction data to the API
        $transaction->save();
//            $result = array();
        if ($transaction->isSuccess()) {
            /**
             * Store transaction.transaction_id and transaction.amount somewhere, which you'll
             * use to settle this transaction (move from auth to sale). See capture.twig
             * for usage recommendations.
             */
            return $this->createResponseObject(['status' => '1', 'message' => $transaction->getResponse()->Message[0]]);
        } else {
            // Something went wrong, check the gateway_response node.
            return $this->createResponseObject(['status' => '0', 'message' => $transaction->getResponse()->RespMSG]);
        }
    }

    public function getCredentials() {
        return $this->credentials;
    }
}


class Enrollment implements IPayCertifyExecutor {
    private $threeDS;
    function __construct(ThreeDS $threeDS) {
        $this->threeDS = $threeDS;
    }
    public function execute() {
        if($this->threeDS->isCardEnrolled()) {
            $_SESSION['3ds'] = $this->threeDS->getSettings();
            // Start the authentication process!
            $this->threeDS->start();
            $message = "";
            $status = "";

            if($this->threeDS->getClient()->hasError()) {
                // Something went wrong, render JSON for debugging
                echo json_encode($this->threeDS->getClient()->getResponse(), JSON_UNESCAPED_SLASHES);
            } else {
                // All good, render the view
                echo "Verifying transaction .. ";
                echo $this->threeDS->render();
            }
        } else {
            // If the card is not enrolled, you can't do 3DS. Do some action here:
            // you can either block the transaction from happenning or just move forward without 3DS.
            return false;
        }
    }
}

class FrictionLess implements IPayCertifyExecutor {

    public function execute() {
        if(isset($_SESSION['3ds']) && $_SESSION['3ds'] !=null) {
            $payCertify = new PayCertify(new PaymentForm(), get_option('wds_donation_mode'));

            $callback = new \PayCertify\ThreeDS\Callback($_REQUEST, $_SESSION['3ds']);
            if ($callback->canAuthenticate()) {
                // If it gets here, it's a callback from the bank participants for 3DS.
                $response = $callback->authenticate();
                // This action should ALWAYS respond as a JSON with the response for the authentication.
                // This is used to redirect the front-end /checkout page to this action.
                return json_encode($response, JSON_UNESCAPED_SLASHES);
            } elseif ($callback->canExecuteTransaction()) {
                // If 3DS was successful, callback.handshake will contain all data you need. Store it for using
                // later. Also, use the callback credit card data to process your transaction and proceed your
                // regular flow.
                // Clear 3DS session.
                $_SESSION['3ds'] = null;
                if ($callback->isHandshakePresent()) {
                    return json_encode($callback->getData(), JSON_UNESCAPED_SLASHES); // Successful 3DS
                } else {
                    // Move forward without 3DS or retry. Up to you!
                    return false; // Non-successful 3DS
                }
            } else {
                return false;
                // no op: no action needs to be taken.
            }
        }
    }
}