<h2>Card Connect</h2>
<table class="form-table">
    <tr valign="top">
        <th scope="row">Merchat ID:</th>
        <td><input type="text" style="width:50%" name="wds_donation_cardconnect_merchant_id" value="<?php echo get_option('wds_donation_cardconnect_merchant_id'); ?>" placeholder="Merchant ID" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Amount:</th>
        <td><input type="text" style="width:50%" name="wds_donation_cardconnect_amount" value="<?php echo get_option('wds_donation_cardconnect_amount'); ?>" placeholder="Amount" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">API Username:</th>
        <td><input type="text" style="width:50%" name="wds_donation_cardconnect_username" value="<?php echo get_option('wds_donation_cardconnect_username'); ?>" placeholder="Username" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">API Password:</th>
        <td><input type="text" style="width:50%" name="wds_donation_cardconnect_password" value="<?php echo get_option('wds_donation_cardconnect_password'); ?>" placeholder="Password" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Site:</th>
        <td><input type="text" style="width:50%" name="wds_donation_cardconnect_site" value="<?php echo get_option('wds_donation_cardconnect_site'); ?>" placeholder="Site" /></td>
    </tr>
</table>