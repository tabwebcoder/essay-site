<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/11/17
 * Time: 11:44 PM
 */


namespace Donations\CardConnect;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Interfaces\IPayment;

class CardConnect implements IPayment {

    var $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->credentials = $environment == IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();
        $this->api = new API();
        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_cardconnect_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_cardconnect_amount');
    }

    function makePayment() {
        $params = array();
        $params['method'] = 'authorizeTransaction';
        $params['request'] = array(
            'merchid'   => $this->credentials['merchant_id'],
//            'accttyppe' => "VISA",
            'account'   => $this->form->getPaymentForm()->getCreditCardNumber(),
            'expiry'    => $this->form->getPaymentForm()->getExpiry(),
            'cvv2'      => $this->form->getPaymentForm()->getCvv(),
            'amount'    => $this->amount,
            'currency'  => "USD",
            'orderid'   => time(),
            'name'      => $this->form->getDonorFirstName() . " " . $this->form->getDonorLastName(),
            'address'   => $this->form->getDonorAddress(),
            'city'      => $this->form->getDonorCity(),
            'region'    => $this->form->getDonorState(),
            'country'   => "US",
            'postal'    => $this->form->getDonorZipCode(),
            'tokenize'  => "Y",
            'capture'   => "Y",
        );;
        $params['credentials'] = $this->credentials;


        return $this->api->sendRequest($params);
    }

    function getAuth() {

    }

    function getSandboxCredentials() {
        return [
            'url' => 'https://fts.prinpay.com:6443/cardconnect/rest',
            'merchant_id' => '496160873888',
            'username' => 'testing',
            'password' => 'testing123',
            'site' => 'fts'
        ];
    }

    function getLiveCredentials() {
        return [
            'url' => 'https://fts.prinpay.com:6443/cardconnect/rest',
            'merchant_id' => get_option('wds_donation_cardconnect_merchant_id'),
            'username' => get_option('wds_donation_authorizenet_username'),
            'password' => get_option('wds_donation_cardconnect_password'),
            'site' => get_option('wds_donation_cardconnect_site')
        ];
    }

    public function getCredentials() {
        return $this->credentials;
    }

}