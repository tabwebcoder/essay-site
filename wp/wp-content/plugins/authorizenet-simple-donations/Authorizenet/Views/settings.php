<h2>Authorize.Net</h2>
<table class="form-table">
    <tr valign="top">
        <th scope="row">Donation amount:</th>
        <td><input type="text" style="width:50%" name="wds_donation_authorizenet_amount" value="<?php echo get_option('wds_donation_authorizenet_amount'); ?>" placeholder="Authorize.net donation amount" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Authorize Net ID</th>
        <td><input type="text" style="width:50%" name="wds_donation_authorizenet_merchant_id" value="<?php echo get_option('wds_donation_authorizenet_merchant_id'); ?>" placeholder="API Login ID" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Authorize Transaction Key ID</th>
        <td><input type="text" style="width:50%" name="wds_donation_authorizenet_transaction_key" value="<?php echo get_option('wds_donation_authorizenet_transaction_key'); ?>" placeholder="API Login ID" /></td>
    </tr>
</table>