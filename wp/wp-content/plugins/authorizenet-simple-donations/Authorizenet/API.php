<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/08/17
 * Time: 3:23 AM
 */

namespace Donations\Authorizenet;


use Donations\Common\Classes\PaymentResponseObject;

class API {
    private $url;

    function __construct($url) {
        $this->url = $url;
    }

    function sendRequest($params) {
        $post_string = "";
        foreach( $params as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        $request = curl_init($this->url);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request); // close curl object

        $response_array = explode($params["x_delim_char"],$post_response);
        $transaction_id = $response_array[6];
        $last_4         = $response_array[50];
        $approval_code  = $response_array[4];

        return $this->createResponseObject($response_array);
    }

    function createResponseObject($result) {
        $responses = [
            '1' => PaymentResponseObject::STATUS_SUCCESS,
            '2' => PaymentResponseObject::STATUS_FAILURE,
        ];

        return new PaymentResponseObject($responses[$result[0]], $result[3]);
    }

}