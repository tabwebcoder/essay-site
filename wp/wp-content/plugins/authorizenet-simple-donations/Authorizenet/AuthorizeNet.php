<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 25/08/17
 * Time: 10:09 PM
 */


namespace Donations\Authorizenet;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Interfaces;

class AuthorizeNet implements Interfaces\IPayment {
    var $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->credentials = $environment == Interfaces\IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_authorizenet_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_authorizenet_amount');
        $this->api = new API($this->credentials['url']);
    }

    public function getAmount() {
        return $this->amount;
    }

    function makePayment() {
        $post_values = array(

            "x_login"			=> get_option('wds_donation_authorizenet_merchant_id'),
            "x_tran_key"		=> get_option('wds_donation_authorizenet_transaction_key'),

            "x_version"			=> "3.1",
            "x_delim_data"		=> "TRUE",
            "x_delim_char"		=> "|",
            "x_relay_response"	=> "FALSE",

            "x_type"			=> "AUTH_CAPTURE",
            "x_method"			=> "CC",
            "x_card_num"		=> $this->form->getPaymentForm()->getCreditCardNumber(),
            "x_card_code"		=> $this->form->getPaymentForm()->getCvv(),
            "x_exp_date"		=> $this->form->getPaymentForm()->getExpiry(),

            "x_amount"			=> $this->amount,
            "x_description"		=> get_option('wds_processor_description'),

            "x_first_name"		=> $this->form->getDonorFirstName(),
            "x_last_name"		=> $this->form->getDonorLastName(),
            "x_email"			=> $this->form->getDonorEmail(),
            "x_address"			=> $this->form->getDonorAddress(),
            "x_state"			=> $this->form->getDonorState(),
            "x_zip"				=> $this->form->getDonorZipCode()
        );

        if($this->form->getThreeDsForm() != null) {
            $post_values['x_authentication_indicator'] = $this->form->getThreeDsForm()->getEciFlag();
            $post_values['x_cardholder_authentication_value'] = $this->form->getThreeDsForm()->getCavv();

        }

        return $this->api->sendRequest($post_values);

    }

    public function makeSubscribe()
    {
        // if subscribe
        if($this->form->getUpsellSubscription() == '1')
        {

            $subscription = new AuthnetARB( get_option('wds_donation_authorizenet_merchant_id'),
                get_option('wds_donation_authorizenet_transaction_key'));
            // Set subscription information
            $subscription->setParameter('amount', get_option('wds_settings_upsell_amount'));
            $subscription->setParameter('cardNumber', $this->form->getPaymentForm()->getCreditCardNumber());
            $subscription->setParameter('expirationDate', $this->form->getPaymentForm()->getExpiryFormat());
            $subscription->setParameter('firstName', $this->form->getDonorFirstName());
            $subscription->setParameter('lastName', $this->form->getDonorLastName());
            $subscription->setParameter('address',  $this->form->getDonorAddress());
            $subscription->setParameter('city', $this->form->getDonorCity());
            $subscription->setParameter('state', $this->form->getDonorState());
            $subscription->setParameter('zip', $this->form->getDonorZipCode());
            $subscription->setParameter('email', $this->form->getDonorEmail());

            // Set the billing cycle for every three months
            $subscription->setParameter('interval_length', get_option('wds_settings_upsell_subscription_duration'));
            $subscription->setParameter('interval_unit', get_option('wds_settings_upsell_subscription_duration_type'));
            $subscription->setParameter('totalOccurrences', get_option('wds_settings_upsell_subscription_occurr'));
            $subscription->setParameter('startDate', date("Y-m-d", strtotime("+ ".get_option('wds_settings_upsell_subscription_duration')." months")));


            // Create the subscription
            $subscription->createAccount();

            // Check the results of our API call
            if ($subscription->isSuccessful())
            {
                // update user into api
                $params = "users[0][username]=user_".$this->form->getDonorFirstName()."&users[0][firstname]".$this->form->getDonorFirstName()."&users[0][lastname]".$this->form->getDonorLastName()."&users[0][email]=".$this->form->getDonorEmail()."&users[0][auth]=manual&users[0][idnumber]=&users[0][lang]=en&users[0][calendartype]=gregorian&users[0][createpassword]=1";
                $moodleApi = get_option('wds_donation_moodle_website');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $moodleApi);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $response = curl_exec($ch);

                print_r($response);

                curl_close($ch);

            }
            else
            {
                // The subscription was not created!
            }
        }
    }

    function getSandboxCredentials() {
        return [
            'url' => 'https://test.authorize.net/gateway/transact.dll',
            'username' => get_option('wds_donation_authorizenet_merchant_id')
        ];
    }

    function getLiveCredentials() {
        return [
            'url' => 'https://secure.authorize.net/gateway/transact.dll',
            'username' => get_option('wds_donation_authorizenet_merchant_id')
        ];
    }

    public function getCredentials() {
        return $this->credentials;
    }
}