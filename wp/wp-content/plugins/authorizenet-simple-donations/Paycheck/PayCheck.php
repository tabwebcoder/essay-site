<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 25/08/17
 * Time: 10:09 PM
 */

namespace Donations\Paycheck;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Interfaces\IPayment;

class PayCheck implements IPayment {
    private $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->credentials = $environment == IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_paycheck_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_paycheck_amount');


        $this->api = new API($this->credentials['url']);
    }
    public function makePayment() {
        $post_values = array(
            'strStoreID' => trim(get_option('wds_donation_paycheck_merchant_id')),
            'strTransitNum' => trim($this->form->getPaymentForm()->getTransitNumber()),
            'strAccount' => trim($this->form->getPaymentForm()->getAccountNumber()),
            'decAmount' => trim($this->amount),
            'strReference' => rand(pow(10, 5-1), pow(10, 5)-1),
            'strTransID' => $this->form->getDonorEmail() . '-' . time(),
            'strName' => trim($this->form->getDonorFirstName()),
            'strLastName' => trim($this->form->getDonorLastName()),
            'strAddress1' => trim($this->form->getDonorAddress()),
            'strAddress2' => '',
            'strCity' => trim($this->form->getDonorCity()),
            'strState' => trim($this->form->getDonorState()),
            'strZip' => trim($this->form->getDonorZipCode()),
            'strTelephone' => trim($this->form->getDonorPhone())
        );

        return $this->api->sendRequest('SubmitCheck', $post_values);
    }

    public function makeSubscribe(){

    }

    function getSandboxCredentials() {
        return [
            'url' => "https://ws.paysoftintern.com/service.asmx?WSDL",
            'username' => trim(get_option('wds_donation_paycheck_merchant_id'))
        ];
    }

    function getLiveCredentials() {
        return [
            'url' => "https://ws.paysoftintern.com/service.asmx?WSDL",
            'username' => trim(get_option('wds_donation_paycheck_merchant_id'))
        ];
    }

    public function getCredentials() {
        return $this->credentials;
    }

    public function getAmount()
    {
        return $this->amount;
    }
}