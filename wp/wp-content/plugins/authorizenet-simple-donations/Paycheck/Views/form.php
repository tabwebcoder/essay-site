<?php use \Donations\Common\Utils\Helper;?>
<div class="form-group paymf1"><label style="width:50%">Checking Account Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_account_number" value="<?php echo Helper::form_value("donor_account_number");?>" id="donor_account_number"> </div>
<div class="form-group paymf1"><label style="width:50%">Routing/Transit Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_transit_number" value="<?php echo Helper::form_value("donor_transit_number");?>" id="donor_transit_number"> </div>
