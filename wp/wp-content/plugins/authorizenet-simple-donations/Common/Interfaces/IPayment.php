<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 25/08/17
 * Time: 10:08 PM
 */


namespace Donations\Common\Interfaces;

interface IPayment {
    const ENVIRONMENT_SANDBOX = "test";
    const ENVIRONMENT_LIVE = "live";

    /*const PAYMENT_GATEWAY_PAYCHECK = "paycheck";
    const PAYMENT_GATEWAY_PAYCERTIFY = "paycertify";
    const PAYMENT_GATEWAY_AUTHORIZENET = "authorizenet";
    const PAYMENT_GATEWAY_EPIN = "epin";
    const PAYMENT_GATEWAY_CARDCONNECT = "cardconnect";*/
    const PAYMENT_GATEWAY_PAYPAL = 'paypal';
    const PAYMENT_GATEWAY_CYBERSOURCE = 'cybersource';

    public function getAmount();

    function makePayment();
    function makeSubscribe();

    function getSandboxCredentials();

    function getLiveCredentials();

    function getCredentials();
}