<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 7:52 PM
 */

namespace Donations\Common\Interfaces;


interface IForm {
    function isValid();
    function getErrorMessages();
}