<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 8:37 PM
 */

namespace Donations\Common\Classes;

use Donations\PayPal\PayPal;
use Donations\Common\Interfaces\IPayment;


class PaymentGatewayFactory
{
    var $paymentMethod;

    function getPaymentGateway($form, $environment, $ccPaymentProcessor)
    {
        //get type of payment gateway e.g: credit card or checking account
        if ($form['pay_method'] == IPayment::PAYMENT_GATEWAY_PAYPAL) {
            return new PayPal($form, $environment);
        }
    }
}