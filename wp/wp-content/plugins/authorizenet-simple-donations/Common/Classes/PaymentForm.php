<?php

namespace Donations\Common\Classes;

use Donations\Common\Interfaces\IForm;
use Donations\Common\Utils\Helper;

/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 7:29 PM
 */
class CreditCardForm implements IForm
{
    private $creditCardNumber;
    private $cvv;
    private $expiry;
    private $expiryFormat;

    private $errorMessage;

    function __construct()
    {
        $this->creditCardNumber = $_POST['donor_card_number'];
        $this->cvv = $_POST['donor_cvv'];
        $this->expiry = $_POST['donor_card_expiry_month'] . $_POST['donor_card_expiry_year'];
        $this->expiryFormat = '20'.$_POST['donor_card_expiry_year'] .'-'.$_POST['donor_card_expiry_month'];
    }

    private function validate()
    {
        $valid = true;
        if ($this->creditCardNumber == '') {
            $valid = false;
            $this->errorMessage[] = 'Credit card number is required.';
        } else {
            if (!is_numeric($this->creditCardNumber)) {
                $valid = false;
                $this->errorMessage[] = 'Credit card number can only be numeric';
            }

            if (strlen($this->creditCardNumber) > 16) {
                $valid = false;
                $this->errorMessage[] = 'Credit card number cannot have more than 15 digits';
            }
        }


        /*if( $this->cvv == ''){
            $valid = false;
            $this->errorMessage[] = 'CVV number is required.';
        }
        else {
            if(!is_numeric($this->cvv)) {
                $valid = false;
                $this->errorMessage[] = 'CVV number can only be numeric';
            }

            if(strlen($this->cvv) > 3) {
                $valid = false;
                $this->errorMessage[] = 'CVV number cannot have more than 3 digits';
            }
        }*/

        if ($this->expiry == '') {
            $valid = false;
            $this->errorMessage[] = 'Expiry date is required.';
        } else {
            if (!is_numeric($this->expiry)) {
                $valid = false;
                $this->errorMessage[] = 'Expiry date can only be numeric';
            }

            if (strlen($this->expiry) > 4) {
                $valid = false;
                $this->errorMessage[] = 'Expiry date cannot have more than 4 digits';
            }
        }

        return $valid;
    }

    function isValid()
    {
        return $this->validate();
    }

    function getErrorMessages()
    {
        return $this->errorMessage;
    }

    /**
     * @return mixed
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * @return mixed
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * @return mixed
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    public function getExpiryFormat()
    {
        return $this->expiryFormat;
    }


}

class ThreeDsForm
{
    private $cavv;
    private $eciFlag;
    private $xid;
    private $paresStatus;
    private $enrolled;

    public function __construct()
    {
        $this->cavv = $_POST['cavv'];
        $this->eciFlag = $_POST['eciFlag'];
        $this->xid = $_POST['xid'];
        $this->paresStatus = $_POST['paresStatus'];
        $this->enrolled = $_POST['enrolled'];
    }

    /**
     * @return mixed
     */
    public function getCavv()
    {
        return $this->cavv;
    }

    /**
     * @param mixed $cavv
     */
    public function setCavv($cavv)
    {
        $this->cavv = $cavv;
    }

    /**
     * @return mixed
     */
    public function getEciFlag()
    {
        return $this->eciFlag;
    }

    /**
     * @param mixed $eciFlag
     */
    public function setEciFlag($eciFlag)
    {
        $this->eciFlag = $eciFlag;
    }

    /**
     * @return mixed
     */
    public function getXid()
    {
        return $this->xid;
    }

    /**
     * @param mixed $xid
     */
    public function setXid($xid)
    {
        $this->xid = $xid;
    }

    /**
     * @return mixed
     */
    public function getParesStatus()
    {
        return $this->paresStatus;
    }

    /**
     * @param mixed $paresStatus
     */
    public function setParesStatus($paresStatus)
    {
        $this->paresStatus = $paresStatus;
    }

    /**
     * @return mixed
     */
    public function getEnrolled()
    {
        return $this->enrolled;
    }

    /**
     * @param mixed $enrolled
     */
    public function setEnrolled($enrolled)
    {
        $this->enrolled = $enrolled;
    }


}

class CheckingAccountForm implements IForm
{
    private $accountNumber;
    private $transitNumber;
    private $checkNumber;

    private $errorMessage;

    function __construct()
    {
        $this->accountNumber = $_POST['donor_account_number'];
        $this->transitNumber = $_POST['donor_transit_number'];
        $this->checkNumber = $_POST['donor_check_number'];
    }

    private function validate()
    {
        $valid = true;
        if ($this->accountNumber == '') {
            $valid = false;
            $this->errorMessage[] = 'Account Number is required.';
        } else {
            if (!is_numeric($this->accountNumber)) {
                $valid = false;
                $this->errorMessage[] = 'Account number can only be numeric';
            }

            if (strlen($this->accountNumber) > 17) {
                $valid = false;
                $this->errorMessage[] = 'Account number cannot have more than 9 digits';
            }
        }


        if ($this->transitNumber == '') {
            $valid = false;
            $this->errorMessage[] = 'Routing Number is required.';
        } else {
            if (!is_numeric($this->transitNumber)) {
                $valid = false;
                $this->errorMessage[] = 'Account number can only be numeric';
            }

            if (strlen($this->transitNumber) != 9) {
                $valid = false;
                $this->errorMessage[] = 'Routing number can only be 9 digits.';
            }
        }

        /*if( $this->checkNumber == ''){
            $valid = false;
            $this->errorMessage[] = 'Check Number is required.';
        }
        else {
            if(!is_numeric($this->checkNumber)) {
                $valid = false;
                $this->errorMessage[] = 'Account number can only be numeric';
            }

            if(strlen($this->checkNumber) > 9) {
                $valid = false;
                $this->errorMessage[] = 'Account number cannot have more than 9 digits';
            }
        }*/

        return $valid;
    }

    function isValid()
    {
        return $this->validate();
    }

    function getErrorMessages()
    {
        return $this->errorMessage;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return mixed
     */
    public function getTransitNumber()
    {
        return $this->transitNumber;
    }

    /**
     * @return mixed
     */
    public function getCheckNumber()
    {
        return $this->checkNumber;
    }


}

class PaymentForm implements IForm
{

    const GATEWAY_CREDITCARD = "credit-card";
    const GATEWAY_CHECKINGACCOUNT = "checking-account";

    private $donorFirstName;
    private $donorLastName;
    private $donorAddress;
    private $donorZipCode;
    private $donorState;
    private $donorEmail;
    private $donationAmount;
    private $donorPaymentMethod;
    private $paymentForm;
    private $donorCity;
    private $donorPhone;
    private $threeDsForm;
    private $upsellProduct;
    private $upsellSubscribe;
    private $upsellSubscribeDuration;

    private $errorMessage;

    function __construct()
    {
        $this->donorFirstName = $_POST['donor_firstname'];
        $this->donorLastName = $_POST['donor_lastname'];
        $this->donorAddress = $_POST['donor_address'];
        $this->donorPhone = $_POST['donor_phone'];
        $this->donorZipCode = $_POST['donor_zipcode'];
        $this->donorCity = $_POST['donor_city'];
        $this->donorState = $_POST['donor_state'];
        $this->donorEmail = $_POST['donor_email'];
//        $this->donationAmount       = $_POST['donation_amount'];
        $this->donorPaymentMethod = $_POST['donor_payment_method'];
        $this->upsellProduct = isset($_POST['upsell-product']) ? $_POST['upsell-product'] : null;
        $this->upsellSubscribe = isset($_POST['donor_subscribe']) ? $_POST['donor_subscribe'] : null;
        $this->upsellSubscribeDuration = isset($_POST['donor_subscribe_duration']) ? $_POST['donor_subscribe_duration'] : null;

        if ($this->donorPaymentMethod == PaymentForm::GATEWAY_CREDITCARD)
            $this->paymentForm = new CreditCardForm();
        else if ($this->donorPaymentMethod == PaymentForm::GATEWAY_CHECKINGACCOUNT)
            $this->paymentForm = new CheckingAccountForm();

        if ($this->isThreeDsAvailable()) {
            $this->threeDsForm = new ThreeDsForm();

        }
    }

    private function isThreeDsAvailable()
    {
        return
            (isset($_POST['cavv']) && $_POST['cavv'] != null && $_POST['cavv'] != '')
            &&
            (isset($_POST['eciFlag']) && $_POST['eciFlag'] != null && $_POST['eciFlag'] != '')
            &&
            (isset($_POST['xid']) && $_POST['xid'] != null && $_POST['xid'] != '');
    }

    public function isValid()
    {
        return $this->validate() && $this->paymentForm->isValid();
    }

    private function validate()
    {
        $valid = true;
        if ($this->donorPhone == '') {
            $valid = false;
            $this->errorMessage[] = 'Phone Number is required.';
        }

        if ($this->donorFirstName == '') {
            $valid = false;
            $this->errorMessage[] = 'First Name is required.';
        }

        if ($this->donorLastName == '') {
            $valid = false;
            $this->errorMessage[] = 'Last Name is required.';
        }

        if ($this->donorAddress == '') {
            $valid = false;
            $this->errorMessage[] = 'Address is required.';
        }

        if ($this->donorCity == '') {
            $valid = false;
            $this->errorMessage[] = 'City is required.';
        }

        if ($this->donorZipCode == '') {
            $valid = false;
            $this->errorMessage[] = 'Zip code is required.';
        } else {
            if (strlen($this->donorZipCode) != 5) {
                $valid = false;
                $this->errorMessage[] = 'Zip code must be 5 digits only.';
            }
        }

        if ($this->donorState == '') {
            $valid = false;
            $this->errorMessage[] = 'State is required.';
        } else {
            if (strlen($this->donorState) > 2) {
                $valid = false;
                $this->errorMessage[] = 'State can have maximum two characters e.g: NY';
            }
        }

        if ($this->donorEmail == '') {
            $valid = false;
            $this->errorMessage[] = 'Email is required.';
        } else {
            if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $this->donorEmail)) {
                $valid = false;
                $this->errorMessage[] = 'Invalid Email.';
            } else {

                $testEmails = explode(',', str_replace(' ', '', get_option('wds_test_emails')));
                if (!in_array($this->donorEmail, $testEmails)) {
                    $posts = get_posts(array(
                        'post_type' => 'wds_donation',
                        'meta_query' => array(
                            array(
                                'key' => 'donor_email',
                                'value' => $this->donorEmail,
                                'compare' => 'LIKE'
                            ),
                            array(
                                'key' => 'donor_phone',
                                'value' => $this->donorPhone,
                                'compare' => 'LIKE'
                            ),
                        )
                    ));

                    $isUniqueInCRM = Helper::checkUniqueEmailonCRM($this->donorEmail);


                    if (count($posts) > 0 || $isUniqueInCRM == "false") {
                        $valid = false;
                        $this->errorMessage[] = 'A donation has already been made using this email address';
                    }
                }
            }
        }

        return $valid;
    }

    public function getErrorMessages()
    {
        return array_merge((array)$this->errorMessage, (array)$this->paymentForm->getErrorMessages());
    }

    /**
     * @return mixed
     */
    public function getDonorFirstName()
    {
        return $this->donorFirstName;
    }

    /**
     * @return mixed
     */
    public function getDonorLastName()
    {
        return $this->donorLastName;
    }

    /**
     * @return mixed
     */
    public function getDonorAddress()
    {
        return $this->donorAddress;
    }

    /**
     * @return mixed
     */
    public function getDonorZipCode()
    {
        return $this->donorZipCode;
    }

    /**
     * @return mixed
     */
    public function getDonorState()
    {
        return $this->donorState;
    }

    /**
     * @return mixed
     */
    public function getDonorEmail()
    {
        return $this->donorEmail;
    }

    /**
     * @return mixed
     */
    public function getDonationAmount()
    {
        return $this->donationAmount;
    }

    /**
     * @return mixed
     */
    public function getDonorPaymentMethod()
    {
        return $this->donorPaymentMethod;
    }

    /**
     * @return CheckingAccountForm
     */
    public function getPaymentForm()
    {
        return $this->paymentForm;
    }

    /**
     * @return mixed
     */
    public function getDonorCity()
    {
        return $this->donorCity;
    }

    /**
     * @return mixed
     */
    public function getDonorPhone()
    {
        return $this->donorPhone;
    }

    /**
     * @param mixed $donationAmount
     */
    public function setDonationAmount($donationAmount)
    {
        $this->donationAmount = $donationAmount;
    }

    /**
     * @return ThreeDsForm
     */
    public function getThreeDsForm()
    {
        return $this->threeDsForm;
    }

    /**
     * @param ThreeDsForm $threeDsForm
     */
    public function setThreeDsForm($threeDsForm)
    {
        $this->threeDsForm = $threeDsForm;
    }

    /**
     * @return null
     */
    public function getUpsellProduct()
    {
        return $this->upsellProduct;
    }

    /**
     * @param null $upsellProduct
     */
    public function setUpsellProduct($upsellProduct)
    {
        $this->upsellProduct = $upsellProduct;
    }

    /**
     * @return null
     */
    public function getUpsellSubscription()
    {
        return $this->upsellSubscribe;
    }

    /**
     * @param null $upsellProduct
     */
    public function setUpsellSubscription($upsellSubs)
    {
        $this->upsellSubscribe = $upsellSubs;
    }

    /**
     * @return null
     */
    public function getUpsellSubscriptionDuration()
    {
        return $this->upsellSubscribeDuration;
    }

    /**
     * @param null $upsellProduct
     */
    public function setUpsellSubscriptionDuration($upsellSubsDur)
    {
        $this->upsellSubscribeDuration = $upsellSubsDur;
    }


}