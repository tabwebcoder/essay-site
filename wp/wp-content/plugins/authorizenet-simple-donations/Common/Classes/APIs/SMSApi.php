<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/03/16
 * Time: 7:03 PM
 */

namespace Donations\Common\Classes\APIs;

use \StdClass;

class SMSApi {

    private $api_key;
    private $registeredNumber;
    private $url;

    const ERROR = "error";
    const SUCCESS = "success";

    public function __construct()
    {
        $this->access_key = "AC24f6951c46fef70e37064464a8aae163";
        $this->api_key = "c769b5345d7de94435cfa3b94c53fab1";
        $this->url = "https://api.twilio.com/2010-04-01/Accounts/AC24f6951c46fef70e37064464a8aae163/Messages.json";
    }

    public function sendMessage($toNumber, $message) {
        $postParams = array(
            "To" => $this->formatNumber($toNumber),
            "Body" => urlencode($message),
            "From" => "+14159662451"
        );


        return $this->call($postParams);
    }

    private function formatNumber($number) {
        $number = "+1" . $number;
        $number = str_replace('-', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);

        return $number;
    }


    function call($params = array()) {

        $result = new stdClass();
        //next example will insert new conversation

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_USERPWD, $this->access_key . ":" . $this->api_key);


        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);

        $decoded = \GuzzleHttp\json_decode($response);

        if ($decoded == null) {
            $result->status = self::ERROR;
            $result->message = "Unable to send SMS";
        }

        if($decoded->status == "queued") {
            $result->status = self::SUCCESS;
            $result->message = "SMS Sent";
        }
        else
        {
            $result->status = $decoded->status;
            $result->message = $decoded->message;
        }

        return $result;
    }
}