<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/03/16
 * Time: 7:03 PM
 */

namespace Donations\Common\Classes\APIs;

use \StdClass;

class OldSMSAPI {

    private $api_key;
    private $registeredNumber;
    private $url;

    const ERROR = "error";
    const SUCCESS = "success";

    public function __construct()
    {
        $this->access_key = "3bbdf07d9e2aa42de13ce7ed7af1104e86b95c31e0dc3310d610efce73b1efc9";
        $this->secret_key = "314dd967ac599c1a56a0758bd7b4e6444d5f1f37d5433df46416be4999792f49";
        $this->registeredNumber = "2106640933";
        $this->url = "https://secure.dialogtech.com/ibp_api.php";
    }

    public function sendMessage($toNumber, $message) {
        $getParams = array(
            "action" => "sms.send_message",
            "to" => $this->formatNumber($toNumber),
            "from" => $this->registeredNumber,
            "message" => urlencode($message)
        );

        return $this->call($getParams);
    }

    private function formatNumber($number) {
        $number = "+1" . $number;
        $number = str_replace('-', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);

        return $number;
    }


    function call($getParams = array(),$postParams = array()) {
        $result = new stdClass();
        //next example will insert new conversation
        $service_url = $this->url . "?access_key=" .  $this->access_key . "&secret_access_key=" . $this->secret_key;
        foreach($getParams as $getParamKey => $getParamValue)
        {
            $service_url .= "&" . $getParamKey . "=" . $getParamValue;
        }

        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $service_url);

        $curl_response = curl_exec($ch);
        if ($curl_response === false) {
            $info = curl_getinfo($ch);
            curl_close($ch);
            $result->status = self::ERROR;
            $result->message = "Unable to send SMS";
        }

        curl_close($ch);
        $decoded = simplexml_load_string($curl_response);

        if ($decoded == null) {
            $result->status = self::ERROR;
            $result->message = "Unable to send SMS";
        }

        if($decoded->result == "success") {
            $result->status = self::SUCCESS;
            $result->message = "SMS Sent";
        }
        else
        {
            $result->status = $decoded->result;
            $result->message = $decoded->result_description;
        }

        return $result;
    }
}