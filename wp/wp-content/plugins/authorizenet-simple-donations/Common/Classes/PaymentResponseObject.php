<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/08/17
 * Time: 2:59 AM
 */

namespace Donations\Common\Classes;


class PaymentResponseObject {
    private $status;
    private $messages;

    const STATUS_SUCCESS = 1;
    const STATUS_FAILURE = 0;

    function __construct($status, $messages = array())
    {
        $this->status = $status;
        $this->messages = $messages;
    }


    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }
}