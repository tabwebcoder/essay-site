<div class="wrap">
    <h1>Donation Settings</h1>

    <?php
    $pluginDir = ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/';

    use \Donations\Common\Interfaces\IPayment;

    if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true'):
        echo '<div id="setting-error-settings_updated" class="updated settings-error">
<p><strong>Settings saved.</strong></p></div>';
    endif;
    ?>

    <form method="post" action="options.php">
        <?php settings_fields('wds-settings-group'); ?>
        <?php do_settings_sections('wds-settings-group'); ?>
        <h2>General Settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Payment Processing Methods</th>
                <td>
                    <input type="checkbox" name="wds_credit_card_enabled" value="1" <?php checked( 1 == get_option('wds_credit_card_enabled') ); ?>> Credit Card
                    <input type="checkbox" name="wds_checking_account_enabled" value="1" <?php checked( 1 == get_option('wds_checking_account_enabled')); ?>> Checking Account
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Mode(Live/Test Sandbox)</th>
                <td>
                    <select name="wds_donation_mode" />
                    <option value="<?php echo IPayment::ENVIRONMENT_LIVE;?>" <?php if( get_option('wds_donation_mode') == IPayment::ENVIRONMENT_LIVE ): echo 'selected'; endif;?> >Live</option>
                    <option value="<?php echo IPayment::ENVIRONMENT_SANDBOX;?>" <?php if( get_option('wds_donation_mode') == IPayment::ENVIRONMENT_SANDBOX ): echo 'selected'; endif;?> >Test/Sandbox</option>
                    </select>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Credit Card Processor</th>
                <td>
                    <select name="wds_payment_gateway" />
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_CYBERSOURCE ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_CYBERSOURCE ): echo 'selected'; endif;?> >Cyber Source</option>
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_PAYPAL ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_PAYPAL ): echo 'selected'; endif;?> >PayPal</option>
                    </select>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Thank You Message</th>
                <td><input type="text" style="width:50%" name="wds_thankyou_message" value="<?php echo get_option('wds_thankyou_message'); ?>" placeholder="Thank you message visible to Donor after donation" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Processor Description</th>
                <td><input type="text" style="width:50%" name="wds_processor_description" value="<?php echo get_option('wds_processor_description'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Test Emails <span style="font-size: 9px;">(comma seperated)</span></th>
                <td><input type="text" style="width:50%" name="wds_test_emails" value="<?php echo get_option('wds_test_emails'); ?>" /></td>
            </tr>
        </table>
        <h2>Cyber Source Credentials</h2>
        <table class="form-table">

            <tr valign="top">
                <th scope="row">Access Key</th>
                <td><input type="text" style="width:50%" name="wds_settings_cs_access_key" value="<?php echo get_option('wds_settings_cs_access_key'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Profile ID</th>
                <td><input type="text" style="width:50%" name="wds_settings_cs_profile_id" value="<?php echo get_option('wds_settings_cs_profile_id'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Secret Key</th>
                <td><input type="text" style="width:50%" name="wds_settings_cs_secret_key" value="<?php echo get_option('wds_settings_cs_secret_key'); ?>" /></td>
            </tr>
        </table>
        <h2>Paypal Credentials</h2>
        <table class="form-table">
        <tr valign="top">
            <th scope="row">Client ID</th>
            <td><input type="text" style="width:50%" name="wds_settings_pp_client_id" value="<?php echo get_option('wds_settings_pp_client_id'); ?>" /></td>
        </tr>
        <tr valign="top">
            <th scope="row">Secret</th>
            <td><input type="text" style="width:50%" name="wds_settings_pp_secret" value="<?php echo get_option('wds_settings_pp_secret'); ?>" /></td>
        </tr>
        <tr valign="top">
            <th scope="row">Thankyou URL</th>
            <td><input type="text" style="width:50%" name="wds_settings_pp_url" value="<?php echo get_option('wds_settings_pp_url'); ?>" /></td>
        </tr>
        </table>
        <?php submit_button(); ?>

    </form>
    <p style="font-weight:bold">Use shortcode [wds_donate]</p>
</div>
