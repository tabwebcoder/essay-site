<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 9/16/2018
 * Time: 5:16 AM
 */
require_once ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/Common/Utils/Helper.php';

?>
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<form id="payment_confirmation"  action="https://secureacceptance.cybersource.com/pay" method="post"/>
<?php
//https://testsecureacceptance.cybersource.com/pay
foreach($_REQUEST as $name => $value) {
    $params[$name] = $value;
}
?>
<?php
foreach($params as $name => $value) {
    if($name == 'submit')
        continue;
    echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
}?>

<?php echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . \Donations\Common\Utils\Helper::sign($params) . "\"/>\n";
?>

</form>
<script type="text/javascript">

    $(document).ready(function(){
       $("form#payment_confirmation").submit();
    });

</script>
