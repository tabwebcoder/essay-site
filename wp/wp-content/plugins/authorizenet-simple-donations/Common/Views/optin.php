<style type="text/css">@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');

@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700');

@font-face {
    font-family:'Open Sans', sans-serif;
    src: url(OpenSans-Regular.ttf);
}
@font-face {
    font-family:'Raleway', sans-serif;
    src: url(Raleway-Regular.ttf);
}
</style>
<table align="center" border="0" class="mid" width="830px" style="background-image: url(http://usjobshelpcenter.com/wp-content/uploads/2017/11/missionBg.jpg); background-size: cover;">
	<tbody>
		<tr>
			<td bgcolor="#202F64" style="padding: 30px 20px;border-bottom: 5px solid #ff0023;"><img src="https://www.postalhiringcenters.com/wp-content/uploads/2017/02/logo-footer.png" style="
" /></td>
		</tr>
		<tr>
			<td style="font-size: 33px;color: #1f4394;font-family: 'Raleway', sans-serif;line-height: 40px;font-weight: bold;text-align:center;padding-top: 46px;">
			US Postal “Job Preferences” Confirmation
            <p style="color: #000;font-size: 24px;text-align: center;font-weight: 700;font-family: 'Raleway', sans-serif;margin: 0px 0px;">Thank you for your interest in a career with the USPS</p>
            </td>
		</tr>
	
		<tr style="padding: 35px 0px;display: block;">
			<td >
			  <p style="font-size: 24px;line-height: 30px;color: red;font-weight: 600;text-align: center;font-family: 'Raleway', sans-serif;padding: 0px 20px;margin: 0px 0px;">If you ALREADY secured your registration with your refundable deposit&hellip;</p>
			  <p style="font-weight: 500;font-size: 18px;line-height: 20px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">You will have another email in your inbox.</p>
              <p style="font-weight: 500;font-size: 18px;line-height: 20px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Please follow the instructions in that email to access your resources to get started towards your new career</p>
            
			</td>
		</tr>
		<tr style="padding-bottom: 35px;display: block;">
			<td style="padding: 0px 20px;">
		

			
			<p style="font-size: 22px;line-height: 30px;color: #70ad47;font-weight: 700;text-align: center;font-family: 'Raleway', sans-serif;padding: 0px 20px;margin: 0px 0px;">If you did NOT YET secure your registration with your fully refundable deposit&hellip;</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Click the button below, keeping in mind&hellip;</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Applications are being accepted in your zip code now</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Entry Level positions include Window Clerk, Mail Carrier, Mail Handler &amp; Mail<br />
			Processor</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Starting pay is $21/hour on average</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">The average postal worker makes $72,000 a year</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 22px;color: #333;text-align: center;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Postal workers with 20 years of service or more are typically making over $100,000 a year			</p>
             
			
	      </td>
		</tr>
	  <tr style="padding-bottom:35px; ">
        	<td> <p style="line-height: 27px;color: #1f4394;text-align: center;margin-top: 4px;font-size: 22px;font-weight: bold;font-family: 'Raleway', sans-serif; ">To Get Your Free Hiring Guide<br />
			Click <span style="color:#70ad47">&ldquo;Complete Registration&rdquo;</span> Below</p></td>
        </tr>
		<tr align="center" style="padding-top: 35px; display:block;" >
        
			<td style="color: #70ad47; font-size: 17px; text-align: center; font-size: 20px; font-weight: bold;"><a clicktracking="off" href="http://postal-guide.com" rel="nofollow" style="
        background-color: #ff0023;
    padding: 14px 10px;
    border-radius: 30px;
    font-size: 33px;
    text-decoration: none;
    color: #fff;
    font-weight: 500;
    display: block;
    margin: 0px auto;
    width: 400px;
    text-align: center;
" target="_blank">Complete Registration</a></td>
		</tr>
		
		
		<tr>
			<td style="color: #70ad47; font-size: 17px; text-align: center; margin-top: 4px; font-size: 20px; font-weight: bold;border-bottom: 5px solid #ff0023;">&nbsp;
			</td>
		</tr>
	</tbody>
</table>
