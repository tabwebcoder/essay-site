<?php use \Donations\Common\Utils\Helper;?>
<div class="form-group paymf1"><label style="width:50%">Card Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_card_number" value="<?php echo Helper::form_value("donor_card_number");?>" id="donor_card_number"> </div>
<div class="form-group paymf2"><label style="width:50%">CVV (3 digits on back of card)</label> <input type="text" maxlength="3" style="width:50%" name="donor_cvv" value="<?php echo Helper::form_value("donor_cvv");?>" id="cvv"> </div>
<div class="form-group expire-card">
    <label style="width:50%">Expiration Date (mmyy)</label>
    <select style="margin:12px 15px 12px 155px;width: 15%;" name="donor_card_expiry_month" value="<?php echo Helper::form_value("donor_card_expiry_month");?>" id="donor_card_expiry_month">
        <?php foreach (Helper::getExpiryMonths() as $value => $name): ?>
            <option <?php echo Helper::selected_value('donor_card_expiry_month', $value) ;?> value="<?php echo $value;?>"><?php echo $name;?></option>
        <?php endforeach;?>
    </select>
    <select style="width: 25%" name="donor_card_expiry_year" value="<?php echo Helper::form_value("donor_card_expiry_year");?>" id="donor_card_expiry_year">
        <?php foreach (Helper::getExpiryYears() as $value => $name): ?>
            <option <?php echo Helper::selected_value('donor_card_expiry_year', $value) ;?> value="<?php echo $value;?>"><?php echo $name;?></option>
        <?php endforeach;?>
    </select>
    <!--    <input type="text" maxlength="4" style="width:50%" name="donor_card_expiry" value="--><?php //echo Helper::form_value("donor_card_expiry");?><!--" id="donor_card_expiry" >-->
</div>
