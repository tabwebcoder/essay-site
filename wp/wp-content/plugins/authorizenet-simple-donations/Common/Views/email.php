<div style="width: 700px;display: block;margin:0 auto;font-family: seoge ui; " >
    <h3 style="font-size: 22px;">Registration Confirmation for US Postal Job</h3>
        <h3 style="color: red;">Please read this email carefully and completely, before proceeding.  It will save you time and make this easier.  </h3>

        <p style="font-size: 17px;line-height: 22px;">Congratulations.  You have successfully registered.</p>
        <p style="font-size: 17px;line-height: 22px;">The hiring process with the US Postal Service is the same for all entry level positions, which include Window Clerk, Mail Carrier, Mail Handler, Mail Processor and Rural Carrier Associate.  </p>

        <p style="font-size: 17px;line-height: 22px;">You will apply online, take an exam, and have an interview.  </p>

        <p style="font-size: 17px;line-height: 22px;">Typically, it takes about 2 weeks to get hired.</p>

        <p style="font-size: 17px;line-height: 22px;">Everything you need is made available to you on the “Resources” page of our website, which you can access by clicking on  <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >“Access My Resource”</a> link here, or at the bottom of this email:   </p>


        <h2 style="text-align: center;text-decoration: underline;color: #4472c4;">Access My Resources</h2>

        <h3>Before you get started, there are 3 main things you need to know. </h3>

        <h4>#1 – LIVE ASSISTANCE IS AVAILABLE TO HELP YOU</h4>

        <p style="font-size: 17px;line-height: 22px;">We are here for you.  All questions are welcome.  Just call us at <strong style="color: #70ad47;font-size: 26px;">(954) 900-1435</strong>, or start a chat on the   <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >“Resources”</a> page.    </p>

        <p style="font-size: 17px;line-height: 22px;">Live Postal Job Placement Specialists are available Monday thru Friday, 9:00 AM to 9:00 PM EST. </p>


        <h4>#2 - JOB AVAILABILITY CAN BE CONFUSING</h4>

        <p style="font-size: 17px;line-height: 22px;">Some job openings are announced to the public, but not all.  Many job openings are only announced to candidates who pass the exam.</p>


        <p style="font-size: 17px;line-height: 22px;">In order to take the exam, simply apply for the closest job to you.  Don’t worry if it’s not the job you really want.  </p>

        <p style="font-size: 17px;line-height: 22px;">After you pass the exam, you will be able to see ALL the jobs available in your area.   </p>




        <h4>#3 – THE EXAM IS NOT HARD, BUT IT IS TIMED</h4>

        <p style="font-size: 17px;line-height: 22px;">The exam is the most important part of the hiring process.   You will take it at an independent testing facility in your area.  </p>


        <p style="font-size: 17px;line-height: 22px;">The questions on the exam are not difficult.  The issue is time. </p>

        <p style="font-size: 17px;line-height: 22px;">So, be sure to practice for the exam on our web site first, so you know how fast you need to go.   </p>

        <p style="font-size: 17px;line-height: 22px;">Most candidates do not practice for it, and only 20% pass it, but 90% of those who practice ahead of time on our site pass it.  </p>

        <p style="font-size: 17px;line-height: 22px;">Remember, to get hired, you must first pass the exam.</p>

        <p style="font-size: 17px;line-height: 22px;">You are on the right track, and perhaps only 2 weeks away from a fantastic new career.  </p>

        <p style="font-size: 17px;line-height: 22px;">Congratulations once again.  </p>

        <p style="text-align: center;margin-top: 50px;font-size: 17px;">To continue, click this link below to…</p>
        <h2 style="text-align: center;text-decoration: underline;color: #4472c4;margin-top: -11px;font-size: 34px;"> <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >Access My Resources</a></h2>

        <h1 style="text-align: center;color: #70ad47;font-size: 35px;">Live Phone Support (954) 900-1435</h1>

        <p style="text-align: center;margin-top: 0px;font-size: 17px;">Monday thru Friday, 9:00 AM to 9:00 PM EST</p>

</div>