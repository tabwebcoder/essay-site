<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 08/09/17
 * Time: 12:01 AM
 */
namespace Donations\Common\Utils;

use Donations\Common\Classes\APIs\SMSApi;
use Donations\Common\Interfaces\IPayment;

Class Helper {
    static function createPost(\Donations\Common\Classes\PaymentForm $form) {
        $post = array(
            'post_type'    => 'wds_donation',
            'post_title'   => 'Donation - '. sanitize_text_field($form->getDonorFirstName()). ' '. sanitize_text_field($form->getDonorLastName()),
            'post_status'  => 'publish',
            'post_author'  => 1,
        );
        $post_id = wp_insert_post( $post );

        add_post_meta($post_id, 'donor_firstname', sanitize_text_field($form->getDonorFirstName()), true);
        add_post_meta($post_id, 'donor_lastname', sanitize_text_field($form->getDonorLastName()), true);
        add_post_meta($post_id, 'donor_email', sanitize_text_field($form->getDonorEmail()), true);
        add_post_meta($post_id, 'donation_amount', sanitize_text_field($form->getDonationAmount()), true);
        add_post_meta($post_id, 'transaction_id', sanitize_text_field($form->getDonorEmail() . '-' . time()), true);

        add_post_meta($post_id, 'donor_address', sanitize_text_field($form->getDonorAddress()), true);

        add_post_meta($post_id, 'donor_zip', sanitize_text_field($form->getDonorZipCode()), true);

        add_post_meta($post_id, 'donor_state', sanitize_text_field($form->getDonorState()), true);
        add_post_meta($post_id, 'donor_phone', sanitize_text_field($form->getDonorPhone()), true);
    }

    static function sendEmail(\Donations\Common\Classes\PaymentForm $form) {

        $to = $form->getDonorEmail();
        $subject = "Registration Confirmation for US Postal Job";
        $headers = array('Content-Type: text/html; charset=UTF-8','From: US Job Help Center <no-reply@postalhiringservice.com');
        $content = file_get_contents(getPluginDir() . 'Common/Views/email.php');

        return wp_mail($to, $subject, $content, $headers);

    }
    
    static function sendEmail2(\Donations\Common\Classes\PaymentForm $form) {

        $to = $form->getDonorEmail();
        $subject = "Ace Your Interview";
        $headers = array('Content-Type: text/html; charset=UTF-8','From: US Job Help Center <notifications@usjobhelpcenter.com');
        $content = file_get_contents(getPluginDir() . 'Common/Views/emailupsell.php');

        return wp_mail($to, $subject, $content, $headers);

    }

    static function sendSms(\Donations\Common\Classes\PaymentForm $form) {
        $smsApi = new SMSAPI();
        $message = "Congrats.  Your postal job registration is confirmed.  To get started, click {url}.  For assistance, call {support_number}.";
        $website            = get_option('wds_donation_sms_website');
        $supportNumber      = get_option('wds_donation_sms_support_number');
        $message = str_replace('{url}', $website, $message);
        $message = str_replace('{support_number}', $supportNumber, $message);

        $message = sprintf($message, $form->getDonorFirstName(), $form->getDonorLastName(), $form->getDonorEmail());

        $smsApi->sendMessage($form->getDonorPhone(), $message);
    }

    static function sendToCRM(\Donations\Common\Classes\PaymentForm $form, IPayment $paymentGateway) {

        $apiUser = "api";
        $apiPass = "superadmin";

        $orderDate = new \DateTime();
        $orderDate = $orderDate->format('Y-m-d H:i:s');

        $paymentGatewayClass = new \ReflectionClass($paymentGateway);
        $gatewayName = $paymentGatewayClass->getShortName();

        $username = $paymentGateway->getCredentials()['username'];
        $accountType = null;
        $accountType = ucfirst(strtolower($gatewayName));


        $params = [
            'client_email'=> $form->getDonorEmail(),
            'first_name' => $form->getDonorFirstName(),
            'last_name' => $form->getDonorLastName(),
            'phone' => $form->getDonorPhone(),
            'address' => $form->getDonorAddress(),
            'city' => $form->getDonorCity(),
            'state' => $form->getDonorState(),
            'zip_code' => $form->getDonorZipCode(),
            'coupon_code' => "",
            'quantity' => '1',
            'is_recurring' => '0',
            'order_date' => $orderDate,
            'accounttype' => $accountType,
            'product_id' => "9",
            'client_id' => "",
            'accountuser' => $username
        ];

        $post_string = "";
        foreach( $params as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        $request = curl_init("http://job-usps.com/pjsupport_stag/backend/web/api/create?model=order");

        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($request, CURLOPT_HEADER, 1);
        curl_setopt($request, CURLOPT_USERPWD, $apiUser . ":" . $apiPass);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request); // close curl object
    }

    static function checkUniqueEmailonCRM($email) {
        $apiUser = "api";
        $apiPass = "superadmin";

        $request = curl_init("http://www.job-usps.com/pjsupport/backend/web/api/is-email-unique?email=" . $email);

        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($request, CURLOPT_HEADER, false);
        curl_setopt($request, CURLOPT_USERPWD, $apiUser . ":" . $apiPass);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request);

        return $post_response;
    }

    static function form_value($paramName) {
        return isset($_POST[$paramName]) ? $_POST[$paramName] :
            isset($_SESSION['post_data'][$paramName]) ? $_SESSION['post_data'][$paramName]  : "";
    }

    static function selected_value($paramName, $value) {
        return isset($_POST[$paramName]) && $_POST[$paramName] == $value ? "selected" : "";
    }

    static function isPaymentMethodSelected($method) {
        if(isset($_POST['donor_payment_method']) && $method == $_POST['donor_payment_method']) {
            return "checked";
        }

        return "";
    }

    static function getStates() {
        return array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        );
    }

    static function getExpiryYears() {
        $years = array();
        $currentYear = date("Y");
        $currentYearShort = date("y");
        for($i = 0; $i < 10; $i++) {
            $years[$currentYearShort++] = $currentYear++;
        }

        return $years;
    }

    static function getExpiryMonths() {
        $months = array();
        for($i = 01; $i <= 12; $i++) {
            $month = sprintf("%02d", $i);
            $months[$month] = $month;
        }

        return $months;
    }

    /*cyber source helper functions*/
    static function sign ($params) {
        return self::signData(self::buildDataToSign($params), get_option('wds_settings_cs_secret_key'));
    }

    static function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    static function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $params[$field];
        }
        return self::commaSeparate($dataToSign);
    }

    static function commaSeparate ($dataToSign) {
        return implode(",",$dataToSign);
    }
}