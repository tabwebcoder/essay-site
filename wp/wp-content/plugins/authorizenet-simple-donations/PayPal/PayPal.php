<?php

namespace Donations\PayPal;

/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 10/3/2018
 * Time: 10:23 PM
 */



use Donations\Common\Interfaces;


class PayPal implements Interfaces\IPayment
{
    var $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct($form, $environment)
    {
        $this->form = $form;
        $this->credentials = $environment == Interfaces\IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        $environment = ($environment == 'test') ? true : false;
        $this->amount = $_POST['amount'];
        $this->api = new API();
    }

    function makePayment()
    {
       $this->api->request($this);
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function makeSubscribe()
    {
        // TODO;
    }

    function getSandboxCredentials()
    {
        return [
            'client_id' => get_option('wds_settings_pp_client_id'),
            'secret' => get_option('wds_settings_pp_secret')
        ];
    }

    function getLiveCredentials()
    {
        return [
            'client_id' => get_option('wds_settings_pp_client_id'),
            'secret' => get_option('wds_settings_pp_secret')
        ];
    }

    public function getCredentials()
    {
        return $this->credentials;
    }


}