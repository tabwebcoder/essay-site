<?php
/**
 * Created by PhpStorm.
 * User: umerz
 * Date: 10/4/2018
 * Time: 12:41 AM
 */

namespace Donations\PayPal;
require __DIR__  . '/../vendor/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

use Donations\Common\Interfaces;
class API
{
    private $api;
    function __construct()
    {
        $environment = Interfaces\IPayment::ENVIRONMENT_LIVE;
        $environment = ($environment == 'test') ? true : false;
        $this->api = $this->_getApiContext(get_option('wds_settings_pp_client_id'), get_option('wds_settings_pp_secret'), $environment);

    }
    public function request($obj)
    {
        if (empty($_POST['transaction_type'])) {
            throw new \Exception('This script should not be called directly, expected post data');
        }
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

// Set some example data for the payment.
        $currency = 'GBP';
        $amountPayable = 10.00;
        $invoiceNumber = uniqid();

        $amount = new Amount();
        $amount->setCurrency($currency)
            ->setTotal($amountPayable);

        $transaction = new Transaction();
        $transaction->setAmount($obj->amount)
            ->setDescription('Some description about the payment being made')
            ->setInvoiceNumber($invoiceNumber);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(get_option('wds_settings_pp_client_url'))
            ->setCancelUrl('/');

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions([$transaction])
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($obj->api);
        } catch (\Exception $e) {
            throw new \Exception('Unable to create link for payment');
        }

        header('location:' . $payment->getApprovalLink());
        exit(1);
    }

    private function _getApiContext($clientId, $clientSecret, $enableSandbox = false)
    {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($clientId, $clientSecret)
        );

        $apiContext->setConfig([
            'mode' => $enableSandbox ? 'sandbox' : 'live'
        ]);

        return $apiContext;
    }
}