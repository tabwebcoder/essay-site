<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 12/09/17
 * Time: 4:05 PM
 */


namespace Donations\EPin;

use Donations\Common\Classes\PaymentResponseObject;

class API
{
    private $url;

    function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    function sendRequest($params, $json = true)
    {
        $post_string = "";
        foreach ($params as $key => $value) {
            $post_string .= "$key=" . urlencode($value) . "&";
        }
        $post_string = rtrim($post_string, "& ");


        $request = curl_init($this->url . "?" . $post_string);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);

        curl_close($request); // close curl object

        if($json === true)
            return $this->createResponseObjectForJson($post_response);
        return $this->createResponseObject($post_response);
    }

    function createResponseObjectForJson($response) {
        $result = "";
        try{
            $result = \GuzzleHttp\json_decode($response);
        } catch (\Exception $e) {
            $result = $response;
            return new PaymentResponseObject(PaymentResponseObject::STATUS_FAILURE, "Internal error: " . str_replace("msg=", "", $result));
        }


        $responses = [
            'Success' => PaymentResponseObject::STATUS_SUCCESS,
            'Failure' => PaymentResponseObject::STATUS_FAILURE,
        ];

        return new PaymentResponseObject($responses[$result[0]->status], array($result[1]->msg, $result[2]->result));
    }

    function createResponseObject($response)
    {
        if(strpos($response, 'Successs') >= 0) {
            return new PaymentResponseObject(PaymentResponseObject::STATUS_SUCCESS, array("Successfully processed payment"));
        }

        return new PaymentResponseObject(PaymentResponseObject::STATUS_FAILURE, array("Payment processing failed"));
    }
}