<h2>Epin Labs</h2>
<table class="form-table">
    <tr valign="top">
        <th scope="row">Epin Amount</th>
        <td><input type="text" style="width:50%" name="wds_donation_epin_amount" value="<?php echo get_option('wds_donation_epin_amount'); ?>" placeholder="Amount" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">EPin Username</th>
        <td><input type="text" style="width:50%" name="wds_donation_epin_username" value="<?php echo get_option('wds_donation_epin_username'); ?>" placeholder="EPin Username" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Epin Password</th>
        <td><input type="text" style="width:50%" name="wds_donation_epin_password" value="<?php echo get_option('wds_donation_epin_password'); ?>" placeholder="EPin Password" /></td>
    </tr>
</table>