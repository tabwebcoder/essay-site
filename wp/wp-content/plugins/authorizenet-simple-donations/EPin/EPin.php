<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 12/09/17
 * Time: 4:03 PM
 */

namespace Donations\EPin;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Classes\PaymentResponseObject;
use Donations\Common\Interfaces;

class EPin implements Interfaces\IPayment {

    var $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->credentials = $environment == Interfaces\IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_epin_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_epin_amount');
    }

    public function makePayment() {
        $generatedEpin = $this->generateEpin();

        if($generatedEpin != -1) {
            return $this->redeemEpin($generatedEpin);
        } else {

        }
    }

    public function makeSubscribe(){

    }

    public function redeemEpin($epin) {
        $this->api = new API($this->credentials['url_redemption']);
        $params = [
           'username' => $this->credentials['username'],
            'password' => $this->credentials['password'],
            'action' => 'decrease',
            'amount' => $this->amount,
            'pin' => $epin
        ];

        return $this->api->sendRequest($params, false);
    }

    public function generateEpin() {
        $this->api = new API($this->credentials['url_epin_generation']);
        $params = [
            'id' => 29342,
            'firstname' => $this->form->getDonorFirstName(),
            'lastname' => $this->form->getDonorLastName(),
            'address' => $this->form->getDonorAddress(),
            'city' => $this->form->getDonorCity(),
            'state' => $this->form->getDonorState(),
            'zip' => $this->form->getDonorZipCode(),
            'country' => 'US',
            'email' => $this->form->getDonorEmail(),
            'accountnumber' => $this->form->getPaymentForm()->getCreditCardNumber(),
            'cvv' => $this->form->getPaymentForm()->getCvv(),
            'amount' => $this->amount,
            'expiration_month' => substr($this->form->getPaymentForm()->getExpiry(), 0, 2),
            'expiration_year' => "20" . substr($this->form->getPaymentForm()->getExpiry(), 2, 2),
        ];

        $result = $this->api->sendRequest($params, true);
        if($result->getStatus() == PaymentResponseObject::STATUS_SUCCESS) {
            return $result->getMessages()[1]->epin;
        }
        else {
            return -1;
        }
    }

    public function getLiveCredentials() {
        return [
            'url_epin_generation' => 'https://fbmsrewards.com/api/getEpin',
            'url_redemption' => 'http://epinlab-v1.herokuapp.com/API/api.php',
            'username' => 'epin@jobplacement.com',
            'password' => 'gm5iBHeW'
        ];
    }

    public function getSandboxCredentials() {
        return [
            'url_epin_generation' => 'http://fbms.epin.market/api/getEpin',
            'url_redemption' => 'http://epinlab-v1.herokuapp.com/API/api.php',
            'username' => 'epin@jobplacement.com',
            'password' => 'gm5iBHeW'
        ];
    }

    public function getCredentials() {
        return $this->credentials;
    }
}