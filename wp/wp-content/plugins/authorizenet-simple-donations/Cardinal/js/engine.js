Cardinal.configure({
    /*logging: {
        debug: "verbose"
    }*/
});


Cardinal.setup("init", {
    jwt: document.getElementById("JWTContainer").value
});

var data = {
    OrderDetails: {
        "OrderNumber": 'ORDER-123',
        "Amount": '39',
        "CurrencyCode": '840'
    },
    Consumer: {
        Account: {
            AccountNumber: document.getElementById("donor_card_number").value,
            ExpirationMonth: document.getElementById("donor_card_expiry_month").value,
            ExpirationYear: "20" + document.getElementById("donor_card_expiry_year").value,
            CardCode: document.getElementById("donor_cvv").value
        }
    }
};


Cardinal.on("payments.setupComplete", function() {
    // For example, you may have your Submit button disabled on page load. Once you are setup
    // for CCA, you may then enable it. This will prevent users from submitting their order
    // before CCA is ready.
    Cardinal.start("cca", data);

});


Cardinal.on("payments.validated", function (data, jwt) {
    console.log("payment.validated");
    console.log(data);
    switch(data.ActionCode){
        case "SUCCESS":
            injectParams(data.Payment.ExtendedData);
            //sendErrorEmail(data, jwt);
            submitForm();
            break;

        case "NOACTION":
            submitForm();
            // Handle unenrolled scenario
            break;

        case "FAILURE":
            sendErrorEmail(data, jwt);
            submitForm();
            // Handle authentication failed or error encounter scenario
            break;

        case "ERROR":
            sendErrorEmail(data, jwt);
            submitForm();
            // Handle service level error
            break;
    }
});

function submitForm() {
    document.getElementById("three-ds-form").submit();

}
function sendErrorEmail(errorData, jwt) {
    $.ajax("", {
        type: 'post',
        data: {
            action: '3dsError',
            threeDsError: JSON.stringify(errorData),
            response_jwt: jwt,
            request_jwt: document.getElementById("JWTContainer").value,
            accountNumber: document.getElementById("donor_card_number").value,
            log: document.getElementById("log").value,
            form: document.getElementById("three-ds-form").innerHTML
        },
        success: function(data) {

        }
    });
}

function injectParams(threeDsData) {
    console.log(threeDsData);
    document.getElementById("three-ds-form").appendChild(createHiddenElement("cavv", threeDsData.CAVV));
    document.getElementById("three-ds-form").appendChild(createHiddenElement("eciFlag", threeDsData.ECIFlag));
    document.getElementById("three-ds-form").appendChild(createHiddenElement("xid", threeDsData.XID));
    document.getElementById("three-ds-form").appendChild(createHiddenElement("paresStatus", threeDsData.PAResStatus));
    document.getElementById("three-ds-form").appendChild(createHiddenElement("enrolled", threeDsData.Enrolled));


}

function createHiddenElement(name, value) {
    var element1 = document.createElement("input");
    element1.type = "hidden";
    element1.value = value;
    element1.name = name;
    element1.id = name;

    return element1;
}