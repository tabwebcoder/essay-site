<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 15/02/18
 * Time: 1:45 PM
 */


namespace Donations\Cardinal;


use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;



class Cardinal {

    //live
    private static $API_ID      = "5a43d68f1ca9d4138087002c";
    private static $API_KEY     = "6ed33d70-2f2b-4ed0-aaa0-fb6a6de407a2";
    private static $ORGUNIT_ID  = "5a43d1c285282c15508215f7";

    //test
//    private static $API_ID      = "5a43daaf6fe3d1127cdf428f";
//    private static $API_KEY     = "b48abcd6-4393-47ae-9c3b-b6280683846d";
//    private static $ORGUNIT_ID  = "5a43d190ff626b2e40200d54";
//    private static $MERCHANGE_ID  = "";


    static function generateJwt($orderTransactionId, $orderObj)
    {
        $order = array(
            "OrderDetails" => array(
                "OrderNumber" =>  'ORDER-' . $orderTransactionId,
                "Amount" => str_replace('.', '', number_format($orderObj['amount'], 2, '.', '')),
                "CurrencyCode" => '840'
            ),
            "Consumer" => array(
                "Account" => array(
                    "AccountNumber" => $orderObj["donor_card_number"],
                    "ExpirationMonth" => $orderObj["donor_card_expiry_month"],
                    "ExpirationYear" =>  "20" . $orderObj["donor_card_expiry_year"],
                    "CardCode" =>  $orderObj["donor_cvv"]
                )
            )
        );

        $currentTime = time();
        $expireTime = 3600; // expiration in seconds - this equals 1hr

        $token = (new Builder())
            ->setIssuer(self::$API_ID) // API Key Identifier (iss claim)
            ->setId($orderTransactionId, true) // The Transaction Id (jti claim)
            ->setIssuedAt($currentTime) // Configures the time that the token was issued (iat claim)
            ->setExpiration($currentTime + $expireTime) // Configures the expiration time of the token (exp claim)
            ->set('OrgUnitId', self::$ORGUNIT_ID) // Configures a new claim, called "OrgUnitId"
            ->set('Payload', $order) // Configures a new claim, called "Payload", containing the OrderDetails
            ->set('ObjectifyPayload', true)
            ->sign(new Sha256(), self::$API_KEY) // Sign with API Key
            ->getToken(); // Retrieves the generated token

        return $token;
    }

    function validateJwt($jwt) {

    }
}