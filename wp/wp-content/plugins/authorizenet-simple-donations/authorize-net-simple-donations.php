<?php
/*
Plugin Name: Simple Donations
Author: Wasim Qamar
Description: Accept donations simply with Authorize.net/Paycertify/Paycheck accounts. Easy to use and configure.
Version: 2.2
License: GPLv2 or later
*/

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);
session_start();

use Donations\Common\Interfaces\IPayment;
use Donations\Common\Utils\Helper;
use Donations\Common\Classes\APIs\SMSApi;
use Donations\Cardinal\Cardinal;

require_once ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/vendor/autoload.php';
require_once ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/Common/Utils/Helper.php';


function getPluginDir() {
    return ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/';
}

spl_autoload_register(function ($class_name) {
    $pluginDir = getPluginDir();
    $class_name = str_replace('\\', '/', $class_name);
  //  echo $class_name."<br/>";
    if(0 === strpos($class_name, 'Donations/')) {
        $class_name = str_replace('Donations/', '', $class_name);
        include $pluginDir . $class_name . '.php';
    }
});

add_action('template_redirect', 'ajaxHandler');

add_shortcode('wds_donate', 'fn_wds_donate');
function fn_wds_donate()
{
    $messages = '';
//    $messages = catchPayCertifyParams();


    if( isset( $_POST['wds_donate'])) {
        $_SESSION['post_data'] = $_POST;
        if($_POST['pay_method'] == IPayment::PAYMENT_GATEWAY_CYBERSOURCE)
        {
            // redirect to payment process page
            include "Common/Views/redirect.php";
            exit;


        } else {

            $paymentGatewayFactory = new \Donations\Common\Classes\PaymentGatewayFactory();

            $paymentGateway = $paymentGatewayFactory->getPaymentGateway($_POST, get_option('wds_donation_mode'), $_POST['pay_method']);

            $_POST['amount'] = $paymentGateway->getAmount();
            //threeDSecurify($_POST);
            $paymentGateway->makePayment();
            /*if ($result->getStatus() == \Donations\Common\Classes\PaymentResponseObject::STATUS_SUCCESS) {

                paymentSuccessful($form);
            }
            else {
                $messages .= implode('<br />', (array) $result->getMessages());
            }*/
        }

    }
    include "Common/Views/form.php";
}

function wds_donation_type() {



    $labels = array(
        'name'                => _x( 'Donations', 'Post Type General Name', 'wds_donation' ),
        'singular_name'       => _x( 'Donation', 'Post Type Singular Name', 'wds_donation' ),
        'menu_name'           => __( 'Donation ', 'wds_donation' ),
        'parent_item_colon'   => __( 'Parent Donation', 'wds_donation' ),
        'all_items'           => __( 'All Donation', 'wds_donation' ),
        'view_item'           => __( 'View Donation', 'wds_donation' ),
        'add_new_item'        => __( 'Add New Donation', 'wds_donation' ),
        'add_new'             => __( 'Add New', 'wds_donation' ),
        'edit_item'           => __( 'Edit Donation', 'wds_donation' ),
        'update_item'         => __( 'Update Donation', 'wds_donation' ),
        'search_items'        => __( 'Search Donation', 'wds_donation' ),
        'not_found'           => __( 'Not found', 'wds_donation' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'wds_donation' ),
    );
    $args = array(
        'label'               => __( 'wds_donation', 'wds_donation' ),
        'description'         => __( 'list of donations', 'wds_donation' ),
        'labels'              => $labels,
        'supports'            => array('title', 'custom-fields' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'wds_donation', $args );

}

add_action( 'init', 'wds_donation_type', 0 );

add_action('admin_menu', 'wds_create_menu');

function wds_create_menu() {

    add_menu_page('Donation Settings', 'Donation Settings', 'administrator', 'donation-settings-page', 'donation_settings_page');
    add_action( 'admin_init', 'donation_register_mysettings' );
}


function donation_register_mysettings() {

    //Common settings
    register_setting( 'wds-settings-group', 'wds_thankyou_message' );
    register_setting( 'wds-settings-group', 'wds_processor_description' );
    register_setting( 'wds-settings-group', 'wds_donation_mode' );
    register_setting( 'wds-settings-group', 'wds_payment_gateway' );
    register_setting( 'wds-settings-group', 'wds_credit_card_enabled' );
    register_setting( 'wds-settings-group', 'wds_checking_account_enabled' );
    register_setting( 'wds-settings-group', 'wds_test_emails' );

    //Paycheck settings
    register_setting( 'wds-settings-group', 'wds_donation_paycheck_merchant_id' );
    register_setting( 'wds-settings-group', 'wds_donation_paycheck_amount' );

    //Authorizenet settings
    register_setting( 'wds-settings-group', 'wds_donation_authorizenet_merchant_id' );
    register_setting( 'wds-settings-group', 'wds_donation_authorizenet_transaction_key' );
    register_setting( 'wds-settings-group', 'wds_donation_authorizenet_amount' );

    //paycertify settings
    register_setting( 'wds-settings-group', 'wds_donation_paycertify_api_key' );
    register_setting( 'wds-settings-group', 'wds_donation_paycertify_api_secret' );
    register_setting( 'wds-settings-group', 'wds_donation_paycertify_gateway_token' );
    register_setting( 'wds-settings-group', 'wds_donation_paycertify_amount' );

    //epin labs settings
    register_setting( 'wds-settings-group', 'wds_donation_epin_username' );
    register_setting( 'wds-settings-group', 'wds_donation_epin_password' );
    register_setting( 'wds-settings-group', 'wds_donation_epin_amount' );

    //SMS Settings
    register_setting( 'wds-settings-group', 'wds_donation_sms_website' );
    register_setting( 'wds-settings-group', 'wds_donation_sms_support_number' );

    //Card connect settings
    register_setting( 'wds-settings-group', 'wds_donation_cardconnect_merchant_id' );
    register_setting( 'wds-settings-group', 'wds_donation_cardconnect_username' );
    register_setting( 'wds-settings-group', 'wds_donation_cardconnect_password' );
    register_setting( 'wds-settings-group', 'wds_donation_cardconnect_site' );
    register_setting( 'wds-settings-group', 'wds_donation_cardconnect_amount' );

    //3ds
    register_setting( 'wds-settings-group', 'wds_donation_3ds_enabled' );
    register_setting( 'wds-settings-group', 'wds_donation_3ds_provider' );

    //upsell product settings
    register_setting( 'wds-settings-group', 'wds_settings_upsell_enabled' );
    register_setting( 'wds-settings-group', 'wds_settings_upsell_amount' );

    // additional settings added
    register_setting( 'wds-settings-group', 'wds_settings_upsell_type' );
    register_setting( 'wds-settings-group', 'wds_settings_upsell_subscription_duration' );
    register_setting( 'wds-settings-group', 'wds_settings_upsell_subscription_duration_type' );
    register_setting( 'wds-settings-group', 'wds_settings_upsell_subscription_occurr' );
    register_setting( 'wds-settings-group', 'wds_donation_moodle_website' );

    //cyber source settings fields
    register_setting( 'wds-settings-group', 'wds_settings_cs_access_key' );
    register_setting( 'wds-settings-group', 'wds_settings_cs_profile_id' );
    register_setting( 'wds-settings-group', 'wds_settings_cs_secret_key' );

    register_setting( 'wds-settings-group', 'wds_settings_pp_client_id' );
    register_setting( 'wds-settings-group', 'wds_settings_pp_secret' );
    register_setting( 'wds-settings-group', 'wds_settings_pp_url' );


}

function donation_settings_page() {
    include "Common/Views/settings.php";
}

function donation_add_meta_box()
{
    $screens = array( 'wds_donation' );

    foreach ( $screens as $screen ) {

        add_meta_box(  'myplugin_sectionid', __( 'Donation Information', 'myplugin_textdomain' ),'donation_meta_box_callback', $screen, 'normal','high' );
    }
}

add_action( 'add_meta_boxes', 'donation_add_meta_box' );

function donation_meta_box_callback($post)
{

    ?>

    <ul>
        <li>
            <div class="html-label">First Name</div>

            <div class="html-field form-group" ><input type="text" name="donor_firstname" id=�donor_firstname" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_firstname', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Last Name</div>

            <div class="html-field form-group"><input type="text" name="donor_lastname" id="donor_last_name" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_lastname', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">E-mail</div>

            <div class="html-field form-group"><input type="text" name="donor_email" id="donor_email" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_email', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Amount (USD)</div>

            <div class="html-field form-group"><input type="text" name="donation_amount" id="donation_amount" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donation_amount', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Transaction ID</div>

            <div class="html-field form-group"><input type="text" name="transaction_id" id="transaction_id" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'transaction_id', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Last 4</div>

            <div class="html-field form-group"><input type="text" name="last_4" id="last_4" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'last_4', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">ApprovalCode</div>

            <div class="html-field form-group"><input type="text" name="approval_code" id="approval_code" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'approval_code', true ));?>"></div>
        </li>
    </ul>


<?php

}


function catchPayCertifyParams() {

    if(isset($_GET['3dsecure']) && $_GET['3dsecure'] == "true") {
        $payCertify = new \Donations\Paycertify\PayCertify(unserialize($_SESSION['form']), get_option('wds_donation_mode'));
        //write_log("Verifying enrollment");
        $threeDSResponse = $payCertify->verifyEnrollment();


        if($threeDSResponse != false) {
        	
        	$data = json_decode($threeDSResponse);
        	
        	$payCertify = new \Donations\Paycertify\PayCertify(unserialize($_SESSION['form']), get_option('wds_donation_mode'));
        	$form = unserialize($_SESSION['form']);

        	$result = $payCertify->makeSecurePayment((array)$data, $form, true);

        	if ($result->getStatus() == \Donations\Common\Classes\PaymentResponseObject::STATUS_SUCCESS) {
        	    paymentSuccessful($form);
        	}
        	else {
                //top.location.waldf = back page
                $msg = implode('<br />', (array) $result->getMessages());
                $formUrl = get_site_url() . '/apply-step-4?msg=' . $msg;
                echo '<script>top.location.replace("' . $formUrl . '");</script>';
        	}
        }
        else {
            $form = unserialize($_SESSION['form']);
            $data['pan'] = $form->getPaymentForm()->getCreditCardNumber();
            $data['card_exp_month'] = substr($form->getPaymentForm()->getExpiry(), 0, 2);
            $data['card_exp_year'] = "20" . substr($form->getPaymentForm()->getExpiry(), 2, 2);
            $data['transaction_id'] = time();

            $result = $payCertify->makeSecurePayment((array)$data, $form, false);

            if ($result->getStatus() == \Donations\Common\Classes\PaymentResponseObject::STATUS_SUCCESS) {
                paymentSuccessful($form);
            }
            else {
                $msg = implode('<br />', (array) $result->getMessages());
                $formUrl = get_site_url() . '/apply-step-4?msg=' . $msg;
                echo '<script>top.location.replace("' . $formUrl . '");</script>';
            }
        }
    }
}

function paymentSuccessful($form) {
    Helper::createPost($form);
    Helper::sendEmail($form);
if($_POST['upsell-product'] ==1)
{
Helper::sendEmail2($form);
} 
    Helper::sendSms($form);
    unset($_SESSION['post_data']);
    $successUrl = get_site_url() . '/success';
    echo '<script>top.location.replace("' . $successUrl . '");</script>';
}

function write_log($message) {
    if (WP_DEBUG === true) {
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
    }
}

function threeDSecurify($order) {
    if(get_option('wds_donation_3ds_enabled') == "1" && isset($order['3ds'])) {
        $token = Cardinal::generateJwt("order-" . time(), $order);

        $log = "Form info: " . json_encode($order);
        $log .= "------------------------------------";
        $log .= "\n\n\n\n Session info: " . json_encode($_SESSION);
        $log .= "------------------------------------";


        echo "Please wait ...";
        echo "<form action='' method='post' id='three-ds-form' name='three-ds-form'>";
        echo "<input type ='hidden' id='JWTContainer' name='JWTContainer' value='$token' />";
        echo "<input type ='hidden' id='log' name='JWTContainer' value='$log' />";

        foreach ($order as $fieldName => $value) {
            if($fieldName != "3ds")
                echo "<input type ='hidden' id='$fieldName' name='$fieldName' value='$value' />";
        }
        echo "</form>";

//test
//        echo "<script src='https://includestest.ccdc02.com/cardinalcruise/v1/songbird.js'></script>";

        //live
        echo "<script src='https://includes.ccdc02.com/cardinalcruise/v1/songbird.js'></script>";
        echo "<script src='" .
            plugins_url( 'Cardinal/js/engine.js', __FILE__ ) . "'></script>";

        exit;
    }
}

function ajaxHandler() {
    if(isset($_POST['action'])) {
        switch($_POST['action']) {
            case '3dsError':
                $threeDsError = $_POST['threeDsError'];
                $accountNumber = $_POST['accountNumber'];
                $response_JWT = $_POST['response_jwt'];
                $request_JWT = $_POST['request_jwt'];
                wp_mail(
                    get_bloginfo('admin_email'),
                    "Failed 3ds transaction",
                    "ThreeDsInfo: " . $threeDsError
                    . "\n\n" . "Request JWT: " . $request_JWT
                    . "\n\n" . "Response JWT: " . $response_JWT
                    . "\n\n" . "Account: " . $accountNumber
                    . "\n\n" . "Log: " . $_POST['log']
                    . "\n\n" . "Form: " . $_POST['form'],
                    array('Content-Type: text/html; charset=UTF-8','From: US Job Help Center <no-reply@postalhiringservice.com'));
                break;

        }

        exit;
    }
}