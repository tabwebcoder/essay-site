<?php
require_once("lib/Stripe.php");

$client_id = StripeClientID;//'pk_test_KEEAs3YmDSwuGDOHZHgiXiNC';
$api_key = StripeClientSecret;//'sk_test_dmipkFpfeHOKlptisXjTRin8';

// FROM WHERE GET TOKEN URI DON'T CHANGE IT
define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');
// URL USING FOR AUTH DON'T CHNAGE IT
define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');



Stripe::setApiKey($api_key);
//Stripe::setClientId($client_id);
//var_dump(Stripe);die;
define('API_KEY', $api_key);
// Client ID OF YOUR APPLICATION SEE IN CONNECT IN DASHBORD OF STRIPE sk_test_3PYl9awNkK4D9JIbAwnvNSkw
//ca_AADLGcf8OAJpdeg6s4lE32z2H7Vbg9YD
define('CLIENT_ID', $client_id);

// Token is created using Stripe.js or Checkout!
// we can create payment directly without creating a token
// its just for the card verification or  we will use that in future for different purposes
// CURRENTLY JUST USING FOR CHARGE_ACCOUNT
function get_stripe_token($name, $card, $expmonth, $expyear, $cvc = '') {
    try {
        $token = Stripe_Token::create(
                        array("card" =>
                            array(
                                "name" => $card,
                                "number" => $name,
                                "exp_month" => $expmonth,
                                "exp_year" => $expyear,
                                "cvc" => $cvc
                            )
        ));
//        $card = array("card" =>
//            array(
//                "name" => $name,
//                "number" => $card,
//                "exp_month" => $expmonth,
//                "exp_year" => $expyear,
//                "cvc" => $cvc
//            )
//        );
//        print_r($card);
//        die;
//        echo '<pre>';
//        print_r($token);
//        exit;

        if (!$token->error) {
            return $token;
        } else {
            $message = ['error' => 'yes', 'message' => $token->error['message']];
            return $message;
        }
    } catch (Exception $ex) {
        return ['error' => 'yes', 'mesage' => $ex];
    }
}

// GET AUTH LINK FOR YOUR APP OF STRIPE
function get_auth_url() {
    // Array parms
    // response type should be code as mention in their API docs

    $authorize_request_body = array(
        'response_type' => 'code',
        'scope' => 'read_write',
        'client_id' => CLIENT_ID
    );
    $url = AUTHORIZE_URI . '?' . http_build_query($authorize_request_body);
    return $url;
}

function get_user_details($code) {
    if (isset($code) && !empty($code)) {
        $token_request_body = array(
            'client_secret' => API_KEY,
            'grant_type' => 'authorization_code',
            'client_id' => CLIENT_ID,
            'code' => $code,
        );
        $resp = send_curl(TOKEN_URI, $token_request_body);
        /* echo '<pre>';
          print_r($resp);
          exit; */
        return $resp;
    }
}

function send_curl($url, $pram) {
    $req = curl_init($url);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POST, true);
    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($pram));
    // TODO: Additional error handling
    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
    $resp = json_decode(curl_exec($req), true);
    curl_close($req);

    /* echo '<pre>';
      echo $url.'?'.http_build_query($pram);
      print_r($resp);
      exit; */

    return $resp;
}

// THIS FUNCTION IS TO CHARGE ACCOUNT...
function charge_amount($token, $payment, $currency) {
    $payment *= 100;
    $payment = (int) $payment;
    if (!$token->error) {
        try {
            $token = $token['id'];
            // Create a Customer:
            $customer = Stripe_Customer::create(array("source" => $token));
            // Charge the Customer instead of the card
            $charge = Stripe_Charge::create(array("amount" => $payment, "currency" => $currency, "customer" => $customer['id']));


            if ($charge['error']) {

                return ['error' => 'yes', 'mesage' => $charge['message']];
            } else {
                return $charge;
            }
        } catch (Exception $ex) {
            return ['error' => 'yes', 'mesage' => $ex];
        }
    } else {
        // 	$_COMMON_HELPER_OBJ->errors['msg'] =$token->error['message'];
        // $_COMMON_HELPER_OBJ->__response();
        // 	exit;
    }
}

function charge_customer($cus_id, $payment, $currency) {
    $payment *= 100;
    $payment = (int) $payment;
    try {
        $charge = Stripe_Charge::create(array("amount" => $payment, "currency" => $currency, "customer" => $cus_id));


        if ($charge['error']) {

            return ['error' => 'yes', 'mesage' => $charge['message']];
        } else {
            return $charge;
        }
    } catch (Exception $ex) {
        return ['error' => 'yes', 'mesage' => $ex];
    }
}

function create_customer($token, $desc = 'essay_customer') {
    try {
        $customer = Stripe_Customer::create(array(
                    "description" => $desc,
                    "source" => $token['id']
        ));
        return $customer;
    } catch (Exception $ex) {
        return ['error' => 'yes', 'mesage' => $ex];
    }
}

function transfer_amount($amount, $currency, $acc_id, $desc) {
    try {
        $amount = $amount * 100;
        return Stripe_Transfer::create(array(
                    "amount" => $amount,
                    "currency" => $currency,
                    "destination" => $acc_id,
                    "description" => $desc
        ));
    } catch (Exception $ex) {
        return ['error' => 'yes', 'mesage' => $ex];
    }
}

?>