<?php
/**
 * API Name:          Form data
 * Description:       Form data!
 * Version:           1.0.0
 * Author:            Husnain ali
 * Author URI:        http://husnain.pk
 * Text Domain:       Husnain
 * License:           GPL-2.0+
 */
function select_from_cms($table) {
    $connection_crm = mysqli_connect('globalmarketingforum.org', 'globalma_hasnain', 'tiptop123', 'globalma_mhwriter_apanel');
    $sql = "select id,name from $table";
    $result = $connection_crm->query($sql);
    $data_array = [];
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $data_array[] = $row;
        }
    }
    $response = array(
        "Status" => true,
        "data" => $data_array
    );
    echo json_encode($response);
}

if (isset($_GET['table_name'])) {
    $table = $_GET['table_name'];
    select_from_cms($table);
}