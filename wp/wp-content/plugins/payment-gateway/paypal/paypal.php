<?php
require "app/start.php";
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item; 
use PayPal\Api\ItemList; 
use PayPal\Api\Payer; 
use PayPal\Api\Payment; 
use PayPal\Api\RedirectUrls; 
use PayPal\Api\Transaction;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
set_time_limit(3600);


function execute_payment($response)
{
    $apiContext = get_api_contax();
    if (isset($response['success']) && $response['success'] == 'true') {

        $paymentId = $response['paymentId'];
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($response['PayerID']);

        try {
            
            $result = $payment->execute($execution, $apiContext);
            
            try {
                $payment = Payment::get($paymentId, $apiContext);

                    $resp = $payment->toArray();
                        $Response_paypal = json_encode( $resp);
                        $connection_db = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
                        $insert_query_log = "INSERT INTO `wp_payment_gateways_response` (GatewayName, Email,Response, Method,Status) VALUES ('Paypal', '', '$Response_paypal','Transaction success','success')";
                        $connection_db->query($insert_query_log);
                    $TransactionDetails = $resp['transactions'][0];
                    $TransId = $resp['id'];
                    $status['msg']      = 'success';
                    $status['TransactionDetail'] = $TransactionDetails;
                    $status['TransactionId'] = $TransId;
                  return  $status;

            } catch (Exception $ex) {
                $error = $ex->getData();
                $status['msg']      = 'fail';
                $status['data']     = $error;
                return $status;
            }
        } catch (Exception $ex) {
            $error = $ex->getData();
            $status['msg']      = 'fail';
            $status['data']     = $error;
            return $status;
        }
            $error = $ex->getData();
            $status['msg']      = 'fail';
            $status['data']     = $error;
    } else {
        $status['msg']      = 'fail';
        $status['data']     = "User Cancelled the Approval";
    }
    return $status;
}

function create_payment_url($charge_amount, $currency, $details = array())
{
    $apiContext = get_api_contax(); 
    $payer = new Payer();
    $payer->setPaymentMethod("paypal");

    $amount = new Amount();
    $amount->setCurrency($currency)
        ->setTotal($charge_amount);


    $item = new Item();

    $item->setDescription(json_encode($details))->setCurrency($currency)->setQuantity(1)->setPrice($charge_amount);
    $itemList = new ItemList();
    $itemList->setItems(array($item));
    
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());

    $baseUrl = getBaseUrl();
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl("$baseUrl/payment-gateway-thanks/?success=true")
        ->setCancelUrl("$baseUrl/payment-cancel/?success=false");

    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    try {
        $payment->create($apiContext);
    } catch (Exception $ex) {
        $error = $ex->getData();
        $status['msg']      = 'fail';
        $status['data']     = $error;
         return $status;
    }
   
    $approvalUrl = $payment->getApprovalLink();
    return $approvalUrl;
}
function getBaseUrl()
{


if($_SERVER['SERVER_NAME'] == "localhost")
            {
$web_url='http://'.$_SERVER['SERVER_NAME']."/outside/essay-site/wp/";
            }
            else
            {
$web_url='http://';
            }

   // global $web_url;
    return $web_url;
}