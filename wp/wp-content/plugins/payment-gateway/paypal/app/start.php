<?php
require_once __DIR__ . '/../vendor/autoload.php';
$connection_db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql = "SELECT * FROM `{$wpdb->prefix}payment_gateways` where GatewayName='Paypal' limit 1";
$result = $connection_db->query($sql);
$row_paypal = $result->fetch_assoc();
if ($row_paypal['GatewayEnviorment'] && $row_paypal['GatewayEnviorment'] == 'Sandbox') {
    define('PaypalClientID', $row_paypal['ClientIDTest']);
    define('PaypalClientSecret', $row_paypal['ClientSecretTest']);
} else {
    define('PaypalClientID', $row_paypal['ClientID']);
    define('PaypalClientSecret', $row_paypal['ClientSecret']);
}
define('PaypalMode', $row_paypal['GatewayEnviorment']);
define('PaypalRedirectSuccess', $row_paypal['RedirectSuccess']);
define('PaypalRedirectCancle', $row_paypal['RedirectCancle']);
$apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
        PaypalClientID, // ClientID 
        PaypalClientSecret
        )
);
/* * ***************Stripe configuration**** *///
$sql_stripe = "SELECT * FROM `{$wpdb->prefix}payment_gateways` where GatewayName='Stripe' limit 1";
$result_stripe = $connection_db->query($sql_stripe);
$row_stripe = $result_stripe->fetch_assoc();
if (!empty($row_stripe)) {
    if ($row_stripe['GatewayEnviorment'] && $row_stripe['GatewayEnviorment'] == 'Sandbox') {
        define('StripeClientID', $row_stripe['ClientIDTest']);
        define('StripeClientSecret', $row_stripe['ClientSecretTest']);
    } else {
        define('StripeClientID', $row_stripe['ClientID']);
        define('StripeClientSecret', $row_stripe['ClientSecret']);
    }
}

function get_api_contax() {
    global $apiContext;
    return $apiContext;
}

// return apiContext
?>