<div class="avada-row">
    <div class="fusion-three-fourth three_fourth fusion-layout-column fusion-column spacing-yes">
        <div class="fusion-column-wrapper">
            <div id="myorder">
                <h1>Order Now for Writing Service</h1>
                <div id="Order">
                    <div class="loader"><img src="/mhrwriter/wp-content/themes/mhrwriter/images/ajax-loader.gif" alt="AJAX Loading" width="100" height="100"></div>


                    <form name="ordernow" id="ordernow" method="post" onsubmit="return ValidateOrder()">
                        <!-- cyber source hidden values-->
                        <input type="hidden" name="access_key" value="<?=get_option('wds_settings_cs_access_key')?>">
                        <input type="hidden" name="profile_id" value="<?=get_option('wds_settings_cs_profile_id')?>">
                        <input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">
                        <input type="hidden" name="signed_field_names"
                               value="merchant_defined_data1,consumer_id,contact_phone,email_address,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency">
                        <input type="hidden" name="unsigned_field_names">
                        <input type="hidden" name="consumer_id" value="<?php //echo date_timestamp_get($date); ?>">
                        <input type="hidden" name="customer_ip_address"
                               value="<?php echo $_SERVER['REMOTE_ADDR'] ?>">
                        <input type="hidden" name="merchant_defined_data1" value="WC">
                        <input type="hidden" name="signed_date_time"
                               value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                        <input type="hidden" name="locale" value="en">
                        <input type="hidden" name="transaction_type" size="25" value="sale">
                        <input type="hidden" name="reference_number"
                               value="<?php //echo date_timestamp_get($date); ?>">
                        <!-- end of hidden fields -->
                        <h3>Customer Information:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Full Name <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="full_name">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Email <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="email_address">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Contact Phone <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="contact_phone">
                            </div>
                            <p></p>
                        </div>
                        <h3 class="top">Order Pricing:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Type of Paper <span>*</span></div>
                            <div class="field-input">
                                <select name="paper_type">
                                    <option value="0">[Not Selected]</option>
                           <!--         <option value="1">Other</option>
                                    <option value="2">Admission Essay (Personal Statement)</option>
                                    <option value="3">Annotated Bibliography</option>
                                    <option value="4">Argumentative Essay</option>
                                    <option value="5">Article</option>
                                    <option value="6">Article Critique</option>
                                    <option value="7">Article Review</option>
                                    <option value="8">Assignment</option>
                                    <option value="9">Biography</option>
                                    <option value="10">Blog Content</option>
                                    <option value="11">Book Report</option>
                                    <option value="12">Book Review</option>
                                    <option value="13">Business Plan</option>
                                    <option value="14">Case Study</option>
                                    <option value="15">Coursework</option>
                                    <option value="16">Cover/Job/Application Letter</option>
                                    <option value="17">Creative Writing</option>
                                    <option value="18">Critical Thinking</option>
                                    <option value="19">Critical Writing</option>
                                    <option value="20">Dissertation</option>
                                    <option value="21">Dissertation Chapter – Abstract</option>
                                    <option value="22">Dissertation Chapter – Conclusion</option>
                                    <option value="23">Dissertation Chapter – Discussion</option>
                                    <option value="24">Dissertation Chapter – Hypothesis</option>
                                    <option value="25">Dissertation Chapter – Introduction</option>
                                    <option value="26">Dissertation Chapter – Literature Review</option>
                                    <option value="27">Dissertation Chapter – Methodology</option>
                                    <option value="28">Dissertation Chapter – Results</option>
                                    <option value="59">Dissertation Proposal</option>
                                    <option value="29">Essay</option>
                                    <option value="30">Exam Question</option>
                                    <option value="31">Ghost Writing</option>
                                    <option value="32">Grant Application</option>
                                    <option value="33">Lab Report</option>
                                    <option value="34">Literature Review</option>
                                    <option value="35">Marketing Proposal</option>
                                    <option value="36">Math/Physics/Economics/Statistics Problems</option>
                                    <option value="37">Movie/Film Review</option>
                                    <option value="38">MS Project Assignment</option>
                                    <option value="39">Multiple Choice Questions – Non-time-framed</option>
                                    <option value="40">Multiple Choice Questions – Time-framed</option>
                                    <option value="58">Personal Statement (Admission Essay)</option>
                                    <option value="41">PowerPoint Presentation</option>
                                    <option value="42">Presentation or Speech</option>
                                    <option value="43">Problem Solving</option>
                                    <option value="44">Product/Service Page Content</option>
                                    <option value="45">Programming</option>
                                    <option value="46">Project</option>
                                    <option value="47">Reaction Paper</option>
                                    <option value="48">Report</option>
                                    <option value="49">Research Paper</option>
                                    <option value="50">Research Proposal</option>
                                    <option value="51">Research Summary</option>
                                    <option value="52">Resume Writing</option>
                                    <option value="53">Statistics Project</option>
                                    <option value="54">Term Paper</option>
                                    <option value="55">Thesis</option>
                                    <option value="56">Thesis Proposal</option>
                                    <option value="57">Web Content</option>-->
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Type of Service <span>*</span></div>
                            <div class="field-input">
                                <select name="service_type"> <option value="0">[Please Select Type of Paper]</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Urgency <span>*</span></div>
                            <div class="field-input">
                                <select name="urgency_time" class="shorts">
                                    <option value="0">[Please Select Type of Paper]</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Quality Level <span>*</span></div>
                            <div class="field-input">
                                <select name="quality_level" class="shorts">
                                    <option value="0">[Not Selected]</option>
<!--                                    <option value="1">Gold (Equivalent to 1st Class)</option>
                                    <option value="2">Silver (Equivalent to 2:1)</option>
                                    <option value="3">Bronze (Equivalent to 2:2)</option>-->
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Number of Pages <span>*</span></div>
                            <div class="field-input">
                                <select name="number_of_pages" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                     <?php
//                                     $word_pages = 250;
//for ($i=1; $i < 201; $i++) { 
    ?>
<!--      <option value="<?php //echo $i ?>"><?php //echo $i ?> page (<?php  //echo $word_pages ?> words)</option>-->
    <?php 
//   $word_pages = $word_pages+250;
//    # code...
//}
                                     ?>
                                  
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Currency <span>*</span></div>
                            <div class="field-input">
                                <select name="currency" class="shortest">
                                    <option value="1" selected="selected">GBP (£)</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Plagiarism Report? <span>*</span></div>
                            <div class="field-input">
                                <div class="plag-yes radio">
                                    <input type="radio" name="plag_report" id="yes" value="1">  <label for="yes">Yes</label><span class="plag_rate">(£4 for 4,000 words)</span></div>
                                <div class="plag-no radio">
                                    <input type="radio" name="plag_report" id="no" value="2">  <label for="no">No</label>
                                </div>
                                <p></p>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Discount Code</div>
                            <div class="field-input">
                                <input type="text" name="discount_code" class="shortest" autocomplete="off">
                            </div>
                            <p></p>
                        </div>
                        <div class="discount_view"></div>
                        <div class="form-row">
                            <div class="field-label cost-label">Total <strong>Cost</strong></div>
                            <div class="field-written">
                                <span class="total">£0.00<b  class="perpage">(select type of paper)</b></span>
                                <p></p>
                            </div>
                            <p></p>
                        </div>
                        <h3 class="top">Order Information:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Subject Area <span>*</span></div>
                            <div class="field-input">
                                <select name="subject_area">
                                    <option value="0">[Not Selected]</option>
<!--                                    <option value="1">Other</option>
                                    <option value="2">Accounting</option>
                                    <option value="3">Accounting Law</option>
                                    <option value="4">Advanced Art</option>
                                    <option value="5">Advertising</option>
                                    <option value="6">Aeronautics</option>
                                    <option value="7">African-American Studies</option>
                                    <option value="8">Aging</option>
                                    <option value="9">Agriculture</option>
                                    <option value="10">Alternative Medicine</option>
                                    <option value="11">American History</option>
                                    <option value="12">American Literature</option>
                                    <option value="13">American Studies</option>
                                    <option value="14">Anatomy</option>
                                    <option value="15">Anthropology</option>
                                    <option value="16">Antique Literature</option>
                                    <option value="17">Application Essay</option>
                                    <option value="18">Application Letters</option>
                                    <option value="19">Applied Science</option>
                                    <option value="20">Archaeology</option>
                                    <option value="21">Architecture</option>
                                    <option value="22">Art</option>
                                    <option value="23">Asian Literature</option>
                                    <option value="24">Asian Studies</option>
                                    <option value="25">Astronomy</option>
                                    <option value="26">Auditing</option>
                                    <option value="27">Australian Studies</option>
                                    <option value="28">Aviation</option>
                                    <option value="29">Banking</option>
                                    <option value="30">Behavioral Science</option>
                                    <option value="31">Bioinformatics</option>
                                    <option value="32">Biological Sciences</option>
                                    <option value="33">Biology</option>
                                    <option value="34">Biomechanics</option>
                                    <option value="35">Biomedical Engineering</option>
                                    <option value="36">Biotechnology</option>
                                    <option value="37">Botany</option>
                                    <option value="38">Business</option>
                                    <option value="39">Business Ethics</option>
                                    <option value="40">Business Intelligence</option>
                                    <option value="41">Business Management</option>
                                    <option value="214">Business Strategy</option>
                                    <option value="42">Business Studies</option>
                                    <option value="43">Canadian Studies</option>
                                    <option value="44">Career Management</option>
                                    <option value="45">Case Study</option>
                                    <option value="46">Celtic Studies</option>
                                    <option value="47">Chemical Engineering</option>
                                    <option value="48">Chemistry</option>
                                    <option value="49">Child</option>
                                    <option value="50">Chinese Studies</option>
                                    <option value="51">Civics</option>
                                    <option value="52">Civil Engineering</option>
                                    <option value="53">Classic English Literature</option>
                                    <option value="54">Communication</option>
                                    <option value="55">Communication Strategies</option>
                                    <option value="56">Community Studies</option>
                                    <option value="57">Company Analysis</option>
                                    <option value="58">Computational Science</option>
                                    <option value="59">Computer Engineering</option>
                                    <option value="60">Computer Science</option>
                                    <option value="61">Construction</option>
                                    <option value="62">Criminal Law</option>
                                    <option value="63">Criminology</option>
                                    <option value="64">Cultural Studies</option>
                                    <option value="65">Dance</option>
                                    <option value="66">Description/Analysis of Place/Territory</option>
                                    <option value="67">Design &amp; Technology</option>
                                    <option value="68">Design Analysis</option>
                                    <option value="69">Digital Cultures</option>
                                    <option value="70">Drama</option>
                                    <option value="73">E-Commerce</option>
                                    <option value="71">East European Studies</option>
                                    <option value="72">Ecology</option>
                                    <option value="74">Econometric</option>
                                    <option value="75">Economics</option>
                                    <option value="76">Education</option>
                                    <option value="77">Education Theories</option>
                                    <option value="78">Electrical Engineering</option>
                                    <option value="79">Electronic Engineering</option>
                                    <option value="80">Engineering</option>
                                    <option value="81">English</option>
                                    <option value="82">English Literature</option>
                                    <option value="83">Environmental Engineering</option>
                                    <option value="84">Environmental Issues</option>
                                    <option value="85">Environmental Science</option>
                                    <option value="86">Environmental Studies</option>
                                    <option value="87">Ethics</option>
                                    <option value="88">Ethnic and Area Studies</option>
                                    <option value="89">European Studies</option>
                                    <option value="90">Family &amp; Consumer Science</option>
                                    <option value="91">Fashion &amp; Textile</option>
                                    <option value="92">Film &amp; Theater Studies</option>
                                    <option value="93">Finance</option>
                                    <option value="94">Financial Law</option>
                                    <option value="95">Food Science</option>
                                    <option value="96">French Studies</option>
                                    <option value="97">Gender</option>
                                    <option value="98">Gene Technology</option>
                                    <option value="99">Genetics</option>
                                    <option value="100">Geography</option>
                                    <option value="101">Geology</option>
                                    <option value="102">Geoscience</option>
                                    <option value="103">Germanic Studies</option>
                                    <option value="104">Globalization</option>
                                    <option value="105">Government</option>
                                    <option value="106">Health</option>
                                    <option value="107">Health Science</option>
                                    <option value="108">Histology</option>
                                    <option value="109">History</option>
                                    <option value="110">Holocaust</option>
                                    <option value="111">Hospitality Management</option>
                                    <option value="112">Human Resource</option>
                                    <option value="113">Human Sexuality</option>
                                    <option value="114">Immunobiology</option>
                                    <option value="115">Indonesian Studies</option>
                                    <option value="116">Information Campaign</option>
                                    <option value="117">Information Science</option>
                                    <option value="118">Information System Management</option>
                                    <option value="119">International Affairs/Relations</option>
                                    <option value="120">International Literary Studies</option>
                                    <option value="121">International Relation</option>
                                    <option value="122">International Studies &amp; Foreign Affairs</option>
                                    <option value="123">Internet</option>
                                    <option value="124">Investment</option>
                                    <option value="125">IT Management</option>
                                    <option value="126">Italian Studies</option>
                                    <option value="127">Japanese Studies</option>
                                    <option value="128">Journalism</option>
                                    <option value="129">Korean Studies</option>
                                    <option value="130">Labor Studies</option>
                                    <option value="131">Latin-American Studies</option>
                                    <option value="132">Law</option>
                                    <option value="133">Legal Issues</option>
                                    <option value="134">Legal Management</option>
                                    <option value="135">Library Science</option>
                                    <option value="136">Linguistic</option>
                                    <option value="137">Literature</option>
                                    <option value="138">Logic &amp; Programming</option>
                                    <option value="139">Logistics</option>
                                    <option value="140">Management</option>
                                    <option value="141">Management Science</option>
                                    <option value="142">Marketing</option>
                                    <option value="143">Mathematics</option>
                                    <option value="144">Mechanical Engineering</option>
                                    <option value="145">Media Studies</option>
                                    <option value="146">Medical Science</option>
                                    <option value="147">Medicine</option>
                                    <option value="148">Meteorology</option>
                                    <option value="149">Methodology</option>
                                    <option value="150">Military Science</option>
                                    <option value="151">Modern Studies</option>
                                    <option value="152">Movies</option>
                                    <option value="153">Music</option>
                                    <option value="154">Native-American Studies</option>
                                    <option value="213">Natural Resources</option>
                                    <option value="155">Natural Sciences</option>
                                    <option value="156">Nature</option>
                                    <option value="157">Neuroscience</option>
                                    <option value="158">Nursing</option>
                                    <option value="159">Nutrition</option>
                                    <option value="160">Paintings</option>
                                    <option value="161">Pedagogy</option>
                                    <option value="162">Performance Studies</option>
                                    <option value="163">Personal Management</option>
                                    <option value="164">Personnel &amp; Development</option>
                                    <option value="165">Pharmacology</option>
                                    <option value="166">Pharmacy</option>
                                    <option value="167">Philosophy</option>
                                    <option value="168">Photography</option>
                                    <option value="169">Physical Education</option>
                                    <option value="170">Physics</option>
                                    <option value="171">Plant Science</option>
                                    <option value="172">Political Economy</option>
                                    <option value="173">Political Science</option>
                                    <option value="174">Project Management</option>
                                    <option value="175">Psychology</option>
                                    <option value="176">Public Administration</option>
                                    <option value="177">Public Health</option>
                                    <option value="178">Public Policy</option>
                                    <option value="179">Public Relation</option>
                                    <option value="180">Radiography</option>
                                    <option value="181">Real Estate/Property</option>
                                    <option value="182">Religion and Theology</option>
                                    <option value="183">Science</option>
                                    <option value="184">Shakespeare</option>
                                    <option value="185">Social Issues</option>
                                    <option value="186">Social Studies</option>
                                    <option value="187">Social Work</option>
                                    <option value="188">Sociology</option>
                                    <option value="189">Software Engineering</option>
                                    <option value="190">Soil Science</option>
                                    <option value="191">Space Science</option>
                                    <option value="192">Special Education</option>
                                    <option value="193">Sport</option>
                                    <option value="194">Statistics</option>
                                    <option value="195">Taxonomy</option>
                                    <option value="196">Teachers Career</option>
                                    <option value="197">Technology</option>
                                    <option value="198">Theater Studies</option>
                                    <option value="199">Tourism</option>
                                    <option value="200">Trade</option>
                                    <option value="201">Visual Arts</option>
                                    <option value="202">Web &amp; High Tech</option>
                                    <option value="203">Web Design</option>
                                    <option value="204">West European Studies</option>
                                    <option value="205">Wildlife Science</option>
                                    <option value="206">Women &amp; Gender Studies</option>
                                    <option value="207">Women Studies</option>
                                    <option value="208">World Affairs</option>
                                    <option value="209">World Literature</option>
                                    <option value="210">World Politics</option>
                                    <option value="211">Youth Issues</option>
                                    <option value="212">Zoology</option>-->
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Your Topic <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="paper_topic">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Attach Files</div>
                            <div class="field-input">
                                <input type="file" name="displayfiles" id="displayfiles" class="button2" value="Click to Browse">
                                <br>
                                <input type="file" name="files" id="fileuploadmanually" style="display:none;" multiple="">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Academic Level <span>*</span></div>
                            <div class="field-input">
                                <select name="academic_level" class="shortest">
                                    <option value="0">[Not Selected]</option>
<!--                                    <option value="2">High School</option>
                                    <option value="3">Under Graduate</option>
                                    <option value="4">Master</option>
                                    <option value="5">PhD</option>
                                    <option value="6">Graduate</option>
                                    <option value="1">Other</option>-->
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Writing Style <span>*</span></div>
                            <div class="field-input">
                                <select name="writing_style" class="shortest">
                                    <option value="0">[Not Selected]</option>
<!--                                    <option value="9">APA</option>
                                    <option value="7">Cambridge</option>
                                    <option value="2">CBE</option>
                                    <option value="3">Chicago/Turabian</option>
                                    <option value="4">Harvard</option>
                                    <option value="5">MLA</option>
                                    <option value="6">Oxford</option>
                                    <option value="8">Vancouver</option>
                                    <option value="1">Other</option>-->
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Preferred Language <span>*</span></div>
                            <div class="field-input">
                                <select name="preferred_language" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="2">English (U.K)</option>
                                    <option value="3">English (U.S.A)</option>
                                    <option value="1">Other</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Required References <span>*</span></div>
                            <div class="field-input">
                                <select name="no_of_sources" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="Not Required">Not Required</option>
                                    <?php
for ($i=1; $i < 81; $i++) { 
    ?>
      <option value="<?php echo $i ?>"><?php echo $i ?></option>
    <?php 
    # code...
}
                                     ?>
                                    
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Detailed Instructions <span>*</span></div>
                            <div class="field-input">
                                <textarea name="any_specification" rows="7" cols="12"></textarea>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">How Did You Find Us <span>*</span></div>
                            <div class="field-input">
                                <select name="reach_us_sources" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="2">Social Media (Facebook, Twitter, Instagram)</option>
                                    <option value="3">Search Engine (Google, Bing)</option>
                                    <option value="4">Refer by a Friend</option>
                                    <option value="5">Returning Customer</option>
                                    <option value="6">Email</option>
                                    <option value="7">Online Advertisement</option>
                                    <option value="8">Newspaper or TV Ads</option>
                                    <option value="9">Billboard or Flyer</option>
                                    <option value="1">Other</option>
                                </select>
                            </div>
                            <p></p>
                        </div>

                        <div class="form-row">
                            <div class="field-label">Payment Method <span>*</span></div>
                            <div class="field-input">
                                <div class="pm-cc-pp radio">

                                    <input type="radio" name="pay_method" id="pp" value="paypal"><label
                                            for="pp">PayPal</label>&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <input
                                            type="radio" name="pay_method" id="cc" value="Stripe"> <label for="cc">Credit
                                        Card</label>

                                </div>

                            </div>
                            <p></p>
                        </div>

                        <div class="form-row">


                            <input type="checkbox" name="agree_terms" id="agree_terms">  <label for="agree_terms">I have read and agree to <a href="/mhrwriter/guarantee" class="style" target="_blank">Terms &amp; Policies</a>.</label>


                            <br>
                            <input type="checkbox" name="agree_2co" id="agree_2co"> <label for="agree_2co">I accept that Paypal or Card payment can be processed via 2checkout.com (our payment processor) which is safe and secure.</label>
                        </div>
                        <div class="form-row">
                            <div class="field-label"></div>
                            <div class="field-input">
                                <input type="hidden" name="amount" class="total_amount">
                                <button name="wds_donate" value="place_order" type="submit">Place Order</button>
                            </div>
                            <p></p>
                        </div>
                        <p></p>
                    </form>
                    <p></p>
                </div>
                <h3 class="heading3">Secure Order Process:</h3>
                <br>
                <p>People are usually concerned at the time of placing an order whether the site is secure and trustworthy or not. Once the payment has been made the thought might enter a student’s mind, what if I don’t get satisfied work or if I have plagiarism issues? The entire team of MHR Writer would like to take this opportunity to remind you that we are here to serve you. According to the Satisfaction Guarantee you will be able to get unlimited revisions if our writers are unable to thoroughly satisfy your initial demands. You can also avail the option of revision or a refund IF there is any plagiarism in the final document.</p>
                <p>If you have placed an order and you want to cancel it, you will be issued a refund. If there are any issues or concerns you can contact the customer support service team at MHR Writer to get personalised assistance. They will be more than happy to assist you.</p>
                <p>Know that with <a href="/mhrwriter/" title="MHR Writing Solutions">MHR Writer</a> you are in good hands. Our goals at the time of composing the terms and conditions is maintaining long term relationships with all of our customers. We will strictly comply with all of the terms and policies to facilitate you. We are enthusiastic in providing learners with quality services each time.</p>
            </div>
        </div>
    </div>
    <div class="fusion-one-fourth one_fourth fusion-layout-column fusion-column last spacing-yes">
        <div class="fusion-column-wrapper">
            <div id="SN">
                <div class="sitebox features">
                    <div class="top">
                        <div class="head">Benefits You Get</div>
                        <div class="bg"><span>Get it<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li>Customer support 24/7</li>
                            <li>100% satisfaction guaranteed</li>
                            <li>100% confidentiality</li>
                            <li>On-time help provided</li>
                            <li>Contact directly to helper</li>
                            <li>Unique ideas and thoughts</li>
                        </ul>
                    </div>
                    <p></p>
                </div>
                <p>
                    <a href="/mhrwriter/order" title="Order Now for Online Assignment Help" class="order"> <img src="/mhrwriter/wp-content/uploads/2018/08/order-now.png" alt="Order Today for Quality Assignment Writing Services" width="270" height="71"> </a>
                </p>
                <div class="sitebox livechat" onclick="Comm100API.open_chat_window(event, 366);">
                    <div class="top">
                        <div class="head"><span>Live Chat</span><span class="status">ONLINE</span></div>
                        <div class="bg"><span>Chat<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <p>Let us Gladly
                            <br>Assist you…!!</p>
                        <p></p>
                    </div>
                    <p></p>
                </div>
                <div class="sitebox callnow">
                    <div class="top">
                        <div class="head">24/7 UK Toll Free</div>
                        <div class="bg"><span>Call<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">+44 800 048 8966</div>
                    <p></p>
                </div>
                <div class="sitebox blogs">
                    <div class="top">
                        <div class="head">Latest Blogs</div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li> <a href="/mhrwriter/how-to-overcome-shyness">How to Overcome Shyness When Meeting New Acquaintances</a> </li>
                            <li> <a href="/mhrwriter/productive-homework-tips-for-students">3 Productive Homework Tips for Students to Achieve Success</a> </li>
                            <li> <a href="/how-to-improve-public-speaking-skills">How to Improve Public Speaking Skills and Confidence</a> </li>
                            <li> <a href="/mhrwriter/ways-to-save-money-to-manage-expenses">Easy Ways to Save Money to Manage Academic Expenses</a> </li>
                            <li> <a href="/mhrwriter/dissertation-vs-thesis-uk">Dissertation VS Thesis Know the Similarities and Differences</a> </li>
                            <li> <a href="/mhrwriter/how-to-concentrate-on-studies-and-retain-a-job">How to Concentrate on Studies and Successfully Retain a Job</a> </li>
                            <li> <a href="/mhrwriter/how-to-write-a-dissertation">How to Write a Dissertation and Concurrently Seek Employment</a> </li>
                            <li> <a href="/mhrwriter/controlling-the-average-student-budget">Controlling the Average Student Budget for Recreational Purposes</a> </li>
                            <li> <a href="/mhrwriter/change-wording-to-avoid-plagiarism">Productively Change Words to Avoid Plagiarism in Your Work</a> </li>
                            <li> <a href="/mhrwriter/how-to-start-writing-a-dissertation">How to Start a Dissertation and the Measures Involved</a> </li>
                        </ul>
                    </div>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <div class="fusion-clearfix"></div>
</div>
<!-- js scripts -->
<script defer="defer" src="http://localhost/outside/essay-site/wp/wp-content/themes/Avada/js/all.js"></script>
<script defer="defer" type="text/javascript"> var service_type_id = 0; var urgency_time_id = 0; </script>
<script defer="defer" type="text/javascript" src="http://localhost/outside/essay-site/wp/wp-content/themes/Avada/js/order-data.js">
</script> <script defer="defer" type="text/javascript"> var TemplateUrl = "http://localhost/outside/essay-site/wp/wp-content/themes/Avada/"; var NoCalculate = ""; var SeparateUpload = "no"; var plag_price = "4"; var plag_words = "4000"; </script>
<script defer="defer" type="text/javascript" src="http://localhost/outside/essay-site/wp/wp-content/themes/Avada/js/orders.js"></script>
<script defer="defer" type="text/javascript" src="http://localhost/outside/essay-site/wp/wp-content/themes/Avada/js/attach-files.js"></script>