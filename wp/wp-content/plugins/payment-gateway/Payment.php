<?php
/**
 * Plugin Name:       Payment gateway
 * Description:       Payment method of paypal and stripe!
 * Version:           1.0.0
 * Author:            Husnain ali
 * Author URI:        http://husnain.pk
 * Text Domain:       Husnain
 * License:           GPL-2.0+
 */
/*
 * Plugin constants
 */
if (!defined('PaymentGateway_URL'))
    define('PaymentGateway_URL', plugin_dir_url(__FILE__));
if (!defined('PaymentGateway_PATH'))
    define('PaymentGateway_PATH', plugin_dir_path(__FILE__));

/*
 * Main class
 */
/**
 * Class PaymentGateway
 *
 * This class creates the option page and add the web app script
 */
require "paypal/paypal.php";
require_once('stripe/Create_payment.php');

class PaymentGateway {

    /**
     * PaymentGateway constructor.
     *
     * The main plugin actions registered for WordPress
     */
    public function __construct() {
        global $wpdb;
// Admin page calls:
        add_action('admin_menu', array($this, 'addAdminMenu'));
//for gateways store
        $create_table_query = "
                CREATE TABLE IF NOT EXISTS `wp_payment_gateways` ( `ID` INT(11) NOT NULL AUTO_INCREMENT , `GatewayName` VARCHAR(255) NOT NULL ,`Enviorment` TEXT NOT NULL , `ClientID` TEXT NOT NULL , `ClientSecret` TEXT NOT NULL ,`ClientIDTest` TEXT NOT NULL , `ClientSecretTest` TEXT NOT NULL , `RedirectSuccess` TEXT NOT NULL ,`RedirectCancle` TEXT NOT NULL , `Status` INT(11) NOT NULL , PRIMARY KEY (`ID`)) ENGINE = MYISAM;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($create_table_query);
//gateways response
        $create_gateways_response_query = "
                CREATE TABLE IF NOT EXISTS `wp_payment_gateways_response` ( `ID` INT(11) NOT NULL AUTO_INCREMENT , `GatewayName` VARCHAR(255) NOT NULL ,`Email` VARCHAR(255) NOT NULL , `Response` TEXT NOT NULL , `Method` TEXT NOT NULL , `CreatedAt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `Status` VARCHAR(255) NOT NULL , PRIMARY KEY (`ID`)) ENGINE = MYISAM;";

//Temp order
        $create_temp_order_query = "
                CREATE TABLE IF NOT EXISTS `wp_temp_orders` ( `ID` INT(11) NOT NULL AUTO_INCREMENT , `Token` TEXT NOT NULL, `TempOrder` TEXT NOT NULL, `PaymentGateway` TEXT NOT NULL,`Email` VARCHAR(255) NOT NULL , `CreatedAt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                `Status` VARCHAR(255) NOT NULL , PRIMARY KEY (`ID`)) ENGINE = MYISAM;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($create_table_query);
        dbDelta($create_gateways_response_query);
        dbDelta($create_temp_order_query);
    }

    public function addAdminMenu() {
        add_menu_page(
                __('PaymentGateway', 'PaymentGateway'), __('PaymentGateway', 'PaymentGateway'), 'manage_options', 'PaymentGateway', array($this, 'adminLayout'), ''
        );
    }

    public function adminLayout() {
        global $wpdb;
        if (isset($_POST['gateway_type']) && $_POST['gateway_type'] == 'Paypal') {
            $ID = $_POST['ID'];
            $GatewayName = 'Paypal';
            $ClientSecret = $_POST['ClientSecret'];
            $ClientID = $_POST['ClientID'];
            $Mode = $_POST['Mode'];
            $ClientIDTest = $_POST['ClientIDTest'];
            $ClientSecretTest = $_POST['ClientSecretTest'];
            $RedirectSuccess = $_POST['RedirectSuccess'];
            $RedirectCancle = $_POST['RedirectCancle'];
            $Status = $_POST['status'];
            if (isset($_POST['PaymentGateway-admin-save']) && empty($ID)) {
                $insert_query = "INSERT INTO `{$wpdb->prefix}payment_gateways`
                     set GatewayName = '$GatewayName',
                                    ClientID = '$ClientID',
                                    ClientSecret = '$ClientSecret',
                                    GatewayEnviorment = '$Mode',
                                    ClientIDTest = '$ClientIDTest',
                                    ClientSecretTest = '$ClientSecretTest',
                                    RedirectSuccess = '$RedirectSuccess',
                                    RedirectCancle = '$RedirectCancle',
                                   Status =   '$Status'";
                $wpdb->query($insert_query);
            } else if (isset($_POST['PaymentGateway-admin-save']) && !empty($ID)) {
                $insert_query = "update `{$wpdb->prefix}payment_gateways`
                                    set GatewayName = '$GatewayName',
                                    ClientID = '$ClientID',
                                    ClientSecret = '$ClientSecret',
                                    GatewayEnviorment = '$Mode',
                                    ClientIDTest = '$ClientIDTest',
                                    ClientSecretTest = '$ClientSecretTest',
                                    RedirectSuccess = '$RedirectSuccess',
                                    RedirectCancle = '$RedirectCancle',
                                   Status =   '$Status'
                                   where ID = $ID";
                $wpdb->query($insert_query);
            }
        } else if (isset($_POST['Stripegateway_type']) && $_POST['Stripegateway_type'] == 'Stripe') {
            $ID = $_POST['StripeID'];
            $GatewayName = 'Stripe';
            $ClientSecret = $_POST['StripeClientSecret'];
            $ClientID = $_POST['StripeClientID'];
            $Mode = $_POST['StripeMode'];
            $ClientIDTest = $_POST['StripeClientIDTest'];
            $ClientSecretTest = $_POST['StripeClientSecretTest'];
            $RedirectSuccess = $_POST['StripeRedirectSuccess'];
            $RedirectCancle = $_POST['StripeRedirectCancle'];
            $Status = $_POST['Stripestatus'];
            if (isset($_POST['PaymentGateway-admin-save-Stripe']) && empty($ID)) {
                $insert_query = "INSERT INTO `{$wpdb->prefix}payment_gateways`
                     set GatewayName = '$GatewayName',
                                    ClientID = '$ClientID',
                                    ClientSecret = '$ClientSecret',
                                    GatewayEnviorment = '$Mode',
                                    ClientIDTest = '$ClientIDTest',
                                    ClientSecretTest = '$ClientSecretTest',
                                    RedirectSuccess = '$RedirectSuccess',
                                    RedirectCancle = '$RedirectCancle',
                                   Status =   '$Status'";
                $wpdb->query($insert_query);
            } else if (isset($_POST['PaymentGateway-admin-save-Stripe']) && !empty($ID)) {
                $insert_query = "update `{$wpdb->prefix}payment_gateways`
                                    set GatewayName = '$GatewayName',
                                    ClientID = '$ClientID',
                                    ClientSecret = '$ClientSecret',
                                    GatewayEnviorment = '$Mode',
                                    ClientIDTest = '$ClientIDTest',
                                    ClientSecretTest = '$ClientSecretTest',
                                    RedirectSuccess = '$RedirectSuccess',
                                    RedirectCancle = '$RedirectCancle',
                                   Status =   '$Status'
                                   where ID = $ID";
                $wpdb->query($insert_query);
            }
        }
        $payment_gateways_paypal = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}payment_gateways` where GatewayName='Paypal'");
        $payment_gateways_stripe = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}payment_gateways` where GatewayName='Stripe'");
        ?>

        <div class="wrap">
            <h3><?php _e('PaymentGateway API Settings', 'PaymentGateway'); ?></h3>

            <h4>
                <?php _e('Paypal', 'PaymentGateway'); ?>
                <h4>
                    <hr>
                    <table class="form-table">
                        <tbody>
                        <form action="?page=PaymentGateway" method="post">
                            <input name="gateway_type" type="hidden"  class="regular-text" value="Paypal"/>
                            <input name="ID" type="hidden" id="ID" class="regular-text"
                                   value="<?php echo (isset($payment_gateways_paypal[0]->ID)) ? $payment_gateways_paypal[0]->ID : ''; ?>"/>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Public key', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="ClientID"
                                           id="ClientID"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->ClientID)) ? $payment_gateways_paypal[0]->ClientID : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Public key Sandbox', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="ClientIDTest"
                                           id="ClientIDTest"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->ClientIDTest)) ? $payment_gateways_paypal[0]->ClientIDTest : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Private key', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="ClientSecret"
                                           id="ClientSecret"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->ClientSecret)) ? $payment_gateways_paypal[0]->ClientSecret : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Private key Sandbox', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="ClientSecretTest"
                                           id="ClientSecretTest"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->ClientSecretTest)) ? $payment_gateways_paypal[0]->ClientSecretTest : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Redirect Url Success', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="RedirectSuccess"
                                           id="RedirectSuccess"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->RedirectSuccess)) ? $payment_gateways_paypal[0]->RedirectSuccess : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Redirect Url Cancle', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="RedirectCancle"
                                           id="RedirectCancle"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_paypal[0]->RedirectCancle)) ? $payment_gateways_paypal[0]->RedirectCancle : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Mode', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <select name="Mode"  class="regular-text">
                                        <option value="Sandbox" <?php echo (isset($payment_gateways_paypal[0]->GatewayEnviorment) && $payment_gateways_paypal[0]->GatewayEnviorment == "Sandbox" ) ? 'selected="select"' : ''; ?>>Sandbox</option>
                                        <option value="Live" <?php echo (isset($payment_gateways_paypal[0]->GatewayEnviorment) && $payment_gateways_paypal[0]->GatewayEnviorment == "Live" ) ? 'selected="select"' : ''; ?>>Live</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Status', 'PaymentGateway'); ?></label>
                                </td>
                                <td>

                                    <input type="radio" name="status" id="status"  value="1" <?php echo (isset($payment_gateways_paypal[0]->Status) && $payment_gateways_paypal[0]->Status == 1 ) ? 'checked="checked"' : ''; ?>>
                                    <label>
                                        <for="pp">Active</label>&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <input
                                        type="radio" name="status" id="status" value="0"  <?php echo (isset($payment_gateways_paypal[0]->Status) && $payment_gateways_paypal[0]->Status == 0 ) ? 'checked="checked"' : ''; ?>> <label for="status">Deactive
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button class="button button-primary" id="PaymentGateway-admin-save" name="PaymentGateway-admin-save"  type="submit"><?php _e('Save', 'PaymentGateway'); ?></button>
                                </td>
                            </tr>
                        </form>
                        </tbody>

                    </table>
                    <hr>
                    <h3>Stripe</h3>
                    <table class="form-table">
                        <tbody>
                        <form action="?page=PaymentGateway" method="post">
                            <input name="Stripegateway_type" type="hidden"  class="regular-text" value="Stripe"/>
                            <input name="StripeID" type="hidden" id="ID" class="regular-text"
                                   value="<?php echo (isset($payment_gateways_stripe[0]->ID)) ? $payment_gateways_stripe[0]->ID : ''; ?>"/>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Public key', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeClientID"
                                           id="StripeClientID"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->ClientID)) ? $payment_gateways_stripe[0]->ClientID : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Public key Sandbox', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeClientIDTest"
                                           id="StripeClientIDTest"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->ClientIDTest)) ? $payment_gateways_stripe[0]->ClientIDTest : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Private key', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeClientSecret"
                                           id="StripeClientSecret"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->ClientSecret)) ? $payment_gateways_stripe[0]->ClientSecret : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Private key Sandbox', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeClientSecretTest"
                                           id="StripeClientSecretTest"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->ClientSecretTest)) ? $payment_gateways_stripe[0]->ClientSecretTest : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Redirect Url Success', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeRedirectSuccess"
                                           id="StripeRedirectSuccess"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->RedirectSuccess)) ? $payment_gateways_stripe[0]->RedirectSuccess : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Redirect Url Cancle', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <input name="StripeRedirectCancle"
                                           id="StripeRedirectCancle"
                                           class="regular-text"
                                           value="<?php echo (isset($payment_gateways_stripe[0]->RedirectCancle)) ? $payment_gateways_stripe[0]->RedirectCancle : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Mode', 'PaymentGateway'); ?></label>
                                </td>
                                <td>
                                    <select name="StripeMode"  class="regular-text">
                                        <option value="Sandbox"  <?php echo (isset($payment_gateways_stripe[0]->GatewayEnviorment) && $payment_gateways_stripe[0]->GatewayEnviorment == "Sandbox" ) ? 'selected="select"' : ''; ?>>Sandbox</option>
                                        <option value="Live" <?php echo (isset($payment_gateways_stripe[0]->GatewayEnviorment) && $payment_gateways_stripe[0]->GatewayEnviorment == "Live" ) ? 'selected="select"' : ''; ?>>Live</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Status', 'PaymentGateway'); ?></label>
                                </td>
                                <td>

                                    <input type="radio" name="Stripestatus" id="status"  value="1" <?php echo (isset($payment_gateways_stripe[0]->Status) && $payment_gateways_stripe[0]->Status == 1 ) ? 'checked="checked"' : ''; ?>>
                                    <label>
                                        <for="pp">Active</label>&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <input
                                        type="radio" name="status" id="status" value="0"  <?php echo (isset($payment_gateways_stripe[0]->Status) && $payment_gateways_stripe[0]->Status == 0 ) ? 'checked="checked"' : ''; ?>> <label for="status">Deactive
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button class="button button-primary" id="PaymentGateway-admin-save-Stripe" name="PaymentGateway-admin-save-Stripe"  type="submit"><?php _e('Save', 'PaymentGateway'); ?></button>
                                </td>
                            </tr>
                        </form>
                        </tbody>

                    </table>
                    </div>

                    <?php
                }

            }

            /*
             * Starts our plugin class, easy!
             */
            new PaymentGateway();

            function post_order_api($id = 0, $token = '') {
                global $wpdb;
                if ($id != 0) {
                    $select_order_query = "select * from `{$wpdb->prefix}temp_orders` where ID = $id ";
                } else {
                    $select_order_query = "select * from `{$wpdb->prefix}temp_orders` where Token = '$token'";
                }
                $order_data = $wpdb->get_results($select_order_query);
                // $json = '{"access_key":"","profile_id":"","transaction_uuid":"5beeb1d950402","signed_field_names":"merchant_defined_data1,consumer_id,contact_phone,email_address,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency","unsigned_field_names":"","consumer_id":"","customer_ip_address":"::1","merchant_defined_data1":"WC","signed_date_time":"2018-11-16T12:02:33Z","locale":"en","transaction_type":"sale","reference_number":"","full_name":"hasnain ali","email_address":"hasnain03ali@gmail.com","contact_phone":"3006854307","paper_type":"5","service_type":"0","urgency_time":"0","quality_level":"2","number_of_pages":"6","currency":"1","plag_report":"1","discount_code":"","subject_area":"11","paper_topic":"24423432","displayfiles":"","files":"","academic_level":"4","writing_style":"3","preferred_language":"3","no_of_sources":"9","any_specification":"werewrwer","reach_us_sources":"6","pay_method":"Stripe","agree_terms":"on","agree_2co":"on","amount":"","wds_donate":"place_order"}';
                $Order_array = json_decode($order_data[0]->TempOrder, true);

                $full_name = $Order_array['full_name'];
                $email_address = $Order_array['email_address'];
                $contact_phone = $Order_array['contact_phone'];
                $customer_ip_address = $Order_array['customer_ip_address'];
                $model = 'order';
                $customer_ip_address = $Order_array['customer_ip_address'];
                $paper_type_id = $Order_array['paper_type'];
                $service_type_id = $Order_array['service_type'];
                $quality_id = $Order_array['quality_level'];
                $urgency_id = $Order_array['urgency_time'];
                $no_pages_id = $Order_array['number_of_pages'];
                $subject_area_id = $Order_array['subject_area'];
                $topic = $Order_array['paper_topic'];
                $acadamic_level_id = $Order_array['academic_level'];
                $writing_style_id = $Order_array['writing_style'];
                $instructions = $Order_array['any_specification'];
                $payment_method = $Order_array['pay_method'];
                $amount = $Order_array['amount'];
                $date = date("Y-m-d H:i:s");

                $connection_crm = mysqli_connect('globalmarketingforum.org', 'globalma_hasnain', 'tiptop123', 'globalma_mhwriter_apanel');
                $sql_customers = "INSERT INTO `customers`
                     set name = '$full_name',
                                    email = '$email_address',
                                    phone = '$contact_phone',
                                    ip = '$customer_ip_address',
                                    city = '',
                                    region = '',
                                    country = '',
                                    created_at = '$date'";
                $result = $connection_crm->query($sql_customers);
                $cus_insert_id = mysqli_insert_id($connection_crm);
                $sql_orders = "INSERT INTO `orders`
                    set customer_id = '$cus_insert_id',
                    status = 'pending',
                    total_amount = '$amount',
                    paper_type_id = '$paper_type_id',
                    service_type_id = '$service_type_id',
                    order_date = '$date',
                    urgency_id = '$urgency_id',
                    quality_id = '$quality_id',
                    no_pages_id = '$no_pages_id',
                    subject_area_id = '$subject_area_id',
                    topic = '$topic',
                    acadamic_level_id = '$acadamic_level_id',
                    writing_style_id = '$writing_style_id',
                    language = '',
                    referances = '',
                    instructions = '$instructions',
                    payment_method = '$payment_method',
                    writer_deadline = ''";
                $result_orders = $connection_crm->query($sql_orders);
                $order_insert_id = mysqli_insert_id($connection_crm);

                $sql_email = "select * from email_templates where type = 'payment_received'";
                $result_email = $connection_crm->query($sql_email);
                $row_email = $result_email->fetch_assoc();
// var_dump($row_email);die;
                $subject = $row_email['subject'];

                $body = $row_email['body'];
                $body = str_replace("#Payment_gateway", $payment_method, $body);
                $body = str_replace("#order_no", $order_insert_id, $body);
                $body = str_replace("#task_topic", $topic, $body);
                $body = str_replace("#client_name", $full_name, $body);
                $body = str_replace("#no_pages", $no_pages_id, $body);
                $body = str_replace("#subject_area", $subject_area_id, $body);


                require_once 'PHPMailer/PHPMailerAutoload.php';
                $mail = new PHPMailer(true);
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.sendgrid.net';
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'mmahwish.1@gmail.com';           // SMTP username
                $mail->Password = 'Tabish$11';                        // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 2525;                                    // TCP port to connect to
                $mail->setFrom('notifications@usjobhelpcenter.com', '');
                $mail->addAddress($email_address);

                $mail->isHTML(true);
                $mail->Subject = $subject;



                $mail->Body = html_entity_decode(html_entity_decode($body));
                $mail->send();
                return $order_insert_id;
                //$post_data = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[name]\"\r\n\r\n".$Order_array['full_name']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[email]\"\r\n\r\n".$Order_array['email_address']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[phone]\"\r\n\r\n".$Order_array['contact_phone']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[ip]\"\r\n\r\n".$Order_array['customer_ip_address']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"model\"\r\n\r\norder\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"total_amount\"\r\n\r\n".$Order_array['amount']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"paper_type_id\"\r\n\r\n".$Order_array['paper_type']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"service_type_id\"\r\n\r\n".$Order_array['service_type']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"quality_id\"\r\n\r\n".$Order_array['quality_level']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"urgency_id\"\r\n\r\n".$Order_array['urgency_time']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"no_pages_id\"\r\n\r\n".$Order_array['number_of_pages']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"subject_area_id\"\r\n\r\n".$Order_array['subject_area']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"topic\"\r\n\r\n".$Order_array['paper_topic']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"acadamic_level_id\"\r\n\r\n".$Order_array['acadamic_level']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"writing_style_id\"\r\n\r\n".$Order_array['writing_style']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"instructions\"\r\n\r\n".$Order_array['any_specification']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"payment_method\"\r\n\r\n".$Order_array['pay_method']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
//                foreach ($post_data as $key => $value) {
//                    $fields_string .= $key . '=' . $value . '&';
//                }
                //  rtrim($fields_string, '&');
//                $curl = curl_init();
//                curl_setopt_array($curl, array(
//                    CURLOPT_URL => "http://globalmarketingforum.org/wpanel/backend/web/index.php/api/record",
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_ENCODING => "",
//                    CURLOPT_MAXREDIRS => 10,
//                    CURLOPT_TIMEOUT => 30,
//                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                    CURLOPT_CUSTOMREQUEST => "POST",
//                    CURLOPT_POSTFIELDS => 'cust[name]=hasnain ali&cust[email]=hasnain03ali@gmail.com&cust[phone]=3006854307&cust[ip]=::1&model=order&total_amount=::1&paper_type_id=4&service_type_id=4&quality_id=1&urgency_id=3&no_pages_id=6&subject_area_id=3&topic=ddsfsdf&acadamic_level_id=5&writing_style_id=3&instructions=sdfdfdfa&payment_method=paypal',//$fields_string, //"------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[name]\"\r\n\r\ntest\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[email]\"\r\n\r\ntest@yopmail.com\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[phone]\"\r\n\r\n123456879\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cust[ip]\"\r\n\r\n12.12.12.12\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"model\"\r\n\r\norder\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"total_amount\"\r\n\r\n12.55\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"paper_type_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"service_type_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"quality_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"urgency_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"no_pages_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"subject_area_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"topic\"\r\n\r\nfrom api\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"acadamic_level_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"writing_style_id\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"instructions\"\r\n\r\nasdasdasd\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"payment_method\"\r\n\r\npaypal\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",//$post_data,
//                    CURLOPT_HTTPHEADER =>
//                    array(
//                        ": ",
//                        "Authorization: Basic YWRtaW46dGwwMDE=",
//                        "Postman-Token: 040b6e4c-5b18-4a55-9d1c-03ae484120d8",
//                        "cache-control: no-cache",
//                        "content-type: multipart/form-data"
//                    )
//                ));
//
//                $response = curl_exec($curl);
//                $err = curl_error($curl);
//
//                curl_close($curl);
//
//                if ($err) {
//                    echo "cURL Error #:" . $err;
//                } else {
//                    var_dump($response);
//                }
            }

            function insert_gateway_response($param, $method, $status, $gateway) {
                global $wpdb;
                $Response = json_encode($param);
                $insert_query = "INSERT INTO `{$wpdb->prefix}payment_gateways_response` (GatewayName, Email,Response, Method,Status) VALUES ('$gateway', '', '$Response','$method','$status')";
                $wpdb->query($insert_query);
                return $insert_id = $wpdb->insert_id;
            }

            function InsertTempOrder($param, $token = '', $Status = 'inprocess', $id = 0) {
                global $wpdb;
                $email_address = $_POST['email_address'];
                $pay_method = $_POST['pay_method'];
                $TempOrder = json_encode($_POST);
                $amount = $_POST['amount'];
                if ($id == 0) {
                    $insert_query = "INSERT INTO `{$wpdb->prefix}temp_orders` (TempOrder, Email,Token, Status,PaymentGateway) VALUES ('$TempOrder', '$email_address', '$token','$Status','$pay_method')";
                    $wpdb->query($insert_query);
                    return $insert_id = $wpdb->insert_id;
                } else {
                    $update_query = "update `{$wpdb->prefix}temp_orders`
                                 set Token = '$token',
                                   Status = '$Status'
                                   where ID = $id";
                    $wpdb->query($update_query);
                }
            }

            function updateTempOrderByToken($token, $Status = 'inprocess') {
                global $wpdb;
                $update_query = "update `{$wpdb->prefix}temp_orders`
                                 set Status = '$Status'
                                   where Token = '$token'";
                $wpdb->query($update_query);
            }

            function form_creation() {
                global $wpdb;
//                var_dump(ABSPATH);
//                die;
                $email_address = $_POST['email_address'];
                $pay_method = $_POST['pay_method'];
                $TempOrder = json_encode($_POST);
                $amount = round($_POST['amount']);
                
                if (isset($pay_method) && $pay_method == 'paypal') {
                    require "paypal_process.php";
                    $url_paypal = create_payment_url($amount, "USD", $details = array('email' => $email_address));
                    if (PaypalMode == 'Sandbox') {
                        $token = str_replace("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=", "", $url_paypal);
                    } else {
                        $token = str_replace("https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=", "", $url_paypal);
                    }
                    $TempOrderID = InsertTempOrder($_POST, $token, 'inprocess', 0);
                    echo '<script type="text/javascript">
                        window.location = "' . $url_paypal . '"
                        </script>';
                } else if (isset($pay_method) && $pay_method == 'Stripe') {
                    $amount = $_POST['amount'];
                    $TempOrderID = InsertTempOrder($_POST, $token, 'inprocess', 0);
                    require "CreditCardForm.php";
                } else if (isset($pay_method) && $pay_method == 'StripeCreditCharge') {

                    if (isset($_POST['card-numbers'])) {
                        $name = $_POST['card-holder-name'];
                        $card = trim($_POST['card-number']);
                        $expmonth = $_POST['expiry-month'];
                        $expyear = $_POST['expiry-year'];
                        $cvc = $_POST['cvv'];
                        $TempOrderID = $_POST['TempOrderID'];
                        $amount = $_POST['amount'];

                        $token = get_stripe_token($name, trim($_POST['card-numbers']), $expmonth, $expyear, $cvc);
                        if (isset($token['error']) && $token['error'] == 'yes') {
                            $ResponseID = insert_gateway_response(json_encode($token), 'Create Token', 'failure', 'Stripe');
                            $message = $token['message'];
                            require "CreditCardForm.php";
                        } else {
                            $token_array = $token->__toArray();
                            $ResponseID = insert_gateway_response(json_encode($token_array), 'Create Token', 'success', 'Stripe');
                            $trans_response = charge_amount($token_array, $amount, 'USD');
                            $trans_response = $trans_response->__toArray();
                            if (isset($trans_response['id'])) {
                                $ResponseID = insert_gateway_response(json_encode($trans_response), 'Charge amount', 'success', 'Stripe');
                                InsertTempOrder('', $trans_response['id'], 'Complete', $TempOrderID);
                                $order_id = post_order_api($TempOrderID);
                                require "thanks.php";
                            } else {
                                require "payment_cancle.php";
                            }
                        }
                    } else {
                        require "CreditCardForm.php";
                    }
                } else {
                    require "Form.php";
                }
            }

            add_shortcode('PaymentGatewayForm', 'form_creation');

            function payment_thanks() {
                global $wpdb;
                $Response = json_encode($_REQUEST);
                $ResponseID = insert_gateway_response($Response, 'Checkout response', 'success', 'Paypal');
                $response = execute_payment($_REQUEST);
                if (isset($response['msg']) && $response['msg'] == 'success') {
                    updateTempOrderByToken($_REQUEST['token'], 'Complete');
                    $order_id = post_order_api(0, $_REQUEST['token']);
                    require "thanks.php";
                } else {
                    updateTempOrderByToken($_REQUEST['token'], 'failed');
                    require "payment_cancle.php";
                }
            }

            add_shortcode('PaymentGatewayThanks', 'payment_thanks');

            function payment_cancle() {
                global $wpdb;
                $Response = json_encode($_REQUEST);
                $ResponseID = insert_gateway_response($Response, 'Checkout response', 'failed', 'Paypal');
                updateTempOrderByToken($_REQUEST['token'], 'Complete');
//                echo $insert_query = "INSERT INTO `{$wpdb->prefix}payment_gateways_response` (GatewayName, Email,Response, Method,Status) VALUES ('Paypal', '', '$Response','response checkout failed','failed')";
//                $wpdb->query($insert_query);
                require "payment_cancle.php";
            }

            add_shortcode('PaymentGatewayCancle', 'payment_cancle');

//            function creditCard() {
//                global $wpdb;
//                var_dump($_REQUEST);
//                $Response = json_encode($_REQUEST);
//                echo $insert_query = "INSERT INTO `{$wpdb->prefix}payment_gateways_response` (GatewayName, Email,Response, Method,Status) VALUES ('Paypal', '', '$Response','response checkout failed','failed')";
//                $wpdb->query($insert_query);
//                require "CreditCardForm.php";
//            }
//
//            add_shortcode('creditCard', 'creditCard');



            